function loader() {
    $("#fakeLoader").fakeLoader({
        timeToHide: 6000, //Time in milliseconds for fakeLoader disappear
        zIndex: 999, // Default zIndex
        spinner: "spinner3", //Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7' 
        bgColor: "rgba(80,45,5,0.6)", //"rgba(246,146,30,0.6)",//"rgba(51, 49, 49, 0.7)", //Hex, RGB or RGBA colors
        imagePath:"assets/images/matching_loader1.jpeg" //If you want can you insert your custom image
    });
    var txt_msg = 'Wir sortieren mit absoluter Sorgfalt diese Eigenschaften, bitten aber jeden Benutzer noch einmal selbst�ndig auf die Verpackung des Supplements zu achten';
    $('#fakeLoader').prepend('<div class="fakeloader_text">' + txt_msg + '</div>');
}