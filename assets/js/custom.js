$(document).ready(function () {
    $('.gt_mobile_menu').click(function () {
        if ($(this).hasClass('toggle_remove')) {
            $(this).removeClass('toggle_remove');
            $('body').removeClass('body-position');
        } else {
            $(this).addClass('toggle_remove');
            $('body').addClass('body-position');
        }
    })
    //-- remove body position class on modal close
    $('.modal_close').click(function () {
        $('body').removeClass('body-position');
    });
    //-- Display successs and alert messages on page load
    if (flash_success_msg != '') {
        $('#success_alert_message').html(flash_success_msg);
        $('#success_alert_modal').modal();
        setTimeout(function () {
            $('#success_alert_modal').modal('hide');
        }, 3000);
    }
    if (flash_error_msg != '') {
        $('#review_alert_message').html(flash_error_msg);
        $('#login_check_modal').modal();
        setTimeout(function () {
            $('#login_check_modal').modal('hide');
        }, 3000);
    }

    "use strict";
    var err_cnt = 0;
    $(".alert-box").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert-box").slideUp(500);
    });

    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
    /*
     ==============================================================
     Mobile Menu Script
     ==============================================================
     */
    if ($('.gt_mobile_menu').length) {
        $('.gt_mobile_menu').on('click', function () {
            if ($(window).width() <= 991) {
                $('.navigation').slideToggle('normal');
            }
        })
    }
    /*
     ==============================================================
     Jquery NSt Script
     ==============================================================
     */
    if ($('.nstSlider').length) {
        $('.nstSlider').nstSlider({
            "crossable_handles": false,
            "left_grip_selector": ".leftGrip",
            "right_grip_selector": ".rightGrip",
            "value_bar_selector": ".bar",
            "value_changed_callback": function (cause, leftValue, rightValue) {
                $(this).parent().find('.leftLabel').text(leftValue);
                $(this).parent().find('.rightLabel').text(rightValue);
            }
        });
    }
    ;
    /*
     ==============================================================
     Jquery Nice Scroll Script
     ==============================================================
     */
    if ($('#gt_product_list_wrap').length) {
        $('#gt_product_list_wrap').niceScroll({
            cursorcolor: "#f6921e",
            cursorwidth: "8px", // cursor width in pixel (you can also write "5px")
            cursorborder: "1px solid #f6921e", // css definition for cursor border
            cursorborderradius: "10px", // border radius in pixel for cursor
            scrollspeed: 100, // scrolling speed
            mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
            touchbehavior: false,
            cursorminheight: 20,
            autohidemode: false,
            horizrailenabled: false,
            disableoutline: false,
        });
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(this).attr('href') == '#tab1') {
            $('#account_tab_title').html('Einstellungen');
        } else if ($(this).attr('href') == '#tab2') {
            $('#account_tab_title').html('BESTELLUNGEN');
        } else if ($(this).attr('href') == '#tab3') {
            $('#account_tab_title').html('BEWERTUNGEN');
        } else if ($(this).attr('href') == '#tab4') {
            $('#account_tab_title').html('DEIN RANG');
        }
        if ($('#gt_product_list_wrap2').length) {
            $('#gt_product_list_wrap2').niceScroll({
                cursorcolor: "#f6921e",
                cursorwidth: "8px", // cursor width in pixel (you can also write "5px")
                cursorborder: "1px solid #f6921e", // css definition for cursor border
                cursorborderradius: "10px", // border radius in pixel for cursor
                scrollspeed: 100, // scrolling speed
                mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
                touchbehavior: false,
                cursorminheight: 20,
                autohidemode: false,
                horizrailenabled: true,
                disableoutline: false,
            });
        }
        if ($(this).attr('href') == '#product_tab1') {
            $('#gt_product_list_wrap2').getNiceScroll().hide(); //-- Hide/Show scrollbar on tab change
        } else {
            $('#gt_product_list_wrap2').getNiceScroll().show();
        }
    });
    /*
     ==============================================================
     Jquery Nice Scroll Script
     ==============================================================
     */
    if ($('.gt_forget_hide,#gt_hide_registration').length) {
        $('.gt_forget_hide,#gt_hide_registration').on('click', function () {
            $('#login_in').css("display", "none");
            $('.modal-backdrop.fade.in').css({"opacity": "0", "z-index": "0"});
            $('body').css("padding-right", "0px");
        });
    }

    $(document).on('click', "#modal_trigger", function () {
        $('.social_login').show();
        $('body').addClass('body-position');
        $('.user_login').hide();
        $('.user_register').hide();
    });
    $("#modal_trigger").leanModal({top: 50, overlay: 0.6, closeButton: ".modal_close"});
    $(function () {
        // Calling Login Form
        $("#password_forget").click(function () {
            $(".social_login").hide();
            $('body').removeClass('body-position');
            $(".user_login").show();
            return false;
        });
        // Calling Register Form
        $("#gt_hide_registration").click(function () {
            $(".social_login").hide();
            $('body').removeClass('body-position');
            $(".user_register").show();
            return false;
        });
        // Calling Register Form
        $(".gt_regitration_ext").click(function () {
            $(".user_register").hide();
            $(".social_login").show();
            $('body').addClass('body-position');
            return false;
        });
        $(".forget_password_again").click(function () {
            $(".user_login").hide();
            $(".social_login").show();
            $('body').addClass('body-position');
            return false;
        });
        // Going back to Social Forms
        $(".back_btn").click(function () {
            $(".user_login").hide();
            $(".user_register").hide();
            $(".social_login").show();
            $('body').addClass('body-position');
            $(".header_title").text('Login');
            return false;
        });
    })

    /*------------Tab Script--------------*/
    if ($('#tabs').length) {
        $('#tabs').tab();
    }

    /**
     * Handles click event of powder and pills div and updates hidden value for same
     * @author KU
     */
    $('.product_tab1').click(function () {
        //-- If class is not disabled then only handle click event
        if ($(this).hasClass('pointer_cursor')) {
            $('.product_tab1').removeClass('select');
            $(this).addClass('select');
            var div_id = $(this).attr('id');
            var type = 1;
            var category_id = 0;
            if (div_id == 'cat_powder_div') {
                type = 1;
                $('#hidden_supplement_type').val(1);
            } else if (div_id == 'cat_pill_div') {
                type = 2;
                $('#hidden_supplement_type').val(2);
            }

            if ($('#hidden_searching_type').val() == '2') {
                var parent = $(".goals_div").closest(".select");
                var parent = $(".category_div").closest(".select");
                category_id = (parent.attr('id')).split('_')[1];
            } else {
                category_id = $('#home1_category').val();
            }

            //-- Get all propertis based on its type selection
            $.ajax({
                url: "categories/ajax_get_properties_by_cat_and_type",
                dataType: "json",
                type: "POST",
                data: {category_id: category_id, type: type},
                success: function (response) {
//                    $('#property_slider_div').html(response.response);
                    $('#js_slider_div').html(response.new_slider);

                    //-- set user allowed points
                    $('#number_of_points_allowed').html(response.allowed_points);
                    $('#hidden_number_of_points_allowed').val(response.calculated_allowed_points);
                    $(".jsSlider").slider({
                        range: "min",
                        min: 1,
                        max: 10,
                        value: 1,
                        slide: function (event, ui) {
                            $(this).parent().parent().find('.rightLabel').text(ui.value);
                        },
                        change: function (event, ui) {
                            var total_points = $('#hidden_number_of_points_allowed').val();
                            var total_val = 0;
                            $('.jsSlider').each(function () {
                                total_val += $(this).slider("option", "value");
                            });
                            used_points = total_points - total_val;
                            if (used_points < 0) {
                                new_val = $(this).slider("option", "value") + used_points;
                                $(this).slider("option", "value", new_val);
                                $(this).parent().parent().find('.rightLabel').text(new_val);
                            } else {
                                $('#number_of_points_allowed').html(used_points);
                            }
                        }
                    });

                    //-- Primaary property ids
                    $('#hidden_primary_property_ids').val(response.property_ids);
                    //-- Secondary properties content
                    $('#hidden_secondary_property_ids').val(response.secondary_property_ids);
                    $('#sec_properties_div').html(response.secondary_properties);
                    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
                }
            });

        }
    });
    $('.fancybox').fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    var screen_width = $(window).width();
    if (screen_width > 991) {
        var prod_div_height = $('.s_product_name').parent().height();
        $('.s_product_img').parent().height(prod_div_height);
    }


});
//-- Handles click event of secondary properties
function sec_properties_click(obj) {
    if ($(obj).hasClass('select')) {
        $(obj).removeClass('select');
        $(obj).tooltip('hide');
    } else {
        $(obj).addClass('select');
        $(obj).tooltip('show');

    }
}
/*-----------User Register form validation----------*/

function register_username_validation(uname) {
    $('.username_custom_loader').css('display', 'block');
    $.ajax({
        url: "users/username_validation",
        dataType: "json",
        type: "POST",
        data: {uname: uname},
        success: function (response) {
            $('.username_custom_loader').css('display', 'none');
            if (response == 0) {
                $('#txt_username').css('border', '2px solid #a50606');
                $('#txt_username_error').html('Benutzername existiert bereits.');
                err_cnt = 1;
            } else {
                $('#txt_username').css('border', '1px solid #dadada');
                $('#txt_username_error').html('');
                err_cnt = 0;
            }
        }
    });
}

function register_email_validation(email) {
    $('.email_custom_loader').css('display', 'block');
    $.ajax({
        url: "users/email_validation",
        dataType: "json",
        type: "POST",
        data: {email: email},
        success: function (response) {
            $('.email_custom_loader').css('display', 'none');
            if (response == 0) {
                $('#txt_email').css('border', '2px solid #a50606');
                $('#txt_email_error').html('Email Already exists !');
                err_cnt = 1;
            } else {
                $('#txt_email').css('border', '1px solid #dadada');
                $('#txt_email_error').html('');
                err_cnt = 0;
            }
        }
    });
}

function frontend_register_validation() {
    var username = $('#txt_username').val();
    var email = $('#txt_email').val();
    var pass = $('#txt_pass').val();
    var c_pass = $('#txt_c_pass').val();
    var i_agree = $('#checknox2').val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (username == '') {
        $('#txt_username').css('border', '2px solid #a50606');
        $('#txt_username_error').html('Pflichtfeld');
        err_cnt = 1;
    } else if (username != '') {
        if (username.length < 5) {
            $('#txt_username').css('border', '2px solid #a50606');
            $('#txt_username_error').html('Username must be minimum 5 characters long!');
            err_cnt = 1;
            return false;
        }
        register_username_validation(username);
    } else {
        $('#txt_username').css('border', '1px solid #dadada');
        $('#txt_username_error').html('');
    }

    if (email == '') {
        $('#txt_email').css('border', '2px solid #a50606');
        $('#txt_email_error').html('Pflichtfeld');
        err_cnt = 1;
    } else if (!regex.test(email)) {
        $('#txt_email').css('border', '2px solid #a50606');
        $('#txt_email_error').html('Enter valid email address');
        err_cnt = 1;
    } else {
        $('#txt_email').css('border', '1px solid #dadada');
        $('#txt_email_error').html('');
    }

    if (pass == '') {
        $('#txt_pass').css('border', '2px solid #a50606');
        $('#txt_pass_error').html('Pflichtfeld');
        err_cnt = 1;
    } else if (pass.length < 6) {
        $('#txt_pass').css('border', '2px solid #a50606');
        $('#txt_pass_error').html('Min. 6 characters are required');
        err_cnt = 1;
    } else if (pass.length > 12) {
        $('#txt_pass').css('border', '2px solid #a50606');
        $('#txt_pass_error').html('Max. 12 characters are required');
        err_cnt = 1;
    } else {
        $('#txt_pass').css('border', '1px solid #dadada');
        $('#txt_pass_error').html('');
    }

    if (c_pass == '') {
        $('#txt_c_pass').css('border', '2px solid #a50606');
        $('#txt_c_pass_error').html('Pflichtfeld');
        err_cnt = 1;
    } else if (c_pass != pass) {
        $('#txt_c_pass').css('border', '2px solid #a50606');
        $('#txt_c_pass_error').html('Not matched');
        err_cnt = 1;
    } else {
        $('#txt_c_pass').css('border', '1px solid #dadada');
        $('#txt_c_pass_error').html('');
    }

    if ($('#checknox2').is(":checked")) {
        $('#txt_i_agree_error').html('');
    } else {
        $('#txt_i_agree_error').html('Pflichtfeld');
        err_cnt = 1;
    }

    if (err_cnt == 1) {
        return false;
    } else {
        //-- Disable button to prevent multiple times form submit
        $("#btn_register").prop('disabled', true);
        return true;
    }
}

/*---------// User Register form validation----------*/

function frontend_login_validation() {
    var username = $('#txt_login_username').val();
    var pass = $('#txt_login_pass').val();
    if (username == '') {
        $('#txt_login_username').css('border', '2px solid #a50606');
        $('#txt_login_username_error').html('Pflichtfeld');
        err_cnt = 1;
    } else {
        $('#txt_login_username').css('border', '1px solid #dadada');
        $('#txt_login_username_error').html('');
    }

    if (pass == '') {
        $('#txt_login_pass').css('border', '2px solid #a50606');
        $('#txt_login_pass_error').html('Pflichtfeld');
        err_cnt = 1;
    } else {
        $('#txt_login_pass').css('border', '1px solid #dadada');
        $('#txt_login_pass_error').html('');
    }

    if (err_cnt == 1) {
        return false;
    } else {
        return true;
    }
}
/**
 * Validates forgot password form
 * @author KU
 * @returns boolean
 */
function frontend_forgot_pwd_validation() {
    $('#forgot_pwd_success').html('');
    var username = $('#forgot_user_name').val();
    if (username == '') {
        $('#forgot_user_name').css('border', '2px solid #a50606');
        $('#forgot_user_name_error').html('Pflichtfeld');
    } else {
        $('#forgot_user_name').css('border', '1px solid #dadada');
        $('#forgot_user_name_error').html('');
        $('.forgot_user_name_custom_loader').css('display', 'block');
        $.ajax({
            url: "users/usernameemail_validate",
            dataType: "json",
            type: "POST",
            data: {uname: username},
            async: false,
            success: function (response) {
                $('.forgot_user_name_custom_loader').css('display', 'none');
//                response = response.trim();
                if (response == 0) {
                    $('#forgot_user_name').css('border', '2px solid #a50606');
                    $('#forgot_user_name_error').html('Bitte gültige E-Mail Adresse oder Nutzernamen eingeben.');
                } else if (response == 'inactive') {
                    $('#forgot_pwd_success').html('<div class="alert alert-danger">Your account is disabled by administrator. Please contact administrator to activate it!</div>');
                } else if (response > 0) {
                    $('#forgot_user_name').css('border', '1px solid #dadada');
                    $('#forgot_user_name_error').html('');
                    $('.forgot_user_name_custom_loader').css('display', 'block');
                    $('#forgot_pwd_btn').prop('disabled', true);
                    $.ajax({
                        url: "users/forgot_pwd",
                        dataType: "json",
                        type: "POST",
                        data: {user_id: response},
                        success: function (data) {
                            if (data == 'fail') {
                                $('#forgot_pwd_btn').prop('disabled', false);
                                $('.forgot_user_name_custom_loader').hide();
//                                $('#forgot_user_name_error').text('You can not reset password for this account!');
                                $('#forgot_user_name_error').text('Dein Account konnte nicht gefunden werden.');
                                return false;
                            }
                            $('#forgot_user_name_error').text('');
                            data = data.trim();
                            $('.forgot_user_name_custom_loader').css('display', 'none');
                            $('#forgot_pwd_success').html('<div class="alert alert-success">New password has been sent to your email successfully!</div>');
                            setTimeout(function () {
                                $('#forgot_pwd_success').html('');
                                window.location.reload();
//                                $('.user_login').hide();
                            }, 5000);
                        }
                    });
                }
            }
        });
    }
    return false;
}

/**
 * Delete Account Validation
 * @author KU
 * @param {object} obj
 * @returns {Boolean}
 */
function checkDelete(obj) {
    var del_text = $('#delete_acnt_message').val();
    if (del_text == 'LÖSCHEN') {
        $('#delete_acnt_message_error').html('');
        $('#delete_acnt_message').removeClass('custom_form_error');
        if (confirm('Bist du dir sicher, dass du deinen Account löschen möchtest? Ja/Nein')) {
            return true;
//            window.location.href=$(obj).attr('href');
        } else {
            return false;
        }
    } else {
        $('#delete_acnt_message_error').html('Please write LÖSCHEN to delete your account');
        $('#delete_acnt_message').addClass('custom_form_error');
        return false;
    }
}

/*-----------Front Page--------------*/

/**
 * 
 * This function is used to close particular #home page section
 * @author pav
 */
function close_home(home) {
    $('#' + home).addClass('hide');
    var prev_section = $('#' + home).prev().attr('id');
    $('#' + prev_section + '_close').removeClass('hide');
    var selection_type = $('#hidden_searching_type').val();
    if ((home == 'home2' && selection_type == '2') || (home == 'home4' && selection_type == '1')) {
        $('#home1').addClass('active');
        $(".disable_section").not($('#home1')).removeClass('active');
    } else {
        $('#' + prev_section).addClass('active');
        $(".disable_section").not($('#' + prev_section)).removeClass('active');
    }
//    console.log(prev_section);
    if (prev_section != undefined) {
        $('html, body').animate({scrollTop: $("#" + prev_section).offset().top}, 500);
    } else {
        $('html, body').animate({scrollTop: 0}, 500);
    }
}

/**
 * 
 * This function is called when user select category from dropdown and click on matching
 * @author pav
 */
function home1_start_matching() {
    var home1_cat = $('#home1_category').val();
    if (home1_cat == '') {
        $('#home1_form').css('margin-bottom', '0px');
        $('#home1_category').css('border', '2px solid red');
        $('#home1_category_error').html('Pflichtfeld');
    } else {
        $('#hidden_searching_type').val('1');
        $('#home1_form').css('margin-bottom', '20px');
        $('#home1_category').css('border', '1px solid #f6921e');
        $('#home1_category_error').html('');
        $('#home4').removeClass('hide');
        $('#home4').addClass('active');
        $(".disable_section").not($('#home4')).removeClass('active');
        //-- Store fisrt cat in local session storage
//        sessionStorage.home3_cat = '';
//        sessionStorage.activDiv = '#home4';
//        sessionStorage.home1_cat = home1_cat;
        $.ajax({
            url: "categories/ajax_get_cat_by_id",
            dataType: "json",
            type: "POST",
            data: {category_id: home1_cat},
            success: function (response) {
                //-- Display powder and pill div as per category type
                if (response.type == 3) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section select');
                    $('.product_tab1').addClass('pointer_cursor');
                    $('#cat_powder_div').addClass('select'); //-- Make Pulver as default selection
                    $('#hidden_supplement_type').val(1);
                } else if (response.type == 2) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section pointer_cursor select');
                    $('#cat_pill_div').addClass('pointer_cursor');
                    $('#cat_powder_div').addClass('disabled_cursor disable_section');
                    $('#cat_pill_div').addClass('select'); //-- Make Pill as default selection
                    $('#hidden_supplement_type').val(2);
                } else if (response.type == 1) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section pointer_cursor select');
                    $('#cat_powder_div').addClass('pointer_cursor');
                    $('#cat_pill_div').addClass('disabled_cursor disable_section');
                    $('#cat_powder_div').addClass('select'); //-- Make Pulver as default selection
                    $('#hidden_supplement_type').val(1);
                }
                $('#hidden_primary_property_ids').val(response.property_ids);
                $('#selected_cateogry_img').attr('src', response.cat_image);
                //-- Primary properties content
//                $('#property_slider_div').html(response.response);
                $('#js_slider_div').html(response.new_slider);
                $(".jsSlider").slider({
                    range: "min",
                    min: 1,
                    max: 10,
                    value: 1,
                    slide: function (event, ui) {
                        $(this).parent().parent().find('.rightLabel').text(ui.value);
                    },
                    change: function (event, ui) {
                        var total_points = $('#hidden_number_of_points_allowed').val();
                        var total_val = 0;
                        $('.jsSlider').each(function () {
                            total_val += $(this).slider("option", "value");
                        });
                        used_points = total_points - total_val;
                        if (used_points < 0) {
                            new_val = $(this).slider("option", "value") + used_points;
                            $(this).slider("option", "value", new_val);
                            $(this).parent().parent().find('.rightLabel').text(new_val);
                        } else {
                            $('#number_of_points_allowed').html(used_points);
                        }
                    }
                });
                //-- set user allowed points
                $('#number_of_points_allowed').html(response.allowed_points);
                $('#hidden_number_of_points_allowed').val(response.calculated_allowed_points);
                //-- Secondary properties content
                $('#hidden_secondary_property_ids').val(response.secondary_property_ids);
                $('#sec_properties_div').html(response.secondary_properties);
                $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
            }
        });
        $('html, body').animate({scrollTop: $("#home4").offset().top}, 500);
    }
}

/**
 * 
 * This function is called when user click on free recommendation
 * @author pav
 */
function home1_free_recommendation() {
    $('#hidden_searching_type').val('2');
    $('#home2').removeClass('hide');
    $('#home2').addClass('active');
    $(".disable_section").not($('#home2')).removeClass('active');
    $('html, body').animate({scrollTop: $("#home2").offset().top}, 500);
}

/**
 * 
 * This function is called when user select goals
 * @author pav
 */
function home2_goal_selection(e) {
    var id = $(e).attr('id');
    $('#' + id).addClass('select');
    $(".goals_div").not($('#' + id)).removeClass('select');
}

/**
 * 
 * This function is called when user submitted selected goals
 * @author pav
 */
function home2_goal_submit() {
    if (!($(".goals_div").hasClass('select'))) {
        $('#home2_goal_selection_error').html('Bitte wähle dein Ziel!');
    } else {
        var parent = $(".goals_div").closest(".select");
        var goal_id = (parent.attr("id")).split("_")[1];
        $.ajax({
            url: "home/ajax_get_cat_by_goals",
            dataType: "json",
            type: "POST",
            data: {goal_id: goal_id},
            success: function (response) {
                $('#home3_cat_row').html(response);
            }
        });
        $('#home2_close').addClass('hide');
        $('#home2_goal_selection_error').html('');
        $('#home3').removeClass('hide');
        $('#home3').addClass('active');
        $(".disable_section").not($('#home3')).removeClass('active');
        $('html, body').animate({scrollTop: $("#home3").offset().top}, 500);
    }
}

/**
 * 
 * This function is called when user select category as per his selected goals
 * @author pav
 */
function home3_category_selection(e) {
    var id = $(e).attr('id');
    $('#' + id).addClass('select');
    $(".category_div").not($('#' + id)).removeClass('select');
}

/**
 * 
 * This function is called when user submit his category
 * @author pav
 */
function home3_category_submit() {
    if (!($(".category_div").hasClass('select'))) {
        $('#home3_category_selection_error').html('Bitte wähle deine Kategorie!');
    } else {
        var parent = $(".category_div").closest(".select");
        var category_id = (parent.attr('id')).split('_')[1];
        var category_img = $('#img_' + parent.attr('id')).attr('src');
        $.ajax({
            url: site_url + "categories/ajax_get_cat_by_id",
            dataType: "json",
            type: "POST",
            data: {category_id: category_id},
            success: function (response) {
//                $('#property_slider_div').html(response.response);
                $('#js_slider_div').html(response.new_slider);

                //-- Display powder and pill div as per category type
                if (response.type == 3) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section select');
                    $('.product_tab1').addClass('pointer_cursor');
                    $('#cat_powder_div').addClass('select'); //-- Make Pulver as default selection
                    $('#hidden_supplement_type').val(1);
                } else if (response.type == 2) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section pointer_cursor select');
                    $('#cat_pill_div').addClass('pointer_cursor');
                    $('#cat_powder_div').addClass('disabled_cursor disable_section');
                    $('#cat_pill_div').addClass('select'); //-- Make Pill as default selection
                    $('#hidden_supplement_type').val(2);
                } else if (response.type == 1) {
                    $('.product_tab1').removeClass('disabled_cursor disable_section pointer_cursor select');
                    $('#cat_powder_div').addClass('pointer_cursor');
                    $('#cat_pill_div').addClass('disabled_cursor disable_section');
                    $('#cat_powder_div').addClass('select'); //-- Make Pulver as default selection
                    $('#hidden_supplement_type').val(1);
                }
                $('#hidden_primary_property_ids').val(response.property_ids);

                $(".jsSlider").slider({
                    range: "min",
                    min: 1,
                    max: 10,
                    value: 1,
                    slide: function (event, ui) {
                        $(this).parent().parent().find('.rightLabel').text(ui.value);
                    },
                    change: function (event, ui) {
                        var total_points = $('#hidden_number_of_points_allowed').val();
                        var total_val = 0;
                        $('.jsSlider').each(function () {
                            total_val += $(this).slider("option", "value");
                        });
                        used_points = total_points - total_val;
                        if (used_points < 0) {
                            new_val = $(this).slider("option", "value") + used_points;
                            $(this).slider("option", "value", new_val);
                            $(this).parent().parent().find('.rightLabel').text(new_val);
                        } else {
                            $('#number_of_points_allowed').html(used_points);
                        }
                    }
                });
                //-- set user allowed points
                $('#number_of_points_allowed').html(response.allowed_points);
                $('#hidden_number_of_points_allowed').val(response.calculated_allowed_points);
                //-- Secondary properties content
                $('#hidden_secondary_property_ids').val(response.secondary_property_ids);
                $('#sec_properties_div').html(response.secondary_properties);
                $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
            }
        });
        $('#home3_close').addClass('hide');
        $('#home3_category_selection_error').html('');
        $('#home4').removeClass('hide');
        $('#home4').addClass('active');
        $(".disable_section").not($('#home4')).removeClass('active');
        $('html, body').animate({scrollTop: $("#home4").offset().top}, 500);
        $('#selected_cateogry_img').attr('src', category_img);
    }
}

/**
 * 
 * This function is called when user submit home page
 * @author pav
 */
function home_page_submit() {
    var allowed_points = $('#number_of_points_allowed').html();
    allowed_points = parseInt(allowed_points);

    if (allowed_points == 0) {
        if ($('#hidden_searching_type').val() == '2') {
            var parent = $(".goals_div").closest(".select");
            var goal_id = (parent.attr("id")).split("_")[1];
            var parent = $(".category_div").closest(".select");
            var category_id = (parent.attr('id')).split('_')[1];
        } else {
            var goal_id = '';
            var category_id = $('#home1_category').val();
        }

        $('#hidden_goal_id').val(goal_id);
        $('#hidden_category_id').val(category_id);
        var prim_properties = [];
        /*$('.nstSlider').each(function () {
         prim_properties.push($(this).nstSlider("get_current_max_value"));
         });*/

        //-- Get primary properties value
        $('.jsSlider').each(function () {
            prim_properties.push($(this).slider("option", "value"));
        });
        $('#hidden_primary_properties').val(prim_properties);

        //-- Get secondary properties value
        var sec_properties = [];
        $('.sec_properties').each(function () {
            if ($(this).hasClass('select')) {
                sec_properties.push($(this).attr("data-id"));
            }
        });
        $('#hidden_secondary_properties').val(sec_properties);
        //
//--Display loader
//    loader();
        $('#matching_loader').show();
        $('body').addClass('disable_scroll');
        $('#matching_perent_hidden_spn').val(0);
        timeout_trigger();
        setTimeout(function () {
            $('#home4_form').submit();
        }, 6000);
    } else {
        $('#review_alert_message').html('Bitte vergib alle Punkte um dein Matching zu starten.');
        $('#login_check_modal').modal();
        setTimeout(function () {
            $('#login_check_modal').modal('hide');
        }, 3000);
    }
}

function timeout_trigger() {
    p = parseInt($("#matching_perent_hidden_spn").val());
    $("#matching_perent_spn").html(p);
    if (p != 100) {
        setTimeout('timeout_trigger()', 60);
    }
    p++;
    $("#matching_perent_hidden_spn").val(p);
}

/*-----------// Front Page----------*/


/**
 * Get category details and displayes category details in home 4 section
 * @param {int} category_id
 * @author KU  */
function get_cat_details(category_id) {
    $.ajax({
        url: "home/get_cat_details",
        dataType: "json",
        type: "POST",
        data: {category_id: category_id},
        success: function (response) {
            $('.username_custom_loader').css('display', 'none');
            if (response) {
                $('#cateogry_img').html('<img src="' + cat_img_link + response.icon + '" alt="' + response.name_gr + '">'); //-- Display category image
                //-- Display category properties
                str = '<div class="range_slider default_width mb10">';
                str += '<span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Preis</span>';
                str += '<div class="range_slider_wrap default_width">';
                str += '<div class="leftLabel white"></div>';
                str += '<div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15"    data-cur_max="5">';
                str += '<div class="bar"></div>';
                str += '<div class="leftGrip"></div>';
                str += '<div class="rightGrip"></div>';
                str += '</div>';
                str += '<div class="rightLabel white">10</div>';
                str += '</div>';
                str += '</div>';
            }
        }
    });
}
/**
 * Add product into user cart
 * @param int supplement_id
 * @author KU
 */
function add_to_cart(supplement_id) {
    //-- Check if user is logged in or not
    if (is_user_loggedin != '') {

        $.ajax({
            url: site_url + "cart/add_cart_item",
            dataType: "json",
            type: "POST",
            data: {supplement_id: supplement_id},
            success: function (response) {
                if (response) {
                    $('.cartitem_count').html(response);
                    $('html, body').animate({scrollTop: 0}, 500);
                    /*
                     $('#message_container').html('<div class="alert alert-box success"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><span class="text-semibold">Dein Produkt wurde erfolgreich auf den Merkzettel geschrieben.</span></div>');
                     //-- Hide Success Message
                     $(".alert-box").fadeTo(2000, 500).slideUp(500, function () {
                     $(".alert-box").slideUp(500);
                     });*/
                    $('#success_alert_message').html('Dein Produkt wurde erfolgreich auf den Merkzettel geschrieben.');
                    $('#success_alert_modal').modal();
                    setTimeout(function () {
                        $('#success_alert_modal').modal('hide');
                    }, 3000);
                }
            }
        });
    } else {
        $('#review_alert_message').html('Please login or register to add item to cart');
        $('#login_check_modal').modal();
    }
}

/**
 * Review like and dislike functionality
 * @author PAV
 * @param {string} status
 * @param {int} review_id
 */
function supplement_likes(status, review_id) {
    $('#fakeLoader').show();
    $("#fakeLoader").fakeLoader({
        timeToHide: 4000,
        zIndex: 999, // Default zIndex
        spinner: "spinner3", //Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7' 
        bgColor: "rgba(80,45,5,0.6)", //"rgba(246,146,30,0.6)",//"rgba(51, 49, 49, 0.7)", //Hex, RGB or RGBA colors
    });
    $.ajax({
        url: "supplements/like_dislike_supp_review",
        dataType: "json",
        type: "POST", data: {status: status, review_id: review_id},
        success: function (response) {
            $('#total_likes_' + review_id).html(response.tot_likes);
            $('#total_dislikes_' + review_id).html(response.tot_dislikes);
            $('#like_button_' + review_id).attr('src', response.like_img);
            $('#dislike_button_' + review_id).attr('src', response.dislike_img);
            $('#fakeLoader').hide();
        }
    });
}


/**
 * Add order when user is redirected to amazon page functionality
 * @author SG
 * @param {int} id
 */
function place_order(data) {
    var sup_id = $(data).data('id');
    $.ajax({
        url: site_url + '/order/place_order',
        //            dataType: "json",
        type: "POST",
        data: {sup_id: sup_id},
        success: function (response) {
            if (response == 'success') {
                window.location.href = site_url + '/order/thank_you';
            }
        }
    });
}

/**
 * Redirect to thanks page after placing order
 * @author SG
 */
function submit_thank_you() {
    if ($('#thank_you_content').val() == '') {
        $('#thank_you_content_lbl').html('Please enter text');
        $('#thank_you_content_lbl').show();
    } else {
        $('#thank_you_content_lbl').html('');
        $('#thank_you_content_lbl').hide();
        $('#thank_you_form').submit();
    }
}
/**
 * Redirects user to thank you page on order's amazon link click
 * @returns {undefined}
 */
function redirect_thankyou() {
    window.location.href = site_url + '/order/thank_you';
}

/**
 * Focuses textbox on search button click 
 */
function focusTextBox() {
    $('#typehead_search').focus();
    /*
     var screen_width = $(window).width();
     if (screen_width > 991) {
     $('#typehead_search').focus();
     } else {
     $('#typehead_search').blur();
     }
     */
}



