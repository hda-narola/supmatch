<?php

/**
 * MY_Controller is called by default before every controller.
 * @author pav
 * */
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->cartitem_count = 0;
        $this->load->model(array('users_model', 'cart_model', 'review_model', 'orders_model'));
        $UserCookieData = $this->input->cookie("cookie_user_id");

        if ($UserCookieData != '') {
            $condition = array('id' => base64_decode($UserCookieData));
            $checkUser = $this->users_model->get_all_details(TBL_USERS, $condition);
            if ($checkUser->num_rows() == 1) {
                $userdata = array(
                    'session_user_id' => $checkUser->row()->id,
                    'session_user_name' => $checkUser->row()->nickname,
                    'session_user_email' => $checkUser->row()->email,
                    'session_user_confirm' => $checkUser->row()->is_verified,
                    'login_user_type' => $checkUser->row()->login_user_type
                );
                $this->session->set_userdata($userdata);
            }
        } else {
            $userdata = [
                'session_user_id',
                'session_user_name',
                'session_user_email',
                'session_user_confirm',
                'login_user_type'
            ];
            $this->session->unset_userdata($userdata);
        }
        $user_id = $this->session->userdata('session_user_id');
        $this->user_reviews = array();
        $this->ranks = array();
        //-- If user is logged in then get his cart item count and reviews
        if ($user_id != '') {
            $this->cartitem_count = $this->cart_model->get_cartitems($user_id, 'count');
            $this->user_reviews = $this->review_model->get_user_reviews($user_id);
            $condition = array('status' => 'active');
            $sort = array(
                array('field' => 'rank', 'type' => 'ASC')
            );
            $this->ranks = $this->users_model->get_all_details(TBL_RANK_DETAILS, $condition, $sort)->result_array();
            $this->user_rank = $this->users_model->get_user_rank($user_id);
        }
        //-- Get all active categories from database for add product formular
        $condition = array('status' => 'active');
        $this->add_productcategories = $this->cart_model->get_all_details(TBL_CATEGORIES, $condition)->result_array();

        // Author SG
        if (!empty($user_id)) {
            $order_condition = array('o.user_id' => $user_id);
            $this->orders_user = $this->orders_model->get_all_orders(TBL_ORDERS, $order_condition, 'o.id');
        }
    }

    public function checkLogin($type = '') {
        if ($type == 'ID') {
            return $this->session->userdata('session_user_id');
        } else if ($type == 'N') {
            return $this->session->userdata('session_user_name');
        } else if ($type == 'E') {
            return $this->session->userdata('session_user_email');
        } else if ($type == 'C') {
            return $this->session->userdata('session_user_confirm');
        } else if ($type == 'T') {
            return $this->session->userdata('login_user_type');
        }
    }

    /**
     * Check unique name for english
     * Called throught ajax
     * @author KU
     */
    public function checkUniqueNameEn($table = NULL, $id = NULL) {
        $this->load->model('my_model');
        $name_en = trim($this->input->get('name_en'));
        $condition = 'name_en="' . $name_en . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $user = $this->my_model->check_unique_name($table, $condition);
        if ($user) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    /**
     * Check unique flavour for german
     * Called throught ajax
     * @author KU
     */
    public function checkUniqueNameGr($table = NULL, $id = NULL) {
        $this->load->model('my_model');
        $name_gr = trim($this->input->get('name_gr'));
        $condition = 'name_gr="' . $name_gr . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $user = $this->my_model->check_unique_name($table, $condition);
        if ($user) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */