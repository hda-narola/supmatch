<?php

/**
 * MY_Model is called by default before every model.
 * @author pav
 * */
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $defaultLanguage = $selectedLanguage = 'gr';
        $this->session->set_userdata('lang', $selectedLanguage);
        $lang = $this->session->get_userdata('lang');
        $filePath = base_url() . "/application/language/" . $selectedLanguage . "/" . $selectedLanguage . "_lang.php";
        if ($selectedLanguage != '') {
            if (!(is_file($filePath))) {
                $this->lang->load($defaultLanguage, $defaultLanguage);
            } else {
                $this->lang->load($selectedLanguage, $selectedLanguage);
            }
        } else {
            $this->lang->load($defaultLanguage, $defaultLanguage);
        }
    }

    /**
     * This function used to add or update records in particular table based on condition. 
     * @param String $mode
     * @param String $table
     * @param Array $dataArr        	
     * @param Array $condition
     * @return Integer $affected_row
     * @author pav
     * */
    public function common_insert_update($mode = '', $table = '', $dataArr = '', $condition = '') {
        if ($mode == 'insert') {
            $this->db->insert($table, $dataArr);
        } else if ($mode == 'update') {
            $this->db->where($condition);
            $this->db->update($table, $dataArr);
        }
        $affected_row = $this->db->affected_rows();
        return $affected_row;
    }

    /**
     * This function returns the table contents based on data. 
     * @param String $table
     * @param Array $condition        	
     * @param Array $sortArr
     * @return Array
     * @author pav
     * */
    public function get_all_details($table = '', $condition = '', $sortArr = '', $limitArr = '') {
        if ($sortArr != '' && is_array($sortArr)) {
            foreach ($sortArr as $sortRow) {
                if (is_array($sortRow)) {
                    $this->db->order_by($sortRow ['field'], $sortRow ['type']);
                }
            }
        }
        if ($limitArr != '') {
            return $this->db->get_where($table, $condition, $limitArr['l1'], $limitArr['l2']);
        } else {
            return $this->db->get_where($table, $condition);
        }
    }

    /**
     * Check name exist or not in table 
     * @param string $condition 
     * @return array
     */
    public function check_unique_name($table, $condition) {
        $this->db->where($condition);
        $this->db->where('status', 'active');
        $query = $this->db->get($table);
        return $query->row_array();
    }

    /**
     * This function used to send email.
     * @param Array $email_values
     * @author pav
     * */
    public function common_email_send($email_values = array()) {
        $type = $email_values ['mail_type'];
        $subject = $email_values ['subject_message'];
        $to = $email_values ['to_mail_id'];
        $from = $email_values ['from_mail_id'];
        $from_name = $email_values ['from_mail_name'];
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype($type);
        $this->email->subject($subject);
        $this->email->from($from, $from_name);
        $this->email->to($to);
        if ($email_values['cc_mail_id'] != '') {
            $this->email->cc($email_values['cc_mail_id']);
        }
        $this->email->message(stripslashes($email_values ['body_messages']));
        if (!$this->email->send()) {
            echo $this->email->print_debugger();
        }
        return 'success';
    }

    /**
     * This function used to update verified details after clicking verification link in email.
     * @param Array $condition
     * @author pav
     * */
    public function account_activation($condition = '') {
        $this->db->select('u.*');
        $this->db->from(TBL_USERS . ' as u');
        $this->db->where($condition);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        if ($num_rows == 1) {
            $dataArr = array(
                'is_verified' => 1
            );
            $this->db->where($condition);
            $this->db->update(TBL_USERS . ' as u', $dataArr);
            return 1;
        } else {
            return 0;
        }
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */