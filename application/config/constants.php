<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/**
 * Contants for Tables
 */
define('TBL_CATEGORIES', 'categories');
define('TBL_CATEGORIES_GOAL_LINK', 'categories_goals_link');
define('TBL_CATEGORIES_LIQUIDS_LINK', 'categories_liquids_link');
define('TBL_CATEGORIES_PROPERTIES_LINK', 'categories_properties_link');
define('TBL_CATEGORIES_SECONDARYPROPERTIES_LINK', 'categories_secondaryproperties_link');
define('TBL_FLAVOURS', 'flavours');
define('TBL_GOALS', 'goals');
define('TBL_LIQUIDS', 'liquids');
define('TBL_PRICE_PROPERTY', 'price_property');
define('TBL_PROPERTIES', 'properties');
define('TBL_PROPERTIES_RATING_LINK', 'properties_rating_link');
define('TBL_RANK_DETAILS', 'rank_details');
define('TBL_ROLES', 'roles');
define('TBL_SHOPPING_CART', 'shopping_cart');
define('TBL_SUPPLEMENTS', 'supplements');
define('TBL_SUPPLEMENTS_FLAVOURS_LINK', 'supplements_flavours_link');
define('TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK', 'supplements_secondaryproperties_link');
define('TBL_SUPPLEMENT_PROPERTIES_AVERAGE_RATINGS', 'supplement_properties_average_ratings');
define('TBL_SUPPLEMENT_RATING', 'supplement_ratings');
define('TBL_SUPPLEMENT_RATING_LIKE', 'supplement_ratings_likes');
define('TBL_USERS', 'users');
define('TBL_ORDERS', 'orders');


/**
 * Contants for Images
 */
define('USERS_IMG', 'uploads/users/');
define('RANKS_IMG', 'uploads/ranks/');
define('CATEGORIES_IMG', 'uploads/categories/');
define('SUPPLEMENTS_IMG', 'uploads/supplements/');


/**
 * Constants for Email 
 */
define('EMAIL_FROM', '');
define('EMAIL_FROM_NAME', '');
define('SUPPMATCH_EMAIL', 'business@supmatch.de');
define('FEEDBACK_EMAIL', 'feedback@supmatch.de');
define('ADDPRODUCT_EMAIL', 'addproduct@supmatch.de');
define('CONTACT_SUPPMATCHEMAIL', 'support@supmatch.de');

/**
 * Constants for cron Amazon API (itemLookup)
 */
defined('AWS_API_KEY') or define('AWS_API_KEY', 'AKIAJOAMIH2FPHYZFDDQ');
defined('AWS_API_SECRET_KEY') or define('AWS_API_SECRET_KEY', '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');
defined('AWS_ASSOCIATE_TAG') or define('AWS_ASSOCIATE_TAG', 'hilfespielsuc-21');




define("SUCCESS", "success");
define("FAILED", "failed");

