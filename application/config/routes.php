<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'home';

//--Admin route
$route['admin'] = 'admin/login';
$route['admin/logout'] = 'admin/login/logout'; //--- Logout route
$route['admin/change_password'] = 'admin/home/change_password'; //--- Change password route

$route['admin/users'] = 'admin/users/display_users';
$route['admin/add_users'] = 'admin/users/add_users';
$route['admin/edit_users/(:any)'] = 'admin/users/edit_users/$1';
$route['admin/delete_users/(:any)'] = 'admin/users/delete_users/$1';
$route['activate/(:any)/(:any)'] = 'admin/users/account_activation/$1/$2'; //-- Admin side activation
$route['active_account/(:any)/(:any)'] = 'users/account_activation/$1/$2'; //-- User side account activation

$route['admin/ranks'] = 'admin/ranks/display_ranks';
$route['admin/add_ranks'] = 'admin/ranks/add_ranks';
$route['admin/edit_ranks/(:any)'] = 'admin/ranks/edit_ranks/$1';
$route['admin/delete_ranks/(:any)'] = 'admin/ranks/delete_ranks/$1';

$route['admin/liquids/add'] = 'admin/liquids/edit'; //--- Add liquid route 
$route['admin/flavours/add'] = 'admin/flavours/edit'; //--- Add flavour route
$route['admin/categories/add'] = 'admin/categories/edit'; //--- Add category route
$route['admin/properties/add'] = 'admin/properties/edit'; //--- Add Property route
$route['admin/secondary_properties'] = 'admin/properties/secondary'; //--- Secondary property routes
$route['admin/secondary_properties/add'] = 'admin/properties/secondary_edit'; //--- Add Secondary property route
$route['admin/secondary_properties/edit/(:any)'] = 'admin/properties/secondary_edit/$1'; //--- Edit Secondary property route
$route['admin/secondary_properties/delete/(:any)'] = 'admin/properties/secondary_delete/$1'; //--- Edit Secondary property route
$route['admin/goals/add'] = 'admin/goals/edit'; //--- Add goal route

$route['admin/supplements'] = 'admin/supplements/display_supplements';
$route['admin/add_supplements'] = 'admin/supplements/add_supplements';
$route['admin/edit_supplements/(:any)'] = 'admin/supplements/edit_supplements/$1';
$route['admin/delete_supplements/(:any)'] = 'admin/supplements/delete_supplements/$1';

$route['admin/businesses/add'] = 'admin/businesses/edit'; //--- Add business route 
$route['admin/liquids/add'] = 'admin/liquids/edit'; //--- Add business route 
$route['admin/businesses/edit_icp/(:any)/(:any)'] = 'admin/businesses/add_icp/$1/$2'; //--- Edit ICP route
$route['admin/hotels/add/(:any)'] = 'admin/hotels/edit/$1'; //--- Edit Hotel route
//-- Front-End Routes
$route['register'] = 'users/user_register';
$route['login'] = 'users/user_login';
$route['logout'] = 'users/user_logout';
$route['login/facebook'] = 'sociallogin/fb_redirect';
$route['social/facebook_callback'] = 'sociallogin/facebook_callback';
$route['setting'] = 'users/general_settings';
$route['setting/change_password'] = 'users/change_password';
$route['pages/hows_work'] = 'pages/hows_work';
$route['pages/about_us'] = 'pages/about_us';
$route['supplements/listing'] = 'supplements/display_supplements';
//$route['pages/' . urlencode("für")] = 'pages/hows_work';
//$route['pages/' . urlencode(utf8_encode('für'))] = 'pages/hows_work';

//-- Crone Routes
$route['crone/get_supplements_price'] = 'crone/get_supplements_price';


//--Business route
$route['business/profile'] = 'business/home/profile'; //-- Business Profile route
$route['business/promo_images'] = 'business/home/promo_images'; //-- Business Promo Feature Images route
$route['business/change_password'] = 'business/home/change_password'; //--- Change password route
$route['business/account_profile'] = 'business/home/account_profile'; //--- Update Profile route
$route['business/icps/add'] = 'business/icps/edit'; //--- Add Icp route 
$route['business/hotels/add/(:any)'] = 'business/hotels/edit/$1'; //--- Edit Hotel route

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
