<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <?php
                if (isset($category_data))
                    echo '<i class="icon-pencil3"></i>';
                else
                    echo '<i class="icon-plus-circle2"></i>';
                ?> 
                <span class="text-semibold"><?php echo $heading; ?></span>
            </h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/categories'); ?>"><i class="icon-tree6"></i> Categories</a></li>
            <li class="active"><?php echo $heading; ?></li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <div class="alert alert-success hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('success') ?></strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) {
                ?>
                <div class="alert alert-danger hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('error') ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php echo validation_errors(); ?>
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal" id="category_info" action="" enctype="multipart/form-data" method="post">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">English Language</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse" class=""></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Icon <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <div class="media no-margin-top">
                                    <div class="media-left" id="image_preview_div">
                                        <?php
                                        $required = 'required="required"';
                                        if (isset($category_data) && $category_data['icon']) {
                                            $required = '';
                                            ?>
                                            <img src="<?php echo CATEGORIES_IMG . $category_data['icon'] ?>" style="width: 58px; height: 58px; border-radius: 2px;" alt="">
                                        <?php } else { ?>
                                            <img src="assets/admin/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;" alt="">
                                        <?php } ?>
                                    </div>
                                    <div class="media-body">
                                        <input type="file" name="icon" id="icon" class="file-styled-primary" onchange="readURL(this);" <?php echo $required ?>>
                                        <span class="help-block">Accepted formats: png, jpg. Max file size 2Mb</span>
                                    </div>
                                </div>
                                <?php echo '<label id="icon_error" class="validation-error-label" for="icon">' . form_error('icon') . '</label>'; ?>
                                <?php
                                if (isset($icon_validation))
                                    echo '<label id="logo-error" class="validation-error-label" for="logo">' . $icon_validation . '</label>';
                                ?>
                            </div>
                        </div>
                        <!-- Name : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <input type="text" name="name_en" id="name_en" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($category_data)) ? $category_data['name_en'] : set_value('name_en'); ?>">
                                <?php echo '<label id="name_en_error" class="validation-error-label" for="name_en">' . form_error('name_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Name : English -->

                        <!-- Description : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <textarea name="desc_en" id="desc_en" class="form-control" required="required" placeholder="Enter Description" aria-required="true" rows="5" cols="5" ><?php echo (isset($category_data)) ? $category_data['desc_en'] : set_value('desc_en'); ?></textarea>
                                <?php echo '<label id="desc_en_error" class="validation-error-label" for="desc_en">' . form_error('desc_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Description : English -->

                        <div class="form-group">
                            <label class="control-label col-lg-2">Type <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <select class="select" name="category_type" id="category_type" required="required">
                                    <?php
                                    $powder_selected = '';
                                    $pill_selected = '';
                                    $both_selected = 'selected';
                                    if (isset($category_data)) {
                                        if ($category_data['type'] == 3) {
                                            $both_selected = 'selected';
                                        } else if ($category_data['type'] == 2) {
                                            $pill_selected = 'selected';
                                            $both_selected = '';
                                        } else if ($category_data['type'] == 1) {
                                            $powder_selected = 'selected';
                                            $both_selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="1" <?php echo $powder_selected; ?>>Powder</option>
                                    <option value="2" <?php echo $pill_selected; ?>>Pill</option>
                                    <option value="3"<?php echo $both_selected; ?>>Both</option>
                                </select>
                                <?php echo '<label id="category_type_error" class="validation-error-label" for="category_type">' . form_error('category_type') . '</label>'; ?>
                            </div>
                        </div>
                        <?php
                        $powder_div_style = '';
                        $pill_div_style = '';
                        if (isset($category_data)) {
                            if ($category_data['type'] == 1) {
                                $powder_div_style = '';
                                $pill_div_style = 'style="display:none"';
                            } else if ($category_data['type'] == 2) {
                                $powder_div_style = 'style="display:none"';
                                $pill_div_style = '';
                            }
                        }
                        ?>
                        <div class="col-lg-6" id="powder_div" <?php echo $powder_div_style ?>>
                            <fieldset class="content-group">
                                <legend class="text-bold">Powder</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Primary Properties <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="property_ids_1[]">
                                            <?php
                                            foreach ($properties as $property) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['property_ids'] != '') {
                                                    $property_ids = explode(',', $category_data['property_ids']);
                                                    if (in_array($property['id'], $property_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $property['id'] . "' " . $selected . ">" . $property['name_en'] . "/" . $property['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Secondary Properties</label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="secondaryproperty_ids_1[]">
                                            <?php
                                            foreach ($secondary_properties as $property) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['secondaryproperty_ids'] != '') {
                                                    $property_ids = explode(',', $category_data['secondaryproperty_ids']);
                                                    if (in_array($property['id'], $property_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $property['id'] . "' " . $selected . ">" . $property['name_en'] . "/" . $property['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Liquids <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="liquid_ids_1[]">
                                            <?php
                                            foreach ($liquids as $liquid) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['liquid_ids'] != '') {
                                                    $liquid_ids = explode(',', $category_data['liquid_ids']);
                                                    if (in_array($liquid['id'], $liquid_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $liquid['id'] . "' " . $selected . ">" . $liquid['name_en'] . "/" . $liquid['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-lg-6" id="pill_div" <?php echo $pill_div_style ?>>
                            <fieldset class="content-group">
                                <legend class="text-bold">Pill</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Primary Properties <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="property_ids_2[]">
                                            <?php
                                            foreach ($properties as $property) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['property_ids_2'] != '') {
                                                    $property_ids = explode(',', $category_data['property_ids_2']);
                                                    if (in_array($property['id'], $property_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $property['id'] . "' " . $selected . ">" . $property['name_en'] . "/" . $property['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Secondary Properties</label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="secondaryproperty_ids_2[]">
                                            <?php
                                            foreach ($secondary_properties as $property) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['secondaryproperty_ids_2'] != '') {
                                                    $property_ids = explode(',', $category_data['secondaryproperty_ids_2']);
                                                    if (in_array($property['id'], $property_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $property['id'] . "' " . $selected . ">" . $property['name_en'] . "/" . $property['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
<!--                                <div class="form-group">
                                    <label class="control-label col-lg-3">Liquids <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select multiple="multiple" class="select" name="liquid_ids_2[]">
                                            <?php
                                            foreach ($liquids as $liquid) {
                                                $selected = '';
                                                if (isset($category_data) && $category_data['liquid_ids_2'] != '') {
                                                    $liquid_ids = explode(',', $category_data['liquid_ids_2']);
                                                    if (in_array($liquid['id'], $liquid_ids)) {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                echo "<option value='" . $liquid['id'] . "' " . $selected . ">" . $liquid['name_en'] . "/" . $liquid['name_gr'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>-->
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">In German Language</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse" class=""></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <!-- Name : German -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <input type="text" name="name_gr" id="name_gr" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($category_data)) ? $category_data['name_gr'] : set_value('name_gr'); ?>">
                                <?php echo '<label id="name_gr_error" class="validation-error-label" for="name_gr">' . form_error('name_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Name : German -->

                        <!-- Description : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <textarea name="desc_gr" id="desc_gr" class="form-control" required="required" placeholder="Enter Description" aria-required="true" rows="5" cols="5" ><?php echo (isset($category_data)) ? $category_data['desc_gr'] : set_value('desc_gr'); ?></textarea>
                                <?php echo '<label id="desc_gr_error" class="validation-error-label" for="desc_gr">' . form_error('desc_gr') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Description : English -->
                        <!-- Switch single -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Status </label>
                            <div class="col-lg-6">
                                <div class="checkbox checkbox-switch">
                                    <label>
                                        <input type="checkbox" name="status" id="status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php
                                        if (isset($category_data)) {
                                            if ($category_data['status'] == 'active') {
                                                echo 'checked';
                                            } else {
                                                echo '';
                                            }
                                        } else {
                                            echo 'checked';
                                        }
                                        ?>>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- /switch single -->
                    </div>
                </div>
                <div class="text-left">
                    <button class="btn btn-success" type="submit" id="btn_submit">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    // Display the preview of image on Catedory image icon selection
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var html = '<img src="' + e.target.result + '" style="width: 58px; height: 58px; border-radius: 2px;" alt="">';
                $('#image_preview_div').html(html);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    remoteEnURL = site_url + "admin/categories/checkUniqueNameEn/<?php echo $table ?>";
    remoteGrURL = site_url + "admin/categories/checkUniqueNameGr/<?php echo $table ?>";
<?php if (isset($category_data)) { ?>
        var liquid_id = '<?php echo $category_data['id'] ?>';
        remoteEnURL += "/" + liquid_id;
        remoteGrURL += "/" + liquid_id;
<?php } ?>
    var validator = $("#category_info").validate({
        ignore: 'input[type=hidden], .select2-search__field, #status', // ignore hidden fields
        errorClass: 'validation-error-label', successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            //            icon: "required",
            //            price_property: "required",
            name_en: {
                required: true,
                remote: remoteEnURL
            },
            desc_en: {
                required: true,
                maxlength: 200
            },
            name_gr: {
                required: true,
                remote: remoteGrURL
            },
            desc_gr: {
                required: true,
                maxlength: 200
            }
        },
        messages: {
            name_en: {
                remote: $.validator.format("Category name already exist!")
            },
            name_gr: {
                remote: $.validator.format("Category name already exist!")
            },
        }
    });

    //-- Hide show div on category type change
    $('#category_type').change(function () {
        if ($(this).val() == 1) {
            $('#pill_div').hide();
            $('#powder_div').show();
        } else if ($(this).val() == 2) {
            $('#powder_div').hide();
            $('#pill_div').show();
        } else {
            $('#powder_div').show();
            $('#pill_div').show();
        }
    });
</script>
