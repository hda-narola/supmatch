<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-tree6"></i> <span class="text-semibold">All Categories</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Categories</li>
        </ul>
    </div>
</div>
<div class="content">

    <?php
    if ($this->session->flashdata('success')) {
        ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) {
        ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>

    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="<?php echo site_url('admin/categories/add'); ?>" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Category</a>
        </div>
        <table class="table table-bordered table-hover datatable-highlight" id="categories_tbl">
            <thead>
                <tr>
                    <th># Sr No.</th>
                    <th>Icon</th>
                    <th>Name</th>
                    <th>Primary Properties</th>
                    <th>Secondary Properties</th>
                    <th>Liquids</th>
                    <!--<th>Type</th>-->
                    <th>Status</th>
                    <th>Created</th>
                    <th class="text-center" style="width:9% !important">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($categories as $category) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <img src='<?php echo ($category['icon'] != '') ? CATEGORIES_IMG . $category['icon'] : 'default.jpg'; ?>' class='img-circle img-sm'>
                        </td>
                        <td>
                            <?php echo "<b>English : </b>" . $category['name_en'] . "<br><b>German  : </b>" . $category['name_gr']; ?>
                        </td>
                        <td>
                            <?php
                            if ($category['type'] == 3) {
                                echo "<b>Powder </b>:" . $category['properties'] . "<br/>";
                                echo "<b>Pill </b>:" . $category['properties2'];
                            } else if ($category['type'] == 2) {
                                echo "<b>Pill </b>:" . $category['properties2'] . "<br/>";
                            } else if ($category['type'] == 1) {
                                echo "<b>Powder </b>:" . $category['properties'] . "<br/>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($category['type'] == 3) {
                                echo "<b>Powder </b>:" . $category['secondary_properties'] . "<br/>";
                                echo "<b>Pill </b>:" . $category['secondary_properties2'];
                            } else if ($category['type'] == 2) {
                                echo "<b>Pill </b>:" . $category['secondary_properties2'] . "<br/>";
                            } else if ($category['type'] == 1) {
                                echo "<b>Powder </b>:" . $category['secondary_properties'] . "<br/>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($category['type'] == 3) {
                                if(!empty($category['liquids'])){
                                    echo "<b>Powder </b>:" . $category['liquids'] . "<br/>";
                                }
                                if(!empty($category['liquids2'])){
                                    echo "<b>Pill </b>:" . $category['liquids2'];
                                }
                            } else if ($category['type'] == 2) {
                                if(!empty($category['liquids2'])){
                                    echo "<b>Pill </b>:" . $category['liquids2'] . "<br/>";
                                }
                            } else if ($category['type'] == 1) {
                                if(!empty($category['liquids'])){
                                    echo "<b>Powder </b>:" . $category['liquids'] . "<br/>";
                                }
                            }
                            ?>
                        </td>
    <!--                        <td>
                        <?php
                        if ($category['type'] == 1)
                            echo "Powder";
                        elseif ($category['type'] == 2)
                            echo "Pill";
                        elseif ($category['type'] == 3)
                            echo "Powder & Pill";
                        ?>
                        </td>-->
                        <td>
                            <?php
                            if ($category['status'] == 'active') {
                                echo "<span class='label label-success'>Active</span>";
                            } else if ($category['status'] == 'inactive') {
                                echo "<span class='label label-default'>Inactive</span>";
                            } else if ($category['status'] == 'deleted') {
                                echo "<span class='label label-danger'>Deleted</span>";
                            }
                            ?>
                        </td>
                        <td><?php echo $category['created']; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/categories/edit/' . $category['id']) ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Category"><i class="icon-pencil7"></i></a>
                            <a href="<?php echo site_url('admin/categories/delete/' . $category['id']) ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Category"><i class="icon-trash"></i></a>
                        </td>
                    </tr>

                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }

    /*
     $(function () {
     var table = $('.datatable-basic').dataTable({
     processing: true,
     serverSide: true,
     language: {
     search: '<span>Filter:</span> _INPUT_',
     lengthMenu: '<span>Show:</span> _MENU_',
     paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'},
     //                searchPlaceholder: "Type name,address,user"
     },
     dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
     order: [[4, "desc"]],
     ajax: site_url + 'admin/categories/get_categories',
     columns: [
     {
     data: "sr_no",
     visible: true,
     sortable: false,
     },
     {
     data: "name_en",
     visible: true,
     },
     {
     data: "name_gr",
     visible: true,
     },
     {
     data: "category",
     visible: true,
     render: function (data, type, full, meta) {
     if (data == '' || data == null) {
     return '--';
     } else {
     return data;
     }
     }
     },
     {
     data: "created",
     visible: true,
     },
     {
     data: "status",
     visible: true,
     searchable: false,
     sortable: false,
     render: function (data, type, full, meta) {
     action = '';
     action += '<a href="' + site_url + 'admin/categories/edit/' + full.id + '" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Business"><i class="icon-pencil7"></i></a>';
     action += '&nbsp;&nbsp;<a href="' + site_url + 'admin/categories/delete/' + full.id + '" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Business"><i class="icon-trash"></i></a>';
     return action;
     }
     },
     ]
     });
     
     $('.dataTables_length select').select2({
     minimumResultsForSearch: Infinity,
     width: 'auto'
     });
     
     }); */
</script>