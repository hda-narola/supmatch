<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <?php
                if (isset($goal_data))
                    echo '<i class="icon-pencil3"></i>';
                else
                    echo '<i class="icon-plus-circle2"></i>';
                ?> 
                <span class="text-semibold"><?php echo $heading; ?></span>
            </h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/goals'); ?>"> <img src="assets/admin/images/crosshairblack.png"> Goals</a></li>
            <li class="active"><?php echo $heading; ?></li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <div class="alert alert-success hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('success') ?></strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) {
                ?>
                <div class="alert alert-danger hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('error') ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal" id="goal_info" action="" enctype="multipart/form-data" method="post">

                <!-- Name : English -->
                <div class="form-group">
                    <label class="control-label col-lg-2">Name (English)<span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="name_en" id="name_en" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($goal_data)) ? $goal_data['name_en'] : set_value('name_en'); ?>">
                        <?php echo '<label id="name_en_error" class="validation-error-label" for="name_en">' . form_error('name_en') . '</label>'; ?>
                    </div>
                </div>
                <!-- /Name : English -->
                <!-- Name : German -->
                <div class="form-group">
                    <label class="control-label col-lg-2">Name (German)<span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="name_gr" id="name_gr" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($goal_data)) ? $goal_data['name_gr'] : set_value('name_gr'); ?>">
                        <?php echo '<label id="name_gr_error" class="validation-error-label" for="name_gr">' . form_error('name_en') . '</label>'; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Categories <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <select multiple="multiple" name="category_ids[]" id="category_ids">
                            <?php
                            foreach ($categories as $category) {
                                $selected = '';
                                if (isset($goal_data) && $goal_data['category_ids'] != '') {
                                    $category_ids = explode(',', $goal_data['category_ids']);
                                    if (in_array($category['id'], $category_ids)) {
                                        $selected = 'selected';
                                    }
                                }
                                echo "<option value='" . $category['id'] . "' " . $selected . ">" . $category['name_en'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!-- Switch single -->
                <div class="form-group">
                    <label class="control-label col-lg-2">Status </label>
                    <div class="col-lg-6">
                        <div class="checkbox checkbox-switch">
                            <label>
                                <input type="checkbox" name="status" id="status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php
                                if (isset($goal_data)) {
                                    if ($goal_data['status'] == 'active') {
                                        echo 'checked';
                                    } else {
                                        echo '';
                                    }
                                } else {
                                    echo 'checked';
                                }
                                ?>>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- /switch single -->
                <!-- /Name : German -->
                <div class="text-left">
                    <button class="btn btn-success" type="submit" id="btn_submit">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    remoteEnURL = site_url + "admin/goals/checkUniqueNameEn/<?php echo $table ?>";
    remoteGrURL = site_url + "admin/goals/checkUniqueNameGr/<?php echo $table ?>";
<?php if (isset($goal_data)) { ?>
        var goal_id = '<?php echo $goal_data['id'] ?>';
        remoteEnURL += "/" + goal_id;
        remoteGrURL += "/" + goal_id;
<?php } ?>
    var validator = $("#goal_info").validate({
        ignore: 'input[type=hidden], .select2-search__field, #status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            name_en: "required",
            name_gr: "required",
            'category_ids[]': {
                required: true,
                minlength: 4
            }

        },
        messages: {
            name_en: {
                remote: $.validator.format("Goal name already exist!")
            },
            name_gr: {
                remote: $.validator.format("Goal name already exist!")
            },
            'category_ids[]': {
                minlength: "Please select 4 categoires"
            }
        }
    });
    // Maximum selection
    $("#category_ids").select2({
        maximumSelectionLength: 4
    });
</script>
