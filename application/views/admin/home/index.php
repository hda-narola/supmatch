<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home') ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <div class="alert alert-success hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('success') ?></strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) {
                ?>
                <div class="alert alert-danger hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('error') ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--    <div class="row">
            <div class="col-lg-4">
                 Total Businesses 
                <div class="panel bg-teal-400">
                    <div class="panel-body">
                        <a href="<?php echo site_url('admin/businesses') ?>" style="color: white">
                            <h3 class="no-margin"><?php echo $businesses; ?></h3>
                            <i class="icon-office"></i> Businesses
                        </a>
                    </div>
                </div>
                 /Total Businesses 
            </div>
    
            <div class="col-lg-4">
                 Total Products 
                <div class="panel bg-pink-400">
                    <div class="panel-body">
                        <a href="<?php echo site_url('admin/payments') ?>"  style="color: white">
                            <h3 class="no-margin">0</h3>
                            <i class="icon-lan2"></i> Payments
                            <i class="icon-list3"></i> Payments
                        </a>
                    </div>
                </div>
                 /Total Products 
            </div>
    
            <div class="col-lg-4">
                 Total Feedbacks 
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <a href="<?php echo site_url('admin/settings') ?>"  style="color: white">
                            <h3 class="no-margin"></h3>
                            <i class="icon-cog3"></i> settings
                        </a>
                    </div>
                </div>
                 /Total Feedbacks 
            </div>
        </div>-->
</div>