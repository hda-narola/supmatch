<?php 
	if(isset($userdata)){
		$form_action = 'admin/edit_users/'.$userdata[0]['id'];
	}else{
		$form_action = 'admin/add_users';
	}
?>
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span> - Add New</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="admin/users">Users</a></li>
			<li class="active">Add New</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
	<!-- Form validation -->
	<div class="panel panel-flat">
		<div class="panel-body">
			<form class="form-horizontal" id="add_users_form" action="<?php echo $form_action; ?>" enctype="multipart/form-data" method="post">
				<fieldset class="content-group">
					<legend class="text-bold">Basic Details</legend>

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Username / Nickname <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="txt_username" id="txt_username" class="form-control" required="required" placeholder="Please enter username / nickname" value="<?php echo (isset($userdata)) ? $userdata[0]['nickname'] : set_value('txt_username'); ?>">
							<?php echo '<label id="txt_username_error" class="validation-error-label" for="txt_username">' . form_error('txt_username') . '</label>'; ?>
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Email field -->
					<div class="form-group">
						<label class="control-label col-lg-3">Email <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="email" name="txt_email" id="txt_email" class="form-control" required="required" placeholder="Please enter email address" value="<?php echo (isset($userdata)) ? $userdata[0]['email'] : set_value('txt_email'); ?>">
							<?php echo '<label id="txt_email_error" class="validation-error-label" for="txt_email">' . form_error('txt_email') . '</label>'; ?>
						</div>
					</div>
					<!-- /email field -->


					<!-- Input with icons -->
					<div class="form-group has-feedback">
						<label class="control-label col-lg-3">Password <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="password" name="txt_password" id="txt_password" class="form-control" required="required" placeholder="Please enter password" value="<?php if(isset($userdata)){ echo $userdata[0]['password']; }else{ echo $random_string; } ?>" readonly>
							<div class="form-control-feedback">
								<i class="icon-key"></i>
							</div><?php echo '<label id="txt_password_error" class="validation-error-label" for="txt_password">' . form_error('txt_password') . '</label>'; ?>

						</div>
					</div>
					<!-- /input with icons -->


					<!-- Input group -->
					<div class="form-group">
						<label class="col-lg-3 control-label">Profile Pic </label>
						<div class="col-lg-9">
							<div class="media no-margin-top">
								<?php 
									if(isset($userdata)){
										if($userdata[0]['image']!=''){
								?>
											<div class="media-left">
												<a href="javascript:void(0);"><img src="<?php echo USERS_IMG.$userdata[0]['image']; ?>" style="width: 58px; height: 58px;" class="img-rounded" alt=""></a>
											</div>
								<?php 
										}
									}
								?>


								<div class="media-body">
									<input type="file" class="file-styled-primary" name="txt_profile_pic" id="txt_profile_pic">
									<span class="help-block">Accepted formats: gif, png, jpg.</span>
								</div>
							</div>
						</div>
					</div>
					<!-- /input group -->

					<!-- Switch single -->
					<div class="form-group">
						<label class="control-label col-lg-3">Status <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<div class="checkbox checkbox-switch">
								<label>
									<input type="checkbox" name="txt_status" id="txt_status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php if(isset($userdata)){ if($userdata[0]['status']=='active'){ echo 'checked'; }}else{ echo 'checked'; } ?>>
								</label>
							</div>
						</div>
					</div>
					<!-- /switch single -->

				</fieldset>

				<div class="text-left">
					<input type="hidden" name="hidden_user_id" value="<?php if(isset($userdata)){ echo $userdata[0]['id']; } ?>">
					<button type="submit" class="btn btn-success">Submit <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
	<!-- /form validation -->
	<?php $this->load->view('Templates/footer');  ?>
</div>
<!-- /content area -->
<script>
	remoteNameURL = site_url + "admin/users/checkUniqueUserName";
	remoteEmailURL = site_url + "admin/users/checkUniqueEmail";
	<?php if (isset($userdata)) { ?>
        var user_id = '<?php echo $userdata[0]['id'] ?>';
        remoteNameURL = site_url + "admin/users/checkUniqueUserName/" + user_id;
        remoteEmailURL = site_url + "admin/users/checkUniqueEmail/" + user_id;
	<?php } ?>
	var validator = $("#add_users_form").validate({
        ignore: 'input[type=hidden], .select2-search__field, #txt_status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label")
        },
        rules: {
        	txt_username:{
        		required: true,
        		minlength: 5,
        		remote: remoteNameURL
        	},
            txt_password: {
                minlength: 5
            },
            txt_email: {
            	required: true,
                email: true,
                remote: remoteEmailURL
            }
        },
        messages: {
            txt_username: {
                remote: $.validator.format("User name already exist!")
            },
            txt_email: {
                remote: $.validator.format("Email already exist!")
            }
        }
    });
</script>
