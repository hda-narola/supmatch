<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span> - List</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/users">Users</a></li>
            <li class="active">Display</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } else if ($this->session->flashdata('error') != '') { ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="admin/add_users" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Users</a>
        </div>

        <table class="table table-bordered table-hover datatable-highlight" id="users_tbl">
            <thead>
                <tr>
                    <th># Sr No.</th>
                    <th>Profile Pic</th>
                    <th>User Rank</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th>Login Type</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th class="text-center" style="width: 10% !important">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sr_no = 1;
                foreach ($usersArr->result() as $user) {
                    ?>
                    <tr>
                        <td><?php echo $sr_no; ?></td>
                        <td>
                            <?php
                            $img = $user->image != '' ? $user->image : 'default.jpg';
                            ?>
                            <img src='<?php echo USERS_IMG . $img; ?>' class='img-circle img-sm'>
                        </td>
                        <td><?php echo $user->rank; ?></td>
                        <td><?php echo $user->nickname; ?></td>
                        <td><a href='mailto:<?php echo $user->email; ?>' target='_blank'><?php echo $user->email; ?></a></td>
                        <td><?php echo $user->role_name; ?></td>
                        <td>
                            <?php
                            if ($user->login_user_type == 'facebook') {
                                $login_type = 'Facebook';
                            } else {
                                $login_type = 'Email';
                            }
                            echo $login_type;
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($user->status == 'active') {
                                echo "<span class='label label-success'>Active</span>";
                            } else if ($user->status == 'inactive') {
                                echo "<span class='label label-default'>Inactive</span>";
                            } else if ($user->status == 'deleted') {
                                echo "<span class='label label-danger'>Deleted</span>";
                            }
                            ?>
                        </td>
                        <td><?php echo $user->created; ?></td>
                        <td>
                                <!-- <a href="" class="btn border-teal text-teal btn-flat btn-icon btn-rounded btn-xs" title="View Users"><i class="icon-eye4"></i></a> -->
                            <a href="admin/edit_users/<?php echo $user->id; ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Users"><i class="icon-pencil7"></i></a>
                            <a href="admin/delete_users/<?php echo $user->id; ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Business"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                    $sr_no++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
</script>