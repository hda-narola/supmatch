<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Supplements</span> - List</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/supplements">Supplements</a></li>
            <li class="active">Display</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } else if ($this->session->flashdata('error') != '') { ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="admin/add_supplements" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Supplements</a>
        </div>

        <table class="table table-bordered table-hover datatable-highlight" id="supplements_tbl">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Amazon ASIN</th>
                    <!--<th>Weight</th>-->
                    <th>Price</th>
                    <th>Category</th>
                    <th>Flavours</th>
                    <th>Reviews</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th class="text-center" style="width: 9% !important">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sr_no = 1;
                foreach ($displayArr['supplements'] as $result) {
                    //p($result); 
                    ?>
                    <tr>
                        <td><?php echo $sr_no; ?></td>
                        <td>
                            <?php
                            $img = $result['images'] != '' ? explode(",", $result['images'])[0] : 'default.jpg';
                            ?>
                            <img src='<?php echo SUPPLEMENTS_IMG . $img; ?>' class='img-circle img-sm'>
                        </td>
                        <td>
                            <?php
                            echo "<b>En: </b>" . $result['name_en'] . "<br><b>Ger: </b>" . $result['name_gr'];
                            ?>
                        </td>
                        <td><?php echo (strlen($result['amazon_asin']) > 10) ? substr($result['amazon_asin'], 0, 10) . '...' : substr($result['amazon_asin'], 0, 10); ?></td>
                        <!--<td><?php echo number_format($result['weight'], 2); ?></td>-->
                        <td><?php echo number_format($result['price'], 2); ?></td>
                        <td><?php echo $result['cat_name_en']; ?></td>
                        <td>
                            <?php
                            $flavours = '';
                            foreach ($result['flavour'] as $flavourArr) {
                                $flavours.=$flavourArr . ',';
                            }
                            echo rtrim($flavours, ',');
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($result['reviews'] > 0) {
                                echo '<a href="' . site_url() . 'admin/supplements/reviews/' . $result['id'] . '">' . $result['reviews'] . ' <i class="icon-star-half"></i></a>';
                            } else {
                                echo $result['reviews'] . ' <i class="icon-star-half"></i>';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($result['status'] == 'active') {
                                echo "<span class='label label-success'>Active</span>";
                            } else if ($result['status'] == 'inactive') {
                                echo "<span class='label label-default'>Inactive</span>";
                            } else if ($result['status'] == 'deleted') {
                                echo "<span class='label label-danger'>Deleted</span>";
                            }
                            ?>
                        </td>
                        <td><?php echo $result['created']; ?></td>
                        <td>
                            <a href="admin/edit_supplements/<?php echo $result['id']; ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Supplements"><i class="icon-pencil7"></i></a>
                            <a href="admin/delete_supplements/<?php echo $result['id']; ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Supplements"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                    $sr_no++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
</script>