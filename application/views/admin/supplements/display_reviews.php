<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $supplement['name_en'] . '/' . $supplement['name_gr'] ?></span> - Reviews</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/supplements">Supplements</a></li>
            <li class="active">Reviews</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } else if ($this->session->flashdata('error') != '') { ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <!--        <div class="panel-heading text-right">
                    <a href="admin/add_supplements" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Supplements</a>
                </div>-->
        <table class="table table-bordered table-hover datatable-highlight" id="reviews_tbl">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>User</th>
                    <th>Review Comment</th>
                    <th>Properties</th>
                    <th>Liquid</th>
                    <th>Flavour</th>
                    <th>Likes</th>
                    <th>Dislikes</th>
                    <!--<th>Status</th>-->
                    <th>Created</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sr_no = 1;
                foreach ($reviews as $review) {
                    ?>
                    <tr>
                        <td><?php echo $sr_no; ?></td>
                        <td>
                            <?php
                            if ($review['nickname'] != '')
                                echo $review['nickname'];
                            else
                                echo "Guest";
                            ?>
                        </td>
                        <td>
                            <?php echo $review['comment'] ?>
                        </td>
                        <td>
                            <?php
                            $properties_gr = explode(",", $review['properties_gr']);
                            $properties_en = explode(",", $review['properties_en']);
                            $property_ratings = explode(",", $review['property_ratings']);
                            ?>
                            <?php
                            if ($properties_gr) {
                                foreach ($properties_gr as $key => $property) {
                                    if ($property != '') {
//                                        echo $properties_en[$key] . '/' . $property . ":"
                                        echo $property . ":"
                                        ?>
                                        <b>
                                            <?php echo $property_ratings[$key]; ?>
                                            <i class="icon-star-half"></i>
                                        </b>
                                        <br>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </td>
                        <td><?php if ($review['liquid_en'] != '') echo $review['liquid_en'] . '/' . $review['liquid_gr']; ?></td>
                        <td><?php if ($review['flavour_en'] != '') echo $review['flavour_en'] . '/' . $review['flavour_gr']; ?></td>
                        <td><?php echo $review['likes']; ?></td>
                        <td><?php echo $review['dislikes']; ?></td>
    <!--                        <td>
                        <?php
                        if ($review['status'] == 'active') {
                            echo "<span class='label label-success'>Active</span>";
                        } else if ($review['status'] == 'inactive') {
                            echo "<span class='label label-default'>Inactive</span>";
                        } else if ($review['status'] == 'deleted') {
                            echo "<span class='label label-danger'>Deleted</span>";
                        }
                        ?>
                        </td>-->
                        <td><?php echo $review['created']; ?></td>
                        <td>
                            <?php if ($review['status'] != 'deleted') { ?>
                                <a href="<?php echo site_url('admin/supplements/delete_review/' . $review['id']) ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Review"><i class="icon-trash"></i></a>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $sr_no++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You really want to delete this review!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
</script>