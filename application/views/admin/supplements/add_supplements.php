<?php
$supp_flavour_arr = '';
if (isset($supplements_data)) {
    $supp_flavour_arr = explode(",", $supp_flavourArr);
    $form_action = 'admin/edit_supplements/' . $supplements_data[0]['id'];
} else {
    $form_action = 'admin/add_supplements';
}
?>
<style type="text/css">
    .form-horizontal .checkbox .checker{ top:17px; }
    #img_price_loader {position: absolute;right: 50px;top: 5px; display: none;height: 25px;}
</style>
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Supplements</span> - Add New</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/supplements">Supplements</a></li>
            <li class="active">Add New</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <!-- Form validation -->
    <form class="form-horizontal" id="add_supplements_form" action="<?php echo $form_action; ?>" enctype="multipart/form-data" method="post">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">English Language</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <fieldset class="content-group">

                    <legend class="text-bold">Basic Details</legend>
                    <!-- Name : English -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" name="name_en" id="name_en" class="form-control" required="required" placeholder="Please enter name" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['name_en'] : set_value('name_en'); ?>">
                            <?php echo '<label id="name_en_error" class="validation-error-label" for="name_en">' . form_error('name_en') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Name : English -->

                    <!-- Description : English -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea name="txt_supplemets_desc_en" id="txt_supplemets_desc_en" class="form-control" required="required" placeholder="Please enter description" aria-required="true" rows="5" cols="5" ><?php echo (isset($supplements_data)) ? $supplements_data[0]['desc_en'] : set_value('txt_supplemets_desc_en'); ?></textarea>
                            <?php echo '<label id="txt_desc_en_error" class="validation-error-label" for="txt_desc_en">' . form_error('txt_supplemets_desc_en') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Description : English -->

                    <legend class="text-bold">Images</legend>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Main Image <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div class="media no-margin-top">
                                <?php
                                $main_image = '';
                                $image_req = 'required="required"';
                                if (isset($supplements_data)) {
                                    if ($supplements_data[0]['images'] != '') {
                                        $imgArr = explode(",", $supplements_data[0]['images']);
                                        $main_image = $imgArr[0];
                                        $image_req = '';
                                        ?>
                                        <div class="media-left">
                                            <a href="javascript:void(0);"><img src="<?php echo SUPPLEMENTS_IMG . $main_image; ?>" style="width: 58px; height: 58px;" class="img-rounded" alt=""></a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="media-body">
                                    <input type="file" class="file-styled-primary" name="txt_main_image" id="txt_main_image" <?php echo $image_req; ?>>
                                    <input type="hidden" name="hidden_main_image" id="hidden_main_image" value="<?php echo $main_image; ?>">
                                    <span class="help-block">Accepted formats: gif, png, jpg.</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Other Images </label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" name="txt_other_images[]" id="txt_other_images" multiple="multiple">
                            <span class="help-block"><code>You can upload here multiple images at a time</code>.</span>
                            <?php
                            if (isset($supplements_data)) {
                                if ($supplements_data[0]['images'] != '') {
                                    $imgArr = explode(",", $supplements_data[0]['images']);
                                    if (count($imgArr) > 1) {
                                        $cnt = 0;
                                        ?>
                                        <div class="row">
                                            <?php
                                            foreach ($imgArr as $other_img) {
                                                if ($cnt == 0) {
                                                    $cnt++;
                                                } else {
                                                    ?>
                                                    <div class="col-lg-2 col-sm-6 div_other_img">
                                                        <div class="thumbnail">
                                                            <div class="thumb">
                                                                <img src="<?php echo SUPPLEMENTS_IMG . $other_img; ?>" alt="" style="height:120px;width:150px">
                                                                <input type="hidden" class="hidden_other_img" name="hidden_other_image[]" id="hidden_other_image<?php echo $cnt; ?>" value="<?php echo $other_img; ?>">
                                                                <div class="caption-overflow">
                                                                    <span>
                                                                            <!-- <a href="assets/images/placeholder.jpg" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a> -->
                                                                        <a href="javascript:void(0);" class="btn btn-danger btn-xs remove_other_img" style="top-margin:5px !important">REMOVE</a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>


                    <legend class="text-bold">Other Details</legend>
                    <!-- Amazon Link -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Amazon Link <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" name="txt_amazon_link" id="txt_amazon_link" class="form-control" required="required" placeholder="Please enter amazon supplement's link" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['amazon_link'] : set_value('txt_amazon_link'); ?>">
                            <?php echo '<label id="txt_amazon_link_error" class="validation-error-label" for="txt_amazon_link">' . form_error('txt_amazon_link') . '</label>'; ?>
                        </div>

                        <label class="control-label col-lg-2">Amazon ASIN <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" name="txt_amazon_asin" id="txt_amazon_asin" class="form-control" required="required" placeholder="Please enter amazon ASIN" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['amazon_ASIN'] : set_value('txt_amazon_asin'); ?>">
                            <?php echo '<label id="txt_amazon_asin_error" class="validation-error-label" for="txt_amazon_asin">' . form_error('txt_amazon_asin') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Amazon Link -->

                    <!-- Weight / Price -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Weight <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="number" name="txt_weight" id="txt_weight" class="form-control" required="required" placeholder="Please enter weight in grams" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['weight'] : set_value('txt_weight'); ?>" min="0">
                            <?php echo '<label id="txt_weight_error" class="validation-error-label" for="txt_weight">' . form_error('txt_weight') . '</label>'; ?>
                        </div>

                        <label class="control-label col-lg-2">Price <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="number" name="txt_price" id="txt_price" class="form-control" required="required" placeholder="Please enter price" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['price'] : set_value('txt_price'); ?>" min="0">
                            <img src="assets/images/loader.gif" id="img_price_loader">
                            <?php echo '<label id="txt_price_error" class="validation-error-label" for="txt_price">' . form_error('txt_price') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Weight / Price -->

                    <div class="form-group">
                        <label class="control-label col-lg-2">Category <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="select-search" name="txt_category" id="txt_category">
                                <option value="">None Selected</option>
                                <?php foreach ($categoryArr->result() as $category) {
                                    ?>
                                    <option value="<?php echo $category->id; ?>" 
                                    <?php
                                    if (isset($supplements_data)) {
                                        if ($category->id == $supplements_data[0]['category_ids']) {
                                            echo 'selected';
                                            $category_type = $category->type;
                                        }
                                    }
                                    ?>
                                            ><?php echo $category->name_en . '/' . $category->name_gr; ?></option>
                                        <?php } ?>
                            </select>
                            <?php echo '<label id="txt_category_error" class="validation-error-label" for="txt_category">' . form_error('txt_category') . '</label>'; ?>
                        </div>

                        <label class="control-label col-lg-2">Flavours</label>
                        <div class="multi-select-full col-lg-4">
                            <select class="multiselect-filtering" multiple="multiple" name="txt_flavours[]" id="txt_flavours">
                                <?php foreach ($flavourArr->result() as $flavour) { ?>
                                    <option value="<?php echo $flavour->id; ?>" 
                                    <?php
                                    if (!empty($supp_flavour_arr)) {
                                        if (in_array($flavour->id, $supp_flavour_arr)) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>><?php echo $flavour->name_en . '/' . $flavour->name_gr; ?>
                                    </option>
                                <?php } ?>
                            </select>
                            <?php echo '<label id="txt_flavours_error" class="validation-error-label" for="txt_flavours">' . form_error('txt_flavours[]') . '</label>'; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Type <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="select" name="supplement_type" id="supplement_type" required="required">
                                <?php
                                if (isset($supplements_data)) {
                                    if ($category_type == 1) {
                                        echo "<option value='" . $category_type . "' selected>Powder</option>";
                                    } else if ($category_type == 2) {
                                        echo "<option value='" . $category_type . "' selected>Pill</option>";
                                    } else if ($category_type == 3) {
                                        if ($supplements_data[0]['type'] == 1) {
                                            echo "<option value='1' selected>Powder</option>";
                                        } else {
                                            echo "<option value='1'>Powder</option>";
                                        }
                                        if ($supplements_data[0]['type'] == 2) {
                                            echo "<option value='2' selected>Pill</option>";
                                        } else {
                                            echo "<option value='2'>Pill</option>";
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <?php echo '<label id="supplement_type_error" class="validation-error-label" for="supplement_typer">' . form_error('supplement_type') . '</label>'; ?>
                        </div>
                    </div>

                    <!-- Secondary properties -->
                    <?php
                    $div_style = 'style="display:none"';
                    if (isset($supplements_data) && $secondary_properties) {
                        $div_style = '';
                    }
                    ?>
                    <div class="form-group" id="secodary_properties_parent_div" <?php echo $div_style ?>>
                        <label class="control-label col-lg-2">Secondary Properties <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div id="secodary_properties_div">
                                <?php
                                if (isset($supplements_data)) {
                                    $selected_sec_pros = explode(",", $supplements_data[0]['secondaryproperty_ids']);
                                    foreach ($secondary_properties as $secondary_property) {
                                        $selected = '';
                                        if (in_array($secondary_property['id'], $selected_sec_pros)) {
                                            $selected = 'checked="checked"';
                                        }
                                        ?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" class="styled" name="txt_boolean_checkbox[]" value="<?php echo $secondary_property['id'] ?>" <?php echo $selected ?>> <?php echo $secondary_property['name_en'] . '/' . $secondary_property['name_gr'] ?>
                                        </label>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <!--                            <label class="checkbox-inline">
                                                            <input type="checkbox" class="styled" name="txt_boolean_checkbox[]" id="txt_boolean_checkbox[]" value="availibility" checked="checked" disabled> Availibility
                                                        </label>-->
                        </div>
                    </div>

                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Status </label>
                        <div class="col-lg-10">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="txt_status" id="txt_status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php
                                    if (isset($supplements_data)) {
                                        if ($supplements_data[0]['status'] == 'active') {
                                            echo 'checked';
                                        }
                                    } else {
                                        echo 'checked';
                                    }
                                    ?>>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->
                </fieldset>
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">German Language</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <fieldset class="content-group">
                    <legend class="text-bold">Basic Details</legend>
                    <!-- Name : German -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" name="name_gr" id="name_gr" class="form-control" required="required" placeholder="Please enter name" value="<?php echo (isset($supplements_data)) ? $supplements_data[0]['name_gr'] : set_value('name_gr'); ?>">
                            <?php echo '<label id="name_gr_error" class="validation-error-label" for="name_gr">' . form_error('name_gr') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Name : German -->

                    <!-- Description : German -->
                    <div class="form-group">
                        <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea name="txt_supplemets_desc_gr" id="txt_supplemets_desc_gr" class="form-control" required="required" placeholder="Please enter description" aria-required="true" rows="5" cols="5" ><?php echo (isset($supplements_data)) ? $supplements_data[0]['desc_gr'] : set_value('txt_supplemets_desc_gr'); ?></textarea>
                            <?php echo '<label id="txt_supplemets_desc_gr_error" class="validation-error-label" for="txt_supplemets_desc_gr">' . form_error('txt_supplemets_desc_gr') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /Description : German -->

                </fieldset>



            </div>
        </div>

        <div class="text-left">
            <input type="hidden" name="hidden_user_id" value="<?php
            if (isset($userdata)) {
                echo $userdata[0]['id'];
            }
            ?>">
            <button type="submit" class="btn btn-success">Submit <i class="icon-arrow-right14 position-right"></i></button>
        </div>
    </form>
    <!-- /form validation -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<!-- /content area -->
<script>
    remoteEnURL = site_url + "admin/categories/checkUniqueNameEn/<?php echo $table ?>";
    remoteGrURL = site_url + "admin/categories/checkUniqueNameGr/<?php echo $table ?>";
<?php if (isset($supplements_data)) { ?>
        var supplement_id = '<?php echo $supplements_data[0]['id'] ?>';
        remoteEnURL += "/" + supplement_id;
        remoteGrURL += "/" + supplement_id;
<?php } ?>
    var validator = $("#add_supplements_form").validate({
        ignore: 'input[type=hidden], .select2-search__field, #txt_status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            name_en: {
                required: true,
                remote: remoteEnURL
            },
            txt_supplemets_desc_en: {
                required: true,
                maxlength: 1000
            },
            name_gr: {
                required: true,
                remote: remoteGrURL
            },
            txt_supplemets_desc_gr: {
                required: true,
                maxlength: 1000
            },
            txt_amazon_link: {
                required: true,
                url: true
            },
            txt_category: {
                required: true
            },
//            'txt_flavours[]': {
//                required: true
//            }
        },
        messages: {
            name_en: {
                remote: $.validator.format("Supplement name already exist!")
            },
            name_gr: {
                remote: $.validator.format("Supplement name already exist!")
            },
        }
    });

    //-- Category change event
    $('#txt_category').change(function () {
        var category_id = $(this).val();
        $.ajax({
            url: site_url + 'admin/supplements/get_cat_details',
            type: 'POST',
            dataType: 'json',
            data: {category_id: category_id},
            success: function (data) {
                var options = '';
                val = data.type;
                if (data.type == 1) {
                    options = '<option value="1">Powder</option>';
                } else if (data.type == 2) {
                    options = '<option value="2">Pill</option>';
                } else if (data.type == 3) {
                    val = 1;
                    options = '<option value="1">Powder</option>';
                    options += '<option value="2">Pill</option>';
                }
                $('#supplement_type').empty().append(options);
                $('#supplement_type').select2();
                $('supplement_type').val(val);
                str = '';
                if (data.second_properties.length == 0) {
                    $('#secodary_properties_div').html('');
                    $('#secodary_properties_parent_div').hide();
                } else {
                    $('#secodary_properties_parent_div').show();
                    str = '';
                    $.each(data.second_properties, function (i, item) {
                        str += '<label class="checkbox-inline"><input type="checkbox" class="styled" name="txt_boolean_checkbox[]" value="' + item.id + '">';
                        str += item.name_en + '/' + item.name_gr;
                        str += '</label>';
                    });
                    $('#secodary_properties_div').html(str);
                    $(".styled").uniform({
                        radioClass: 'choice'
                    });
                }
            }});
    });
    //-- Supplement type change event
    $('#supplement_type').change(function () {
        var category_id = $('#txt_category').val();
        var type = $(this).val();
        $.ajax({
            url: site_url + 'admin/supplements/get_secondary_property_details',
            type: 'POST',
            dataType: 'json',
            data: {category_id: category_id, type: type},
            success: function (data) {
                str = '';
                if (data.second_properties.length == 0) {
                    $('#secodary_properties_div').html('');
                    $('#secodary_properties_parent_div').hide();
                } else {
                    $('#secodary_properties_parent_div').show();
                    str = '';
                    $.each(data.second_properties, function (i, item) {
                        str += '<label class="checkbox-inline"><input type="checkbox" class="styled" name="txt_boolean_checkbox[]" value="' + item.id + '">';
                        str += item.name_en + '/' + item.name_gr;
                        str += '</label>';
                    });
                    $('#secodary_properties_div').html(str);
                    $(".styled").uniform({
                        radioClass: 'choice'
                    });
                }
            }
        });
    });

    $('.remove_other_img').click(function () {
        $(this).parents('.div_other_img').remove();
        //$(this).siblings('.hidden_other_img').val('');
    });
    
    $('#txt_amazon_asin').on('blur', function (){
        
        $('#img_price_loader').show();
        var asin_id = $('#txt_amazon_asin').val();
        $.ajax({
            url: "admin/supplements/get_suppliment_price",
            type: 'POST',
            data: {txt_amazon_asin: asin_id},
            success: function (response) {
                if(response != ''){
                    $('#txt_price').val(response);
                    $('#txt_price').prop('readonly', true);
                }else{
                    $('#txt_price').val(response);
                    $('#txt_price').prop('readonly', false);
                }
                $('#img_price_loader').hide();
            }
        });
    });
</script>

