<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-star-full2"></i> <span class="text-semibold">Ranks</span> - List</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/ranks">Ranks</a></li>
            <li class="active">Display</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } else if ($this->session->flashdata('error') != '') { ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="admin/add_ranks" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Ranks</a>
        </div>

        <table class="table table-bordered table-hover datatable-highlight" id="ranks_tbl">
            <thead>
                <tr>
                    <th>#Rank.</th>
                    <th>Icon</th>
                    <th>Name</th>
                    <th>Required Text</th>
                    <th>Reward Text</th>
                    <th>Color</th>
                    <th>Required</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th class="text-center" style="width: 10%;">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($resultArr->result() as $result) {
                    ?>
                    <tr>
                        <td><?php echo $result->rank; ?></td>
                        <td>
                            <?php
                            $img = $result->icon != '' ? $result->icon : 'default.jpg';
                            ?>
                            <img src='<?php echo RANKS_IMG . $img; ?>' class='img-circle img-sm'>
                        </td>
                        <td><?php echo $result->name; ?></td>
                        <td><?php echo $result->requirement; ?></td>
                        <td><?php echo $result->reward; ?></td>
                        <td><?php echo $result->color; ?></td>
                        <td>
                            <?php
                            echo "<b>Reviews : </b>" . $result->required_reviews . "<br>" .
                            "<b>Likes : </b>" . $result->required_likes;
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($result->status == 'active') {
                                echo "<span class='label label-success'>Active</span>";
                            } else if ($result->status == 'inactive') {
                                echo "<span class='label label-default'>Inactive</span>";
                            } else if ($result->status == 'deleted') {
                                echo "<span class='label label-danger'>Deleted</span>";
                            }
                            ?>
                        </td>
                        <td><?php echo $result->created; ?></td>
                        <td>
                            <a href="admin/edit_ranks/<?php echo $result->id; ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Ranks"><i class="icon-pencil7"></i></a>
                            <a href="admin/delete_ranks/<?php echo $result->id; ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Ranks"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
</script>