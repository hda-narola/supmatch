<?php
if (isset($rankdata)) {
    $form_action = 'admin/edit_ranks/' . $rankdata[0]['id'];
} else {
    $form_action = 'admin/add_ranks';
}
?>
<script type="text/javascript" src="assets/admin/js/pages/form_validation.js"></script> 
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Ranks</span> - Add New</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="admin"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="admin/ranks"><i class="icon-star-full2"></i> Ranks</a></li>
            <li class="active">Add New</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal" id="add_ranks_form" action="<?php echo $form_action; ?>" enctype="multipart/form-data" method="post">
                <fieldset class="content-group">
                    <legend class="text-bold">Basic Details</legend>

                    <!-- Basic text input -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="txt_name" id="txt_name" class="form-control" required="required" placeholder="Please enter name" value="<?php echo (isset($rankdata)) ? $rankdata[0]['name'] : set_value('txt_name'); ?>">
                            <?php echo '<label id="txt_name_error" class="validation-error-label" for="txt_name">' . form_error('txt_name') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <div class="form-group">
                        <label class="control-label col-lg-3">Rank <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="number" name="txt_rank" id="txt_rank" class="form-control" required="required" placeholder="Please enter rank in number" value="<?php echo (isset($rankdata)) ? $rankdata[0]['rank'] : set_value('txt_rank'); ?>" min="0">
                            <?php echo '<label id="txt_rank_error" class="validation-error-label" for="txt_rank">' . form_error('txt_rank') . '</label>'; ?>
                        </div>
                    </div>

                    <!-- Email field -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Requirement Text <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="txt_requirement_text" id="txt_requirement_text" class="form-control" required="required" placeholder="Please enter requirement text" value="<?php echo (isset($rankdata)) ? $rankdata[0]['requirement'] : set_value('txt_requirement_text'); ?>">
                            <?php echo '<label id="txt_requirement_text_error" class="validation-error-label" for="txt_requirement_text">' . form_error('txt_requirement_text') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /email field -->


                    <!-- Input with icons -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Reward <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="txt_reward" id="txt_reward" class="form-control" required="required" placeholder="Please enter reward" value="<?php echo (isset($rankdata)) ? $rankdata[0]['reward'] : set_value('txt_reward'); ?>">
                            <?php echo '<label id="txt_reward_error" class="validation-error-label" for="txt_reward">' . form_error('txt_reward') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- /input with icons -->


                    <!-- Input group -->
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Icon <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="media no-margin-top">
                                <?php
                                $image_req = 'required="required"';
                                if (isset($rankdata)) {
                                    if ($rankdata[0]['icon'] != '') {
                                        $image_req = '';
                                        ?>
                                        <div class="media-left">
                                            <a href="javascript:void(0);"><img src="<?php echo RANKS_IMG . $rankdata[0]['icon']; ?>" style="width: 58px; height: 58px;" class="img-rounded" alt=""></a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                                <div class="media-body">
                                    <input type="file" class="file-styled" name="txt_icon" id="txt_icon" <?php echo $image_req; ?>>
                                    <span class="help-block">Accepted formats: gif, png, jpg.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /input group -->

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Color <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control colorpicker-show-input" data-preferred-format="hex" name="txt_color" id="txt_color" value="<?php echo (isset($rankdata)) ? $rankdata[0]['color'] : '#E63E3E'; ?>" >
                            <?php echo '<label id="txt_color_error" class="validation-error-label" for="txt_color">' . form_error('txt_color') . '</label>'; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Required Reviews <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="number" name="txt_req_reviews" id="txt_req_reviews" class="form-control" required="required" placeholder="Please enter total number of required reviews" value="<?php echo (isset($rankdata)) ? $rankdata[0]['required_reviews'] : set_value('txt_req_reviews'); ?>"  min="0">
                            <?php echo '<label id="txt_req_reviews_error" class="validation-error-label" for="txt_req_reviews">' . form_error('txt_req_reviews') . '</label>'; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Required Likes <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="number" name="txt_req_likes" id="txt_req_likes" class="form-control" required="required" placeholder="Please enter total number of required likes" value="<?php echo (isset($rankdata)) ? $rankdata[0]['required_likes'] : set_value('txt_req_likes'); ?>"  min="0">
                            <?php echo '<label id="txt_req_likes_error" class="validation-error-label" for="txt_req_likes">' . form_error('txt_req_likes') . '</label>'; ?>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                            <label class="col-lg-3 control-label">Required Dislikes <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                    <input type="number" name="txt_req_dislikes" id="txt_req_dislikes" class="form-control" required="required" placeholder="Please enter total number of required dislikes" value="<?php echo (isset($rankdata)) ? $rankdata[0]['required_dislikes'] : set_value('txt_req_dislikes'); ?>">
                    <?php echo '<label id="txt_req_dislikes_error" class="validation-error-label" for="txt_req_dislikes">' . form_error('txt_req_dislikes') . '</label>'; ?>
                            </div>
                    </div> -->

                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">Status <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="txt_status" id="txt_status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php
                                    if (isset($rankdata)) {
                                        if ($rankdata[0]['status'] == 'active') {
                                            echo 'checked';
                                        }
                                    } else {
                                        echo 'checked';
                                    }
                                    ?>>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->
                </fieldset>

                <div class="text-left">
                    <input type="hidden" name="hidden_user_id" value="<?php
                    if (isset($userdata)) {
                        echo $userdata[0]['id'];
                    }
                    ?>">
                    <button type="submit" class="btn btn-success">Submit <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->
    <?php $this->load->view('Templates/footer'); ?>
</div>
<!-- /content area -->
<script>
    remoteEnURL = site_url + "admin/ranks/checkUniqueRankName";
    remoterankURL = site_url + 'admin/ranks/checkUniqueRank';
<?php if (isset($rankdata)) { ?>
        var rank_id = '<?php echo $rankdata[0]['id'] ?>';
        remoteEnURL = site_url + "admin/ranks/checkUniqueRankName/" + rank_id;
        remoterankURL += '/' + rank_id;
<?php } ?>
    var validator = $("#add_ranks_form").validate({
        ignore: 'input[type=hidden], .select2-search__field, #txt_status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            txt_name: {
                required: true,
                maxlength: 150,
                remote: remoteEnURL
            },
            txt_rank: {
                required: true,
                remote: remoterankURL
            },
            txt_requirement_text: {
                maxlength: 150,
            },
            txt_reward: {
                maxlength: 150,
            }
        },
        messages: {
            txt_name: {
                remote: $.validator.format("Rank name already exist!")
            },
            txt_rank: {
                remote: $.validator.format("Rank already exist!")
            }
        }
    });
</script>
