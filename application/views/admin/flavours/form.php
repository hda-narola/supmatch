<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <?php
                if (isset($flavour_data))
                    echo '<i class="icon-pencil3"></i>';
                else
                    echo '<i class="icon-plus-circle2"></i>';
                ?> 
                <span class="text-semibold"><?php echo $heading; ?></span>
            </h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/flavours'); ?>"><img src="assets/admin/images/fruitsblack.png"> Flavours</a></li>
            <li class="active"><?php echo $heading; ?></li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <div class="alert alert-success hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('success') ?></strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) {
                ?>
                <div class="alert alert-danger hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('error') ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal" action="" id="flavour_info" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-lg-3 control-label">Flavour Name (English) <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="name_en" id="name_en" placeholder="Enter Flavour Name(English)" class="form-control" value="<?php echo (isset($flavour_data)) ? $flavour_data['name_en'] : set_value('name_en'); ?>" required="required">
                        <?php
                        echo '<label id="name_en-error" class="validation-error-label" for="name_en">' . form_error('name_en') . '</label>';
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Flavour Name (German) <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="name_gr" id="name_gr" placeholder="Enter Flavour Name(German)" class="form-control" value="<?php echo (isset($flavour_data)) ? $flavour_data['name_gr'] : set_value('name_gr'); ?>" required="required">
                        <?php
                        echo '<label id="name_gr-error" class="validation-error-label" for="name_gr">' . form_error('name_gr') . '</label>';
                        ?>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit" id="btn_submit">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>

    remoteEnURL = site_url + "admin/flavours/checkUniqueNameEn/<?php echo $table ?>";
    remoteGrURL = site_url + "admin/flavours/checkUniqueNameGr/<?php echo $table ?>";
<?php if (isset($flavour_data)) { ?>
        var liquid_id = '<?php echo $flavour_data['id'] ?>';
        remoteEnURL += "/" + liquid_id;
        remoteGrURL += "/" + liquid_id;
<?php } ?>
    var validator = $("#flavour_info").validate({
        ignore: 'input[type=hidden], .select2-search__field, #txt_status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            name_en: {
                required: true,
                remote: remoteEnURL
            },
            name_gr: {
                required: true,
                remote: remoteGrURL
            },
        },
        messages: {
            name_en: {
                remote: $.validator.format("Flavour name already exist!")
            },
            name_gr: {
                remote: $.validator.format("Flavour name already exist!")
            },
        }
    });
</script>
