<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><img src="assets/admin/images/fruitsblack.png"> <span class="text-semibold">All Flavours</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Flavours</li>
        </ul>
    </div>
</div>
<div class="content">

    <?php
    if ($this->session->flashdata('success')) {
        ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) {
        ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>

    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="<?php echo site_url('admin/flavours/add'); ?>" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> Add Flavour</a>
        </div>
        <table class="table table-bordered table-hover datatable-highlight" id="flavours_tbl">
            <thead>
                <tr>
                    <th style="width:10% !important" class="hidden"># Sr No.</th>
                    <th style="width:10% !important"># Sr No.</th>
                    <th style="width:25% !important">Name (English)</th>
                    <th style="width:25% !important">Name (German)</th>
                    <th style="width:20% !important">Created</th>
                    <!--<th style="width:20% !important">Status</th>-->
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($flavours as $flavour) {
                    ?>
                    <tr>
                        <td class="hidden"><?php echo $i; ?></td>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $flavour['name_en']; ?></td>
                        <td><?php echo $flavour['name_gr']; ?></td>
                        <td><?php echo $flavour['created']; ?></td>
                        <!--<td><?php echo $flavour['created']; ?></td>-->
                        <td>
                            <a href="<?php echo site_url('admin/flavours/edit/' . $flavour['id']) ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Flavour"><i class="icon-pencil7"></i></a>
                            <a href="<?php echo site_url('admin/flavours/delete/' . $flavour['id']) ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Flavour"><i class="icon-trash"></i></a>
                        </td>
                    </tr>

                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this flavour!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
    /*
     $(function () {
     var table = $('.datatable-basic').dataTable({
     processing: true,
     serverSide: true,
     language: {
     search: '<span>Filter:</span> _INPUT_',
     lengthMenu: '<span>Show:</span> _MENU_',
     paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'},
     //                searchPlaceholder: "Type name,address,user"
     },
     dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
     order: [[4, "desc"]],
     ajax: site_url + 'admin/flavours/get_flavours',
     columns: [
     {
     data: "sr_no",
     visible: true,
     sortable: false,
     },
     {
     data: "name_en",
     visible: true,
     },
     {
     data: "name_gr",
     visible: true,
     },
     {
     data: "category",
     visible: true,
     render: function (data, type, full, meta) {
     if (data == '' || data == null) {
     return '--';
     } else {
     return data;
     }
     }
     },
     {
     data: "created",
     visible: true,
     },
     {
     data: "status",
     visible: true,
     searchable: false,
     sortable: false,
     render: function (data, type, full, meta) {
     action = '';
     action += '<a href="' + site_url + 'admin/flavours/edit/' + full.id + '" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Business"><i class="icon-pencil7"></i></a>';
     action += '&nbsp;&nbsp;<a href="' + site_url + 'admin/flavours/delete/' + full.id + '" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Business"><i class="icon-trash"></i></a>';
     return action;
     }
     },
     ]
     });
     
     $('.dataTables_length select').select2({
     minimumResultsForSearch: Infinity,
     width: 'auto'
     });
     
     }); */
</script>