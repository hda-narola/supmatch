<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <?php
                if (isset($property_data))
                    echo '<i class="icon-pencil3"></i>';
                else
                    echo '<i class="icon-plus-circle2"></i>';
                ?> 
                <span class="text-semibold"><?php echo $heading; ?></span>
            </h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/' . $link); ?>"><i class="icon-list2"></i> <?php echo $link_title; ?> Properties</a></li>
            <li class="active"><?php echo $heading; ?></li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <div class="alert alert-success hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('success') ?></strong>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) {
                ?>
                <div class="alert alert-danger hide-msg">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <strong><?php echo $this->session->flashdata('error') ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal" id="property_info" action="" enctype="multipart/form-data" method="post">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">English Language</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse" class=""></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <!-- Name : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <input type="text" name="name_en" id="name_en" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($property_data)) ? $property_data['name_en'] : set_value('name_en'); ?>">
                                <?php echo '<label id="name_en_error" class="validation-error-label" for="name_en">' . form_error('name_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Name : English -->

                        <!-- Description : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <textarea name="desc_en" id="desc_en" class="form-control" required="required" placeholder="Enter Description" aria-required="true" rows="5" cols="5" ><?php echo (isset($property_data)) ? $property_data['desc_en'] : set_value('desc_en'); ?></textarea>
                                <?php echo '<label id="desc_en_error" class="validation-error-label" for="desc_en">' . form_error('desc_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Description : English -->
                        <?php if ($table != TBL_PRICE_PROPERTY) { ?>
                            <!-- Switch single -->
                            <div class="form-group">
                                <label class="control-label col-lg-2">Status </label>
                                <div class="col-lg-6">
                                    <div class="checkbox checkbox-switch">
                                        <label>
                                            <input type="checkbox" name="status" id="status" data-on-text="Active" data-off-text="Inactive" class="switch" required="required" <?php
                                            if (isset($property_data)) {
                                                if ($property_data['status'] == 'active') {
                                                    echo 'checked';
                                                } else {
                                                    echo '';
                                                }
                                            } else {
                                                echo 'checked';
                                            }
                                            ?>>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /switch single -->
                        <?php } ?>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">In German Language</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse" class=""></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <!-- Name : German -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <input type="text" name="name_gr" id="name_gr" class="form-control" required="required" placeholder="Enter Name" value="<?php echo (isset($property_data)) ? $property_data['name_gr'] : set_value('name_gr'); ?>">
                                <?php echo '<label id="name_gr_error" class="validation-error-label" for="name_gr">' . form_error('name_en') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Name : German -->

                        <!-- Description : English -->
                        <div class="form-group">
                            <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                            <div class="col-lg-6">
                                <textarea name="desc_gr" id="desc_gr" class="form-control" required="required" placeholder="Enter Description" aria-required="true" rows="5" cols="5" ><?php echo (isset($property_data)) ? $property_data['desc_gr'] : set_value('desc_gr'); ?></textarea>
                                <?php echo '<label id="desc_gr_error" class="validation-error-label" for="desc_gr">' . form_error('desc_gr') . '</label>'; ?>
                            </div>
                        </div>
                        <!-- /Description : English -->
                    </div>
                </div>
                <div class="text-left">
                    <button class="btn btn-success" type="submit" id="btn_submit">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    remoteEnURL = site_url + "admin/properties/checkUniqueNameEn/<?php echo $table ?>";
    remoteGrURL = site_url + "admin/properties/checkUniqueNameGr/<?php echo $table ?>";
<?php if (isset($property_data)) { ?>
        var property_id = '<?php echo $property_data['id'] ?>';
        remoteEnURL += "/" + property_id;
        remoteGrURL += "/" + property_id;
<?php } ?>
    var validator = $("#property_info").validate({
        ignore: 'input[type=hidden], .select2-search__field, #status', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label")
        },
        rules: {
            name_en: {
                required: true,
                remote: remoteEnURL
            },
            desc_en: {
                required: true,
                maxlength: 150
            },
            name_gr: {
                required: true,
                remote: remoteGrURL
            },
            desc_gr: {
                required: true,
                maxlength: 150
            }
        },
        messages: {
            name_en: {
                remote: $.validator.format("Property name already exist!")
            },
            name_gr: {
                remote: $.validator.format("Property name already exist!")
            },
        }
    });
</script>
