<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-list2"></i> <span class="text-semibold">All <?php echo $heading ?></span></h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Properties</li>
        </ul>
    </div>
</div>
<div class="content">

    <?php
    if ($this->session->flashdata('success')) {
        ?>
        <div class="alert bg-success alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Success!</span> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) {
        ?>
        <div class="alert bg-danger alert-styled-left bootstrap_alert">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Error!</span> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>

    <div class="panel panel-flat">
        <div class="panel-heading text-right">
            <a href="<?php echo site_url('admin/' . $link . '/add'); ?>" class="btn btn-success btn-labeled"><b><i class="icon-plus-circle2"></i></b> <?php echo $add_heading ?></a>
        </div>
        <table class="table table-bordered table-hover datatable-highlight" id="properties_tbl">
            <thead>
                <tr>
                    <th># Sr No.</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($link == 'properties') { ?>
                    <tr>
                        <td></td>
                        <td><?php echo "<b>English : </b>" . $price_property['name_en'] . "<br><b>German  : </b>" . $price_property['name_gr']; ?></td>
                        <td><?php echo "<b>English : </b>" . character_limiter($price_property['desc_en'], 20) . "<br><b>German  : </b>" . character_limiter($price_property['desc_gr'], 20); ?></td>
                        <td><span class='label label-success'>Active</span></td>
                        <td></td>
                        <td>   
                            <a href="<?php echo site_url('admin/properties/edit_priceproperty') ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Property"><i class="icon-pencil7"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                <?php
                $i = 1;
                foreach ($properties as $property) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php echo "<b>English : </b>" . $property['name_en'] . "<br><b>German  : </b>" . $property['name_gr']; ?>
                        </td>
                        <td><?php echo "<b>English : </b>" . character_limiter($property['desc_en'], 20) . "<br><b>German  : </b>" . character_limiter($property['desc_gr'], 20); ?></td>
                        <td>
                            <?php
                            if ($property['status'] == 'active') {
                                echo "<span class='label label-success'>Active</span>";
                            } else if ($property['status'] == 'inactive') {
                                echo "<span class='label label-default'>Inactive</span>";
                            } else if ($property['status'] == 'deleted') {
                                echo "<span class='label label-danger'>Deleted</span>";
                            }
                            ?>
                        </td>
                        <td><?php echo $property['created']; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/' . $link . '/edit/' . $property['id']) ?>" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Property"><i class="icon-pencil7"></i></a>
                            <a href="<?php echo site_url('admin/' . $link . '/delete/' . $property['id']) ?>" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Property"><i class="icon-trash"></i></a>
                        </td>
                    </tr>

                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php $this->load->view('Templates/footer'); ?>
</div>
<script>
    function confirm_alert(e) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this property!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            confirmButtonText: "Yes, delete it!"
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = $(e).attr('href');
                return true;
            }
            else {
                return false;
            }
        });
        return false;
    }
    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [4]
            }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Datatable 'length' options
    $('.datatable-show-all').DataTable({
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
    // DOM positioning
    $('.datatable-dom-position').DataTable({
        dom: '<"datatable-header length-left"lp><"datatable-scroll"t><"datatable-footer info-right"fi>',
    });

    // Highlighting rows and columns on mouseover
    var lastIdx = null;
    var table = $('.datatable-highlight').DataTable();

    $('.datatable-highlight tbody').on('mouseover', 'td', function () {
        var colIdx = table.cell(this).index().column;

        if (colIdx !== lastIdx) {
            $(table.cells().nodes()).removeClass('active');
            $(table.column(colIdx).nodes()).addClass('active');
        }
    }).on('mouseleave', function () {
        $(table.cells().nodes()).removeClass('active');
    });
    // Columns rendering
    $('.datatable-columns').dataTable({
        columnDefs: [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                render: function (data, type, row) {
                    return data + ' (' + row[3] + ')';
                },
                targets: 0
            },
            {visible: false, targets: [3]}
        ]
    });

    /*
     $(function () {
     var table = $('.datatable-basic').dataTable({
     processing: true,
     serverSide: true,
     language: {
     search: '<span>Filter:</span> _INPUT_',
     lengthMenu: '<span>Show:</span> _MENU_',
     paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'},
     //                searchPlaceholder: "Type name,address,user"
     },
     dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
     order: [[4, "desc"]],
     ajax: site_url + 'admin/properties/get_properties',
     columns: [
     {
     data: "sr_no",
     visible: true,
     sortable: false,
     },
     {
     data: "name_en",
     visible: true,
     },
     {
     data: "name_gr",
     visible: true,
     },
     {
     data: "property",
     visible: true,
     render: function (data, type, full, meta) {
     if (data == '' || data == null) {
     return '--';
     } else {
     return data;
     }
     }
     },
     {
     data: "created",
     visible: true,
     },
     {
     data: "status",
     visible: true,
     searchable: false,
     sortable: false,
     render: function (data, type, full, meta) {
     action = '';
     action += '<a href="' + site_url + 'admin/properties/edit/' + full.id + '" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-xs" title="Edit Business"><i class="icon-pencil7"></i></a>';
     action += '&nbsp;&nbsp;<a href="' + site_url + 'admin/properties/delete/' + full.id + '" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-xs" onclick="return confirm_alert(this)" title="Delete Business"><i class="icon-trash"></i></a>';
     return action;
     }
     },
     ]
     });
     
     $('.dataTables_length select').select2({
     minimumResultsForSearch: Infinity,
     width: 'auto'
     });
     
     }); */
</script>