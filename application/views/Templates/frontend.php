<?php error_reporting(0); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
                        <title><?php echo $title; ?></title>
                        <base href="<?php echo base_url(); ?>">
                            <link rel="icon" type="image/x-icon" href="assets/images/favicon.png" >
                                <!-- Custom Main StyleSheet CSS -->
                                <link href="assets/css/style.css" rel="stylesheet">
                                    <!-- Pnotify css-->
                                    <!--<link href="assets/css/pnotify.css">-->
                                    <!--<link href="assets/pnotify/pnotify.custom.min.css">-->
                                    <!-- Responsive CSS -->
                                    <link href="assets/css/responsive.css" rel="stylesheet">
                                        <!-- Typeahead Css-->
                                        <link href="assets/css/typehead.css" rel="stylesheet">
                                            <link href="assets/css/fakeLoader.css" rel="stylesheet" type="text/css"/>
                                            <style>
                                                .alert-box {
                                                    border-style: solid;
                                                    border-width: 1px;
                                                    display: block;
                                                    width: 100%;
                                                    font-weight: normal;
                                                    margin-bottom: 1.25rem;
                                                    position: absolute;
                                                    top: 0;
                                                    padding: 0.875rem 1.5rem 0.875rem 0.875rem;
                                                    font-size: 0.8125rem;
                                                    background-color: #008cba;
                                                    border-color: #0078a0;
                                                    color: white;
                                                    box-shadow: 0px 1px 5px 0 darkslategrey;
                                                    z-index:1002;
                                                }
                                                .alert-box.success {
                                                    background-color: #f6921e;
                                                    border-color: #3a945b;
                                                    color: white;
                                                    text-align: center;
                                                    font-size: 20px;
                                                }
                                            </style>
                                            <script>
                                                is_user_loggedin = "<?php echo $user_session_id ?>";
                                                site_url = "<?php echo site_url(); ?>";
                                                cat_img_link = "<?php echo CATEGORIES_IMG ?>";
                                                flash_success_msg = "<?php echo $this->session->flashdata('success'); ?>";
                                                flash_error_msg = "<?php echo $this->session->flashdata('error'); ?>";
                                            </script>
                                            </head>
                                            <body>
                                                <?php /*
                                                  if ($this->session->flashdata('success')) { ?>
                                                  <div class="alert alert-box success">
                                                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                  <span class="text-semibold"><?php echo $this->session->flashdata('success'); ?></span>
                                                  </div>
                                                  <?php } if ($this->session->flashdata('error')) { ?>
                                                  <div class="alert alert-box success">
                                                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                  <span class="text-semibold"><?php echo $this->session->flashdata('error'); ?></span>
                                                  </div>
                                                  <?php } */ ?>
                                                <div id="message_container"></div>

                                                <!-- Contact us Popup Start -->
                                                <div class="modal fade" id="contact_us" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="bg_white">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <div class="popup_heading default_width text-center theme_bg">    
                                                                <h4 class="popup_title black">Kontakt</h4>
                                                            </div>
                                                            <div class="modal-body default_width">
                                                                <form id="contact_form" method="post" action="<?php echo site_url('home/send_contact_email') ?>">
                                                                    <div class="pop_des form-horizontal default_width">
                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="text" id="sender_email_contact" name="sender_email_contact" class="black form-control" placeholder="E-Mail">
                                                                                <label id="sender_email_lbl" class="alert-danger" style="display: none;"></label>
                                                                        </div>
                                                                        <div class="popup_field default_width mb15">
                                                                            <select id="sender_subject_contact" name="sender_subject_contact" class="black form-control">
                                                                                <option value="">Betreff</option>
                                                                                <option value="Produktfragen">Produktfragen</option>
                                                                                <option value="Feedback">Feedback</option>
                                                                                <option value="Technische Probleme">Technische Probleme</option>
                                                                                <option value="Geschäftliches">Geschäftliches</option>
                                                                                <option value="Sonstiges">Sonstiges</option>
                                                                            </select>
                                                                            <label id="sender_email_lbl" class="alert-danger" style="display: none;"></label>
                                                                        </div>
                                                                        <div class="popup_field default_width mb15">
                                                                            <textarea class="black contact_textarea form-control" id="email_content_text" name="email_content_text" placeholder="Deine Nachricht..."></textarea>
                                                                            <label id="email_content_lbl" class="alert-danger" style="display: none;"></label>
                                                                        </div>
                                                                        <div class="product_form text-center">
                                                                            <!--<a id="send_contact_email">SENDEN</a>-->
                                                                            <button type="submit" name="send_contact_email" id="send_contact_email" class="btn_register">SENDEN</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="popup_heading default_width text-center theme_bg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Contact us Popup Start -->

                                                <div id="modal" class="popupContainer" style="display:none;">
                                                    <!-- Login Popup Start -->
                                                    <div class="social_login">
                                                        <div class="bg_white">
                                                            <button class="modal_close">&times;</button>
                                                            <div class="popup_heading default_width text-center theme_bg">    
                                                                <h4 class="popup_title black"><?= $this->lang->line('home_log_in') ?></h4>
                                                            </div>

                                                            <div class="modal-body default_width">
                                                                <div class="pop_des form-horizontal default_width">
                                                                    <form method="post" action="login" onsubmit="return frontend_login_validation()">
                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="text" name="txt_login_username" id="txt_login_username" class="black form-control" placeholder="<?= $this->lang->line('home_email') ?>/<?= $this->lang->line('home_username') ?>">
                                                                                <font class="error_color" id="txt_login_username_error"></font>
                                                                        </div>

                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="password" name="txt_login_pass" id="txt_login_pass" class="black form-control" placeholder="<?= $this->lang->line('home_password') ?>">
                                                                                <font class="error_color" id="txt_login_pass_error"></font>
                                                                        </div>

                                                                        <div class="product_form default_width text-center mb25">
                                                                            <button type="submit" name="btn_login" id="btn_login" class="btn_register"><?= $this->lang->line('home_log_in') ?></button>
                                                                        </div>
                                                                        <div class="gt_forget_wrap default_width mb20">
                                                                            <div class="remember_wrap">
                                                                                <input id="txt_stay_logged_in" type="checkbox" name="txt_stay_logged_in">
                                                                                    <label for="txt_stay_logged_in"><span></span><?= $this->lang->line('home_stay_logged_in') ?></label>
                                                                            </div>
                                                                            <div class="forget_password"><a class="gt_forget_hide" href="#" id="password_forget"><?= $this->lang->line('home_forgot_password') ?></a></div>
                                                                        </div>
                                                                        <div class="gt_or_wrap default_width text-center mb15"><span><?= $this->lang->line('or') ?></span></div>
                                                                        <div class="fb_btn text-center default_width">
                                                                            <a href="<?php echo site_url() . 'login/facebook'; ?>">
                                                                                <img src="assets/images/facebook.png" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class="popup_heading  default_width text-center theme_bg">
                                                                <p class="white no_margin"><?= $this->lang->line('home_not_member_yet') ?> <b><a id="gt_hide_registration" href="#"><?= $this->lang->line('home_register_now') ?></a></b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Login Popup Start -->

                                                    <!-- Forget Popup Start -->
                                                    <div class="user_login">
                                                        <div class="bg_white">
                                                            <button class="modal_close">&times;</button>
                                                            <div class="popup_heading default_width text-center theme_bg">    
                                                                <h4 class="popup_title black small-title">Passwort vergessen?</h4>
                                                            </div>

                                                            <div class="modal-body default_width">
                                                                <form method="post" id="forgot_pwd_form" onsubmit="return frontend_forgot_pwd_validation()">
                                                                    <div class="pop_des form-horizontal default_width">
                                                                        <div class="popup_sub_hdg text-center default_width mb20">
                                                                            <p class="black"><?php echo utf8_encode("Gib bitte deine E-Mail Adresse oder deinen Benutzernamen ein. Du erh�lst eine E-Mail mit einem neuen Passwort.") ?></p>
                                                                        </div>
                                                                        <div id="forgot_pwd_success" class="text-center default_width"></div>
                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="text" name="user_name" id="forgot_user_name" class="black form-control" placeholder="Benutzername / E-Mail" required="required">
                                                                                <span class="form-control-feedback forgot_custom_loader forgot_user_name_custom_loader" style="display:none"><img src="assets/images/loader.gif" id="img_common_loader"></span>
                                                                                <font class="error_color" id="forgot_user_name_error"></font>
                                                                        </div>
                                                                        <div class="product_form default_width text-center">
                                                                            <!--<a href="#">Passwort zurücksetzen</a>-->
                                                                            <button type="submit" class="forgot_pwd_btn" id="forgot_pwd_btn"><?php echo utf8_encode("Passwort zur�cksetzen") ?></button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="popup_heading  default_width text-center theme_bg">
                                                                <p class="white no_margin"><?php echo utf8_encode("Zur�ck zum") ?>  <b class="forget_password_again">Einloggen!</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Forget Popup Start -->

                                                    <!-- Registration Popup Start -->
                                                    <div class="user_register">
                                                        <div class="bg_white">
                                                            <button class="modal_close">&times;</button>
                                                            <div class="popup_heading default_width text-center theme_bg">    
                                                                <h4 class="popup_title black"><?= $this->lang->line('home_registration') ?></h4>
                                                            </div>

                                                            <div class="modal-body default_width">
                                                                <div class="pop_des form-horizontal default_width">
                                                                    <!--<form method="post" action="<?php echo site_url('register'); ?>" onsubmit="return frontend_register_validation()" id="user_register_form">-->
                                                                    <form method="post" action="<?php echo site_url('register'); ?>" id="user_register_form">
                                                                        <div class="popup_field default_width mb15 has-feedback">
                                                                            <!--<input type="text" name="txt_username" id="txt_username" class="black form-control" placeholder="<?php echo $this->lang->line('home_username') ?>" onblur="register_username_validation(this.value)">-->
                                                                            <input type="text" name="txt_username" id="txt_username" class="black form-control" placeholder="<?php echo $this->lang->line('home_username') ?>">
                                                                                <span class="form-control-feedback custom_loader username_custom_loader" style="display:none"><img src="assets/images/loader.gif" id="img_common_loader"></span>
                                                                                <font class="error_color" id="txt_username_error"></font>
                                                                        </div>
                                                                        <div class="popup_field default_width mb15 has-feedback">
                                                                            <!--<input type="text" name="txt_email" id="txt_email" class="black form-control" placeholder="<?php echo $this->lang->line('home_email') ?>" onblur="register_email_validation(this.value)">-->
                                                                            <input type="text" name="txt_email" id="txt_email" class="black form-control" placeholder="<?php echo $this->lang->line('home_email') ?>">
                                                                                <span class="form-control-feedback custom_loader email_custom_loader" style="display:none"><img src="assets/images/loader.gif" id="img_common_loader"></span>
                                                                                <font class="error_color" id="txt_email_error"></font>
                                                                        </div>
                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="password" name="txt_pass" id="txt_pass" class="black form-control" placeholder="<?php echo $this->lang->line('home_password') ?>">
                                                                                <font class="error_color" id="txt_pass_error"></font>
                                                                        </div>
                                                                        <div class="popup_field default_width mb15">
                                                                            <input type="password" name="txt_c_pass" id="txt_c_pass" class="black form-control" placeholder="<?php echo $this->lang->line('home_reenter_password') ?>">
                                                                                <font class="error_color" id="txt_c_pass_error"></font>
                                                                        </div>
                                                                        <!--                                <div>
                                                                                                            <input type="checkbox" id="txt_i_agree" name="txt_i_agree">
                                                                                                        </div>-->
                                                                        <div class="gt_forget_wrap text-center default_width mb15 gt_reg_pop_check">
                                                                            <div class="remember_wrap">
                                                                                <input type="checkbox" id="txt_i_agree" name="txt_i_agree">
                                                                                    <label for="txt_i_agree"><span></span>Hiermit stimme ich den AGBs zu.</label>
                                                                            </div>
                                                                            <font class="error_color" id="txt_i_agree_error"></font>
                                                                        </div>
                                                                        <div class="gt_forget_wrap text-center default_width mb15 gt_reg_pop_check">
                                                                            <div class="remember_wrap">
                                                                                <input id="checknox3" type="checkbox">
                                                                                    <label for="checknox3"><span></span><?php echo $this->lang->line('home_subscribe_to_newsletter') ?> </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="product_form default_width text-center mb20">
                                                                            <button type="submit" name="btn_register" id="btn_register" class="btn_register"><?php echo $this->lang->line('home_register_now') ?></button>
                                                                        </div>
                                                                    </form>
                                                                    <div class="gt_or_wrap default_width text-center mb15"><span><?php echo $this->lang->line('or') ?></span></div>
                                                                    <div class="fb_btn text-center default_width">
                                                                        <a href="<?php echo base_url() . 'login/facebook'; ?>">
                                                                            <img src="assets/images/facebook.png" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="popup_heading  default_width text-center theme_bg">
                                                                <p class="white no_margin"><?php echo $this->lang->line('home_back_to') ?>  <b class="gt_regitration_ext">Einloggen!</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Registration Popup Start -->
                                                </div>

                                                <!-- Tab View Popup Start -->
                                                <div class="modal fade" id="product_tab_view" role="dialog">
                                                    <div class="modal-dialog tab_popup_width">
                                                        <!-- Modal content-->
                                                        <div class="bg_white">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <div class="popup_heading default_width text-center theme_bg">    
                                                                <h4 class="popup_title black" id="account_tab_title">Einstellungen</h4>
                                                            </div>

                                                            <div class="modal-body no_padding default_width">
                                                                <div class="gt_tab_link">
                                                                    <ul data-tabs="tabs">
                                                                        <li class="active">
                                                                            <a data-toggle="tab" href="#tab1">
                                                                                <i class="res_tab_icon icon-cogwheel"></i>
                                                                                <span>Allgemein</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tab" href="#tab2">
                                                                                <i class="res_tab_icon icon-cart"></i>
                                                                                <span>Deine Bestellungen</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tab" href="#tab3">
                                                                                <i class="res_tab_icon icon-communication"></i>
                                                                                <span>Deine Bewertungen</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tab" href="#tab4">
                                                                                <i class="res_tab_icon icon-favorite"></i>
                                                                                <span>Dein Rang</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-content">

                                                                    <div class="tab-pane active" id="tab1">
                                                                        <div class="tab_popup_des form-horizontal">
                                                                            <?php if ($_SESSION['login_user_type'] != 'facebook') { ?>
                                                                                <form id="update_email_form" action="<?php echo site_url('users/general_settings') ?>" method="post">
                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span>Aktuelles Passwort:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="password" name="txt_current_password_email" id="txt_current_password_email" class="form-control <?php
                                                                                                if (form_error('txt_current_password_email') != '') {
                                                                                                    echo 'form_error_text';
                                                                                                }
                                                                                                ?>" placeholder="Gib dein aktuelles Passwort ein">
                                                                                                    <font id="txt_current_password_email_error" class="form_error" for="txt_current_password_email"><?= form_error('txt_current_password_email') ?></font>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span>Benutzername:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="text" name="txt_user_name" id="txt_user_name" class="form-control" value="<?= $this->session->userdata('session_user_name') ?>" disabled>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span>E-Mail:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="text" name="txt_new_email" id="txt_new_email" class="form-control <?php
                                                                                                if (form_error('txt_new_email') != '') {
                                                                                                    echo 'form_error_text';
                                                                                                }
                                                                                                ?>" placeholder="Gib deine neue E-Mail ein">
                                                                                                    <font id="txt_new_email_error" class="form_error" for="txt_new_email"><?= form_error('txt_new_email') ?></font>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="product_form default_width text-center mb20">
                                                                                        <button type="submit" class="product_form_update_email_btn btn-hover-effect"><?php echo utf8_encode("E-Mail �ndern") ?></button>
                                                                                    </div>
                                                                                </form>

                                                                                <form id="update_password_form" action="<?php echo site_url('users/change_password') ?>" method="post">
                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span>Aktuelles Passwort:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="password" name="txt_current_password_pass" id="txt_current_password_pass" class="form-control <?php
                                                                                                if (form_error('txt_current_password_pass') != '') {
                                                                                                    echo 'form_error_text';
                                                                                                }
                                                                                                ?>" value="<?php echo set_value('txt_current_password_pass'); ?>">
                                                                                                    <font id="txt_current_password_pass_error" class="form_error" for="txt_current_password_pass"><?= form_error('txt_current_password_pass') ?></font>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span>Neues Passwort:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="password" name="txt_new_password" id="txt_new_password" class="form-control <?php
                                                                                                if (form_error('txt_new_password') != '') {
                                                                                                    echo 'form_error_text';
                                                                                                }
                                                                                                ?>" value="<?php echo set_value('txt_new_password'); ?>">
                                                                                                    <font id="txt_new_password_error" class="form_error" for="txt_new_password"><?= form_error('txt_new_password') ?></font>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-5">
                                                                                            <div class="popup_field_label default_width mb15">
                                                                                                <span style="text-transform:inherit !important">Neues Passwort wiederholen:</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7">
                                                                                            <div class="popup_field default_width mb15">
                                                                                                <!--<input type="text" name="user_name" class="form-control">-->
                                                                                                <input type="password" name="txt_new_re_password" id="txt_new_re_password" class="form-control <?php
                                                                                                if (form_error('txt_new_re_password') != '') {
                                                                                                    echo 'form_error_text';
                                                                                                }
                                                                                                ?>" value="<?php echo set_value('txt_new_re_password'); ?>">
                                                                                                    <font id="txt_new_re_password_error" class="form_error" for="txt_new_re_password"><?= form_error('txt_new_re_password') ?></font>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="product_form default_width text-center mb20">
                                                                                        <!--<a href="#">Passwort ändern</a>-->
                                                                                        <button type="submit" class="product_form_update_email_btn btn-hover-effect"><?php echo utf8_encode("Passwort �ndern") ?></button>
                                                                                    </div>
                                                                                </form>
                                                                            <?php } ?>
                                                                            <div class="popup_sub_hdg text-center default_width mb20">
                                                                                <p class="black">
                                                                                    <?php echo utf8_encode('Um deinen Account zu l�schen, schreibe �L�SCHEN� in das Textfeld.'); ?>
                                                                                </p>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-5">
                                                                                    <div class="popup_field default_width">
                                                                                        <input type="text" name="delete_acnt_message" id="delete_acnt_message" class="form-control">
                                                                                            <div id="delete_acnt_message_error" class="custom_form_error" for="delete_acnt_message"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-7">
                                                                                    <div class="popup_account_token default_width text-center">
                                                                                        <a href="<?php echo site_url('users/delete_request') ?>" onclick="return checkDelete(this)"><?php echo utf8_encode("Meinen Account l�schen") ?></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab2">
                                                                        <div class="gt_product_list_wrap gt_tab_product_list bg_white" id="gt_buyer_scroll">
                                                                            <?php if ($this->orders_user) { ?>
                                                                                <div class="short">
                                                                                    <form class="form-inline">
                                                                                        <div class="form-group">
                                                                                            <p class="form-control-static">Sortieren nach</p>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <select class="form-control" id="order_sort_option">
                                                                                                <!--<option value="o.id">Sort by orders</option>-->
                                                                                                <option value="o.order_date">Datum</option>
                                                                                                <option value="name_gr">Produktname</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                                <div id="order_tab_list">
                                                                                    <?php foreach ($this->orders_user as $order) { ?>
                                                                                        <div class="gt_product_list mb30">
                                                                                            <!--<div class="gt_product_img">-->
                                                                                            <div class="buyer_product_img">
                                                                                                <?php
                                                                                                $images_arr = explode(',', $order['images']);
                                                                                                ?>
                                                                                                <img src="uploads/supplements/<?php echo $images_arr[0]; ?>" alt="">
                                                                                            </div>
                                                                                            <div class="gt_product_price">
                                                                                                <h5 class="theme_color mb10">Produktname</h5>
                                                                                                <h4 class="black"><?php echo $order['name_gr']; ?></h4>
                                                                                            </div>
                                                                                            <div class="gt_product_price">
                                                                                                <h5 class="theme_color mb10">Preis:</h5>
                                                                                                <h4 class="black"><?php echo $order['price']; ?>€</h4>
                                                                                            </div>
                                                                                            <div class="gt_product_cart_btn">
                                                                                                <a href="<?php echo $order['amazon_link']; ?>" target="_blnk" onclick="redirect_thankyou(this)" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>
                                                                                            </div>
                                                                                            <span class="date"><?php echo date('d-m-Y', strtotime($order['order_date'])); ?></span>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                                <?php
                                                                            } else {
                                                                                echo "<div class='tab_product_buyer_wrap mb25' style='padding: 15px;text-align: center;'><i class='fa fa-info-circle'></i>&nbsp; Noch keine Bestellungen!</div>";
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane" id="tab3">
                                                                        <div class="buyer_tab_outer_wrap" id="gt_buyer_scroll">
                                                                            <?php if ($this->user_reviews) { ?>
                                                                                <div class="short">
                                                                                    <form class="form-inline">
                                                                                        <div class="form-group">
                                                                                            <p class="form-control-static">Sortieren nach</p>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <select class="form-control" id="review_sort_option">
                                                                                                <option value="created">Datum</option>
                                                                                                <option value="product_name">Produktname</option>
                                                                                                <option value="likes">Meiste Likes</option>
                                                                                                <option value="dislikes">Meiste Dislikes</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                                <div id="user_reviews_div">
                                                                                    <?php
                                                                                    foreach ($this->user_reviews as $user_review) {
                                                                                        ?>
                                                                                        <div class="tab_product_buyer_wrap mb25">
                                                                                            <a href="<?php echo site_url('supplements/view_supplement/' . $user_review['supplement_id']) ?>">
                                                                                                <div class="buyer_product_img">
                                                                                                    <?php $image = explode(",", $user_review['images']); ?>
                                                                                                    <img src="<?php echo SUPPLEMENTS_IMG . $image[0] ?>" alt="<?php echo $user_review['product_name'] ?>">                                                    
                                                                                                </div>
                                                                                            </a>
                                                                                            <div class="buyer_product_des">
                                                                                                <div class="buyer_name mb15">
                                                                                                    <?php if ($user_review['rank_icon'] != '') { ?>
                                                                                                        <img src="<?php echo RANKS_IMG . $user_review['rank_icon'] ?>" alt="">
                                                                                                        <?php } else { ?>
                                                                                                            <img src="assets/images/buyer_img.png" alt="">
                                                                                                            <?php } ?>
                                                                                                            <p class="no_margin black">
                                                                                                                <!--<span class="theme_color">-->
                                                                                                                <?php
                                                                                                                if ($user_review['color'] != '') {
                                                                                                                    $user_name_style = 'style="color:' . $user_review['color'] . '"';
                                                                                                                } else {
                                                                                                                    $user_name_style = 'class="theme_color"';
                                                                                                                }
                                                                                                                ?>
                                                                                                                <span <?php echo $user_name_style ?>>
                                                                                                                    <?php echo $user_review['nickname'] ?> 
                                                                                                                </span> - <?php echo date('d-m-Y', strtotime($user_review['created'])); ?>.</p>
                                                                                                            </div>
                                                                                                            <div class="product_gradiant mb15">
                                                                                                                <ul>
                                                                                                                    <?php if (!empty($user_review['flavour'])) { ?>
                                                                                                                        <li>
                                                                                                                            <span class="theme_color">Geschmack:</span>
                                                                                                                            <?php echo $user_review['flavour'] ?>
                                                                                                                        </li>
                                                                                                                    <?php } ?>
                                                                                                                    <?php if (!empty($user_review['liquid'])) { ?>
                                                                                                                        <li>
                                                                                                                            <span class="theme_color">Gemischt mit:</span>
                                                                                                                            <?php echo $user_review['liquid'] ?>
                                                                                                                        </li>
                                                                                                                    <?php } ?>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="buyer_des default_width mb20">
                                                                                                                <p style="word-wrap: break-word"><?php echo $user_review['comment'] ?></p>
                                                                                                            </div>
                                                                                                            <div class="product_rating">
                                                                                                                <?php
                                                                                                                $properties = explode(",", $user_review['properties']);
                                                                                                                $property_ratings = explode(",", $user_review['property_ratings']);
                                                                                                                ?>
                                                                                                                <ul>
                                                                                                                    <?php foreach ($properties as $key => $property) { ?>
                                                                                                                        <li>
                                                                                                                            <span><?php echo $property ?></span>
                                                                                                                            <i class="fa fa-star theme_color"></i>
                                                                                                                            <p><?php echo $property_ratings[$key] ?></p>
                                                                                                                        </li>
                                                                                                                    <?php } ?>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="gt_product_like">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        <img src="assets/images/like.png" alt="Like" id="like_button_<?php echo $user_review['id']; ?>">
                                                                                                                            <p id="total_likes_<?php echo $user_review['id']; ?>" class="user_review_like_txt"><?php echo $user_review['likes']; ?></p>
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        <img src="assets/images/unlike.png" alt="Dislike" id="dislike_button_<?php echo $user_review['id']; ?>">
                                                                                                                            <p id="total_dislikes_<?php echo $user_review['id']; ?>" class="user_review_like_txt"><?php echo $user_review['dislikes']; ?></p>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            </div>
                                                                                                            </div>
                                                                                                        <?php }
                                                                                                        ?>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        echo "<div class='tab_product_buyer_wrap mb25' style='padding: 15px;text-align: center;'><i class='fa fa-info-circle'></i>&nbsp; Noch keine Bewertungen!</div>";
                                                                                                    }
                                                                                                    ?>
                                                                                                    </div>
                                                                                                    </div>

                                                                                                    <div class="tab-pane" id="tab4">
                                                                                                        <div class="buyer_tab_outer_wrap">
                                                                                                            <div class="top_range_hdg_bg">
                                                                                                                <div class="tab_range_des_bg default_width">
                                                                                                                    <div class="tab_range_rank">
                                                                                                                        &nbsp;
                                                                                                                    </div>
                                                                                                                    <div class="tab_range_img">
                                                                                                                        &nbsp;
                                                                                                                    </div>
                                                                                                                    <div class="tab_range_name black">
                                                                                                                        RANG
                                                                                                                    </div>
                                                                                                                    <div class="tab_range_points">
                                                                                                                        Voraussetzung
                                                                                                                    </div>
                                                                                                                    <!--                                            <div class="tab_range_des">
                                                                                                                                                                    Prämie
                                                                                                                                                                </div>-->
                                                                                                                </div><div class="clearfix"></div>
                                                                                                            </div>
                                                                                                            <div class="tab4_scroll" id="gt_buyer_scroll">
                                                                                                                <?php foreach ($this->ranks as $rank) { ?>
                                                                                                                    <?php
                                                                                                                    $rank_class = '';
                                                                                                                    if ($rank['id'] == $this->user_rank['rank_id']) {
                                                                                                                        $rank_class = 'active';
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                    <div class="tab_range_des_bg default_width <?php echo $rank_class ?>">
                                                                                                                        <div class="tab_range_rank">
                                                                                                                            <?php echo $rank['rank'] ?>
                                                                                                                        </div>
                                                                                                                        <div class="tab_range_img">
                                                                                                                            <img src="<?php echo RANKS_IMG . $rank['icon'] ?>" alt="<?php echo $rank['name'] ?>" width="79px" height="76px">
                                                                                                                        </div>
                                                                                                                        <div class="tab_range_name black">
                                                                                                                            <?php echo $rank['name'] ?>
                                                                                                                        </div>
                                                                                                                        <div class="tab_range_points">
                                                                                                                            <ul>
                                                                                                                                <li>
                                                                                                                                    <?php echo $rank['requirement'] ?>
                                                                                                                                </li>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                        <!--                                                <div class="tab_range_des">
                                                                                                                                                                            <p class="no_margin black"><?php echo $rank['reward'] ?></p>
                                                                                                                                                                        </div>-->
                                                                                                                    </div>
                                                                                                                <?php } ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!--                                <div class="buyer_tab_outer_wrap">
                                                                                                                                            <table>
                                                                                                                                                <thead>
                                                                                                                                                    <tr>
                                                                                                                                                        <th></th>
                                                                                                                                                        <th></th>
                                                                                                                                                        <th>Range</th>
                                                                                                                                                        <th>Voraussetzung</th>
                                                                                                                                                        <th>Prämie</th>
                                                                                                                                                    </tr>
                                                                                                                                                </thead>
                                                                                                                                                <tbody>
                                                                                                        <?php foreach ($this->ranks as $rank) { ?>
                                                                                                            <?php
                                                                                                            $rank_class = '';
                                                                                                            if ($rank['id'] == $this->user_rank['rank_id']) {
                                                                                                                $rank_class = 'active';
                                                                                                            }
                                                                                                            ?>
                                                                                                                                                                                                                                <tr class="<?php echo $rank_class ?>">
                                                                                                                                                                                                                                <td>
                                                                                                            <?php echo $rank['rank'] ?>
                                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                                <td>
                                                                                                                                                                                                                                <img src="<?php echo RANKS_IMG . $rank['icon'] ?>" alt="<?php echo $rank['name'] ?>" width="79px" height="76px">
                                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                                <td>
                                                                                                            <?php echo $rank['name'] ?>
                                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                                <td>
                                                                                                                                                                                                                                <ul>
                                                                                                                                                                                                                                <li>
                                                                                                            <?php echo $rank['requirement'] ?>
                                                                                                                                                                                                                                </li>
                                                                                                                                                                                                                                </ul>
                                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                                <td>
                                                                                                                                                                                                                                <p class="no_margin black"><?php echo $rank['reward'] ?></p>
                                                                                                                                                                                                                                </td>
                                                                                                                                                                                                                                </tr>
                                                                                                        <?php } ?>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                        </div>-->
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="popup_heading  default_width text-center theme_bg">
                                                                                                        <p class="white no_margin"></p>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <!-- Tab View Popup Start -->

                                                                                                    <!-- Tab Add Product Popup start -->
                                                                                                    <div class="modal fade" id="add_product_tab_view" role="dialog">
                                                                                                        <div class="modal-dialog tab_popup_width addproduct" >
                                                                                                            <!-- Modal content-->
                                                                                                            <div class="bg_white">
                                                                                                                <button class="modal_close" data-dismiss="modal">&times;</button>
                                                                                                                <div class="popup_heading default_width text-center theme_bg">    
                                                                                                                    <h4 class="popup_title black">PRODUKT HINZUFÜGEN</h4>
                                                                                                                </div>
                                                                                                                <div class="modal-body default_width" style="min-height:500px;">
                                                                                                                    <form method="post" id="add_product_form" action="<?php echo site_url('home/add_product_request') ?>">
                                                                                                                        <div class="pop_des form-horizontal default_width">
                                                                                                                            <p class="black" style="text-align:center; font-weight:bold;">Dein gewünschtes Produkt fehlt im Sortiment? Gib uns jetzt Bescheid, indem du dieses Formular ausfüllst.</p>
                                                                                                                            <div class="popup_field default_width mb15">
                                                                                                                                <input type="text" name="add_product_name" class="black form-control" placeholder="Produktname">
                                                                                                                            </div>
                                                                                                                            <div class="popup_field default_width mb15">
                                                                                                                                <select name="add_product_category">
                                                                                                                                    <option value="">Kategorie</option>
                                                                                                                                    <?php
                                                                                                                                    foreach ($this->add_productcategories as $category) {
                                                                                                                                        echo "<option value='" . $category['id'] . "'>" . $category['name_gr'] . "</option>";
                                                                                                                                    }
                                                                                                                                    ?>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                            <div class="popup_field default_width mb15">
                                                                                                                                <input type="text" name="add_product_amazonlink" class="black form-control" placeholder="Amazon-Link">
                                                                                                                            </div>
                                                                                                                            <div class="popup_field default_width mb15">
                                                                                                                                <textarea name="add_product_description" style="resize:none;" class="black form-control" placeholder="Füge ein Kommentar hinzu…"></textarea>
                                                                                                                            </div>
                                                                                                                            <p class="black">Möchtest du informiert werden, wenn das Produkt hinzugefügt wurde?</p>
                                                                                                                            <div class="popup_field default_width mb15">
                                                                                                                                <input type="text" name="add_product_email" class="black form-control" placeholder="E-Mail (optional)">
                                                                                                                            </div>
                                                                                                                            <div class="product_form default_width text-center ">
                                                                                                                                <button type="submit" class="product_form_update_email_btn">PRODUKT HINZUFÜGEN</button>
                                                                                                                                <!--<a href="#">PRODUKT HINZUFUGEN</a>-->
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </form>
                                                                                                                </div>
                                                                                                                <div class="popup_heading  default_width text-center theme_bg"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!-- Tab Add Product Popup End -->

                                                                                                    <!-- Tab Login Alert modal -->
                                                                                                    <div class="modal fade" id="login_check_modal" role="dialog">
                                                                                                        <div class="modal-dialog">
                                                                                                            <!-- Modal content-->
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <div class="modal-body">
                                                                                                                <div class="alert alert-danger" role="alert" id="review_alert_message">Please login or register to post review</div>
                                                                                                            </div>        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!-- Tab Login Success modal -->
                                                                                                    <div class="modal fade" id="success_alert_modal" role="dialog">
                                                                                                        <div class="modal-dialog">
                                                                                                            <!-- Modal content-->
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <div class="modal-body">
                                                                                                                <div class="alert alert-success" role="alert" id="success_alert_message"></div>
                                                                                                            </div>        
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!-- Tab Login Alert modal -->

                                                                                                    <!--Wrapper Start-->  
                                                                                                    <div class="main_wrapper">
                                                                                                        <header class="header_bg">
                                                                                                            <div class="container">
                                                                                                                <div class="logo"><a href="<?php echo site_url('/') ?>"><img src="assets/images/logo.png" alt=""></a></div>
                                                                                                                <div class="header_r">
                                                                                                                    <button class="gt_mobile_menu gt_theme_bg2">
                                                                                                                        <span class="icon-bar"></span>
                                                                                                                        <span class="icon-bar"></span>
                                                                                                                        <span class="icon-bar"></span>
                                                                                                                    </button>
                                                                                                                    <nav class="navigation">
                                                                                                                        <ul>
                                                                                                                            <li class="visible-xs cart-li">
                                                                                                                                <div class="cart_wrap">
                                                                                                                                    <a href="<?php echo site_url('cart') ?>"><i class="fa fa-shopping-cart"></i></a>
                                                                                                                                    <span class="bigg cartitem_count" id="cartitem_count"><?php echo $this->cartitem_count; ?></span>
                                                                                                                                </div>
                                                                                                                            </li>
                                                                                                                            <li class="<?php
                                                                                                                            if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') {
                                                                                                                                echo 'active';
                                                                                                                            }
                                                                                                                            ?>"><a href="<?php echo base_url(); ?>"><?= $this->lang->line('home_start_page') ?></a></li>
                                                                                                                            <li class="<?php
                                                                                                                            if ($this->uri->segment(2) == 'hows_work') {
                                                                                                                                echo 'active';
                                                                                                                            }
                                                                                                                            ?>"><a href="pages/hows_work">So funktioniert's</a></li>
                                                                                                                            <li class="<?php
                                                                                                                            if ($this->uri->segment(2) == 'about_us') {
                                                                                                                                echo 'active';
                                                                                                                            }
                                                                                                                            ?>"><a href="pages/about_us">Über uns</a></li>
                                                                                                                            <li><a href="#" data-toggle="modal" data-target="#contact_us"><?= $this->lang->line('home_contact_us') ?></a></li>
                                                                                                                            <?php if ($user_session_id == '') { ?>
                                                                                                                                <li class="login_btn">
                                                                                                                                    <a id="modal_trigger" href="#modal"><?= $this->lang->line('home_log_in') ?></a>
                                                                                                                                </li>
                                                                                                                            <?php } else { ?>
                                                                                                                                <li class="dropdown login_wrap">
                                                                                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src='uploads/extra/acc_preview3.png' height="30px"> <span class="caret" style="color:#f6921e"></span></a>
                                                                                                                                    <ul class="dropdown-menu">
                                                                                                                                        <!--<li><a href="setting">Setting</a></li>-->
                                                                                                                                        <li><a href="#" data-toggle="modal" data-target="#product_tab_view">Einstellungen</a></li>
                                                                                                                                        <li role="separator" class="divider"></li>
                                                                                                                                        <li><a href="<?php echo site_url('logout'); ?>">Abmelden</a></li>
                                                                                                                                    </ul>
                                                                                                                                </li>
                                                                                                                            <?php } ?>
                                                                                                                        </ul>
                                                                                                                    </nav>
                                                                                                                    <div class="cart_wrap hidden-xs">
                                                                                                                        <a href="<?php echo site_url('cart') ?>"><i class="fa fa-shopping-cart"></i></a>
                                                                                                                        <span id="cartitem_count" class="cartitem_count"><?php echo $this->cartitem_count; ?></span>
                                                                                                                    </div>
                                                                                                                    <button class="search-btn visible-xs" id="searchme"><i class="fa fa-search"></i></button>
                                                                                                                </div>
                                                                                                                <div class="search_form " id="searchbar">
                                                                                                                    <form onsubmit="return false;">
                                                                                                                        <input type="search" placeholder="Produkt suchen…" id="typehead_search" autocomplete="off">
                                                                                                                            <button type="submit" onclick="focusTextBox();"></button>
                                                                                                                    </form>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </header>
                                                                                                        <!-- Main content -->
                                                                                                        <div>
                                                                                                            <?php echo $body; ?>
                                                                                                        </div>
                                                                                                        <!-- /main content -->
                                                                                                        <footer class="footer_bg">
                                                                                                            <div class="container">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-6">
                                                                                                                        <div class="site_map default_width">
                                                                                                                            <ul>
                                                                                                                                <li><a href="<?php echo site_url('pages/impressum') ?>">Impressum</a></li>
                                                                                                                                <li><a href="<?php echo site_url('pages/agbs') ?>">AGBs</a></li>
                                                                                                                                <li><a href="<?php echo site_url('pages/datenschutzerklarung') ?>">Datenschutzerklarung</a></li>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-6">
                                                                                                                        <div class="social_media pull-right">
                                                                                                                            <ul>
                                                                                                                                <li><a href="https://www.facebook.com/SupMatch/" target="blank"><i class="fa fa-facebook"></i></a></li>
                                                                                                                                <!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
                                                                                                                                <li><a href="https://www.instagram.com/supmatch/" target="blank"><i class="fa fa-instagram"></i></a></li>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                        <div class="site_map pull-right">
                                                                                                                            <ul>
                                                                                                                                <li>© <?php echo date('Y'); ?> <a href="<?php echo site_url('/') ?>"> SupMatch.de</a></li>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!--<div id="slider-range-max"></div>-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </footer>
                                                                                                    </div>
                                                                                                    <!--Wrapper End-->
                                                                                                    <!--Bootstrap core JavaScript-->
                                                                                                    <script src="assets/js/jquery.js"></script>

                                                                                                    <script src="assets/js/bootstrap.min.js"></script>
                                                                                                    <script src="assets/js/jquery.nstSlider.js"></script>
                                                                                                    <script src="assets/js/jquery.leanModal.min.js"></script>
                                                                                                    <!--Custom JavaScript-->
                                                                                                    <script src="assets/js/custom.js"></script>
                                                                                                    <script src="assets/js/loader_function.js" charset="ISO-8859-1"></script>
                                                                                                    <!--<script src="assets/admin/js/plugins/notifications/pnotify.min.js"></script>-->
                                                                                                    <!--<script src="assets/pnotify/pnotify.custom.min.js"></script>-->
                                                                                                    <script src="assets/admin/js/plugins/forms/validation/validate.min.js"></script>
                                                                                                    <!-- Type ahead jquery-->
                                                                                                    <!--<script src="assets/js/bloodhound.js"></script>-->
                                                                                                    <!--<script src="assets/js/typeahead.jquery.js"></script>-->
                                                                                                    <script src="assets/js/typeahead.bundle.js"></script>
                                                                                                    <script src="assets/js/nicescroll.js"></script>
                                                                                                    <script src="assets/js/fakeLoader.js"></script>
                                                                                                    <!--<script src="assets/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>-->
                                                                                                    <script src="assets/js/star-rating.js" type="text/javascript"></script>
                                                                                                    <script type="text/javascript" src="assets/js/jquery.fancybox.js?v=2.1.5"></script>
                                                                                                    <link href="assets/css/fancybox/jquery.fancybox.css?v=2.1.5">
                                                                                                        <link rel="stylesheet" href="assets/css/jquery-ui.css">
                                                                                                            <script src="assets/js/jquery-ui.js"></script>
                                                                                                            <script type="text/javascript">
                                                                                                                                $(document).ready(function () {
                                                                                                                                    var session_user_id = '';
                                                                                                                                    session_user_id = '<?php echo $this->session->userdata('session_user_id'); ?>';
                                                                                                                                    if (session_user_id != '') {
                                                                                                                                        encode_session_id = '<?php echo base64_encode($this->session->userdata('session_user_id')); ?>';
                                                                                                                                        //alert(encode_session_id);
                                                                                                                                        var date = new Date();
                                                                                                                                        var minutes = 30;
                                                                                                                                        date.setTime(date.getTime() + (minutes * 60 * 1000));
                                                                                                                                        document.cookie = 'cookie_user_id=' + encode_session_id + '; expires=' + date + '; path=/';
                                                                                                                                    }
                                                                                                                                    
                                                                                                                                    /*if ($('#gt_product_list_wrap2').length) {
                                                                                                                                     $('#gt_product_list_wrap2').getNiceScroll().show();
                                                                                                                                     }*/
                                                                                                                                });
                                                                                                                                //-- Change email form validation
                                                                                                                                $('#update_email_form').validate({
                                                                                                                                    errorClass: 'custom_form_error',
                                                                                                                                    rules: {
                                                                                                                                        txt_current_password_email: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        txt_new_email: {
                                                                                                                                            required: true,
                                                                                                                                            email: true,
                                                                                                                                            remote: '<?php echo site_url('users/checkUniqueEmail'); ?>'
                                                                                                                                        },
                                                                                                                                    },
                                                                                                                                    messages: {
                                                                                                                                        txt_current_password_email: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        txt_new_email: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            remote: $.validator.format("Email already exist!")
                                                                                                                                        },
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                //-- Register form validate
                                                                                                                                $('#user_register_form').validate({
                                                                                                                                    errorClass: 'custom_form_error',
                                                                                                                                    rules: {
                                                                                                                                        txt_username: {
                                                                                                                                            required: true,
                                                                                                                                            remote: '<?php echo site_url('users/check_register_uname'); ?>'
                                                                                                                                        },
                                                                                                                                        txt_email: {
                                                                                                                                            required: true,
                                                                                                                                            email: true,
                                                                                                                                            remote: '<?php echo site_url('users/check_register_email'); ?>'
                                                                                                                                        },
                                                                                                                                        txt_pass: {
                                                                                                                                            required: true,
                                                                                                                                            minlength: 6
                                                                                                                                        },
                                                                                                                                        txt_c_pass: {
                                                                                                                                            required: true,
                                                                                                                                            equalTo: "#txt_pass"
                                                                                                                                        },
                                                                                                                                        txt_i_agree: {
                                                                                                                                            required: true,
                                                                                                                                        }
                                                                                                                                    },
                                                                                                                                    messages: {
                                                                                                                                        txt_username: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            remote: $.validator.format("Benutzername existiert bereits.")
                                                                                                                                        },
                                                                                                                                        txt_email: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            remote: $.validator.format("Diese E-Mail existiert schon.")
                                                                                                                                        },
                                                                                                                                        txt_pass: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            minlength: 'Mindestens 6 Zeichen werden benötigt.'
                                                                                                                                        },
                                                                                                                                        txt_c_pass: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            equalTo: 'Passwort stimmt nicht überein.'
                                                                                                                                        },
                                                                                                                                        txt_i_agree: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                $('#user_register_form').on('submit', function () {
                                                                                                                                    if ($('#user_register_form').valid()) {
                                                                                                                                        if ($('#txt_i_agree').is(":checked")) {
                                                                                                                                            $('#txt_i_agree_error').html('');
                                                                                                                                        } else {
                                                                                                                                            $('#txt_i_agree_error').html('Pflichtfeld');
                                                                                                                                            return false;
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                //-- Update password form validation
                                                                                                                                $('#update_password_form').validate({
                                                                                                                                    errorClass: 'custom_form_error',
                                                                                                                                    rules: {
                                                                                                                                        txt_current_password_pass: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        txt_new_password: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        txt_new_re_password: {
                                                                                                                                            required: true,
                                                                                                                                            equalTo: "#txt_new_password"
                                                                                                                                        },
                                                                                                                                    },
                                                                                                                                    messages: {
                                                                                                                                        txt_current_password_pass: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        txt_new_password: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        txt_new_re_password: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                            equalTo: 'Passwort stimmt nicht überein.'
                                                                                                                                        },
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                //-- Add product form validation
                                                                                                                                $('#add_product_form').validate({
                                                                                                                                    errorClass: 'custom_form_error',
                                                                                                                                    rules: {
                                                                                                                                        add_product_name: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        add_product_category: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        add_product_amazonlink: {
                                                                                                                                            required: true,
                                                                                                                                            url: true,
                                                                                                                                        },
                                                                                                                                        add_product_description: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        /*add_product_email: {
                                                                                                                                         required: true,
                                                                                                                                         email: true,
                                                                                                                                         },*/
                                                                                                                                    },
                                                                                                                                    messages: {
                                                                                                                                        add_product_name: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        add_product_category: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        add_product_amazonlink: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        add_product_description: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                //-- Contact us form validation
                                                                                                                                $('#contact_form').validate({
                                                                                                                                    errorClass: 'custom_form_error',
                                                                                                                                    rules: {
                                                                                                                                        sender_email_contact: {
                                                                                                                                            required: true,
                                                                                                                                            email: true
                                                                                                                                        },
                                                                                                                                        sender_subject_contact: {
                                                                                                                                            required: true,
                                                                                                                                        },
                                                                                                                                        email_content_text: {
                                                                                                                                            required: true,
                                                                                                                                        }
                                                                                                                                    },
                                                                                                                                    messages: {
                                                                                                                                        sender_email_contact: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        sender_subject_contact: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        },
                                                                                                                                        email_content_text: {
                                                                                                                                            required: 'Pflichtfeld',
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                //-- Search bar autocomplete
                                                                                                                                var suggestionEngine = new Bloodhound({
                                                                                                                                    datumTokenizer: function (d) {
                                                                                                                                        return Bloodhound.tokenizers.whitespace(d.value);
                                                                                                                                    },
                                                                                                                                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                                                                                                                                    remote: {
                                                                                                                                        url: '<?php echo site_url() ?>supplements/search_suggest?query=',
                                                                                                                                        replace: function (url, query) {
                                                                                                                                            return url + query;
                                                                                                                                        },
                                                                                                                                        ajax: {
                                                                                                                                            type: "POST",
                                                                                                                                            data: {
                                                                                                                                                q: function () {
                                                                                                                                                    return $('#typehead_search').val()
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                });
                                                                                                                                
                                                                                                                                var suggestionTemplate = function (data) {
                                                                                                                                    var images = data.images;
                                                                                                                                    img = images.split(',');
                                                                                                                                    return '<div><a href="<?php echo site_url() ?>supplements/view_supplement/' + data.id + '" class="suggest_pimg" onclick="return hideScroll()"><img class="image" src="<?php echo base_url() . SUPPLEMENTS_IMG ?>' + img[0] + '" width="30px" height="30px"/></a> <a href="<?php echo site_url() ?>supplements/view_supplement/' + data.id + '" class="suggest_pname" onclick="return hideScroll()"><p class="supplement-name">' + data.name + '</p></a> <a href="<?php echo site_url() ?>supplements/view_supplement/' + data.id + '" class="suggest_plink" onclick="return hideScroll()">MEHR</a></div>';
                                                                                                                                }
                                                                                                                                suggestionEngine.initialize();
                                                                                                                                $('#typehead_search').typeahead(
                                                                                                                                        {
                                                                                                                                            hint: false,
                                                                                                                                            highlight: true,
                                                                                                                                            minLength: 1
                                                                                                                                        },
                                                                                                                                {
                                                                                                                                    name: 'search_location',
                                                                                                                                    displayKey: function (data) {
                                                                                                                                        return data.name;
                                                                                                                                    }, //--- use for change value on change suggestions
                                                                                                                                    source: suggestionEngine,
                                                                                                                                    limit: 100,
                                                                                                                                    templates: {
                                                                                                                                        notFound: '<p class="not"><i class="fa fa-info-circle"></i>&nbsp; Kein Produkt gefunden! <a href="#" data-toggle="modal" data-target="#add_product_tab_view" class="suggest_plink" style="top:0px !important;width: 150px !important;">PRODUKT HINZUFÜGEN</a></p>',
                                                                                                                                        suggestion: suggestionTemplate
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                );
                                                                                                                                /**
                                                                                                                                 * Scrollbar for autocomplete
                                                                                                                                 */
                                                                                                                                $('#typehead_search').keyup(function () {
                                                                                                                                    if (($('.tt-dataset').length) && ($('.tt-menu').hasClass('tt-open'))) {
                                                                                                                                        $('.tt-dataset').niceScroll({
                                                                                                                                            cursorcolor: "#f6921e",
                                                                                                                                            cursorwidth: "8px", // cursor width in pixel (you can also write "5px")
                                                                                                                                            cursorborder: "1px solid #f6921e", // css definition for cursor border
                                                                                                                                            cursorborderradius: "10px", // border radius in pixel for cursor
                                                                                                                                            scrollspeed: 100, // scrolling speed
                                                                                                                                            mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
                                                                                                                                            touchbehavior: false,
                                                                                                                                            cursorminheight: 20,
                                                                                                                                            autohidemode: false,
                                                                                                                                            horizrailenabled: false,
                                                                                                                                            disableoutline: false,
                                                                                                                                            zindex: 1001
                                                                                                                                        });
                                                                                                                                    }
                                                                                                                                }).focus(function () {
                                                                                                                                    if (($('.tt-dataset').length) && ($('.tt-menu').hasClass('tt-open'))) {
                                                                                                                                        //                                                            $('.nicescroll-rails').show();
                                                                                                                                        $('.tt-dataset').getNiceScroll().show();
                                                                                                                                    }
                                                                                                                                }).blur(function () {
                                                                                                                                    $('.tt-dataset').getNiceScroll().hide();
                                                                                                                                });
                                                                                                                                /*Hide scrollbar on product image,name or link click*/
                                                                                                                                function hideScroll() {
                                                                                                                                    $('.tt-dataset').getNiceScroll().hide();
                                                                                                                                }
                                                                                                                                $('.tt-dataset').click(function () {
                                                                                                                                    $('.tt-dataset').getNiceScroll().hide();
                                                                                                                                });
                                                                                                                                /*Review sort change event*/
                                                                                                                                $('#review_sort_option').change(function () {
                                                                                                                                    $.ajax({
                                                                                                                                        url: site_url + "home/get_user_reviews",
                                                                                                                                        type: "POST",
                                                                                                                                        data: {review_sort_option: $(this).val()},
                                                                                                                                        success: function (response) {
                                                                                                                                            $('#user_reviews_div').html(response);
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                });
                                                                                                                                $('#order_sort_option').change(function () {
                                                                                                                                    $.ajax({
                                                                                                                                        url: site_url + "order/get_user_orders",
                                                                                                                                        type: "POST",
                                                                                                                                        data: {order_sort_option: $(this).val()},
                                                                                                                                        success: function (response) {
                                                                                                                                            $('#order_tab_list').html(response);
                                                                                                                                            $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                });
                                                                                                            </script>
                                                                                                            <script type="text/javascript">
                                                                                                                if (window.location.hash && window.location.hash == '#_=_') {
                                                                                                                    window.location.hash = ''; // for older browsers, leaves a # behind
                                                                                                                    history.pushState('', document.title, window.location.pathname); // nice and clean
                                                                                                                    e.preventDefault(); // no page reload
                                                                                                                }
                                                                                                                $(document).ready(function () {
                                                                                                                    $("#searchme").click(function () {
                                                                                                                        $("#searchbar").toggle("fast");
                                                                                                                    });
                                                                                                                });
                                                                                                                
                                                                                                            </script>
                                                                                                            <!-- Loader div-->
                                                                                                            <div id="matching_loader" style="display: none;">
                                                                                                                <input type="hidden" id="matching_perent_hidden_spn" value="0"/>
                                                                                                                <div class="loader">
                                                                                                                    <img src="assets/images/matching_loader1_old.png">
                                                                                                                    <!--<img src="assets/images/matching_loader1.jpg">-->
                                                                                                                        <div id="matching_perent_div"><span id="matching_perent_spn">0</span>%</div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="modal fade" id="youtube_modal" role="dialog">
                                                                                                                <div class="modal-dialog">
                                                                                                                    <!-- Modal content-->
                                                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                                    <div class="modal-body">
                                                                                                                        <iframe width="600" height="400" src="https://www.youtube.com/embed/Ll9l0xKkfgc" frameborder="0" allowfullscreen></iframe>
                                                                                                                    </div>        
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="modal fade" id="review" role="dialog">
                                                                                                                <div class="modal-dialog">
                                                                                                                    <!-- Modal content-->
                                                                                                                    <div class="bg_white">
                                                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                                        <div class="popup_heading default_width text-center theme_bg">    
                                                                                                                            <h4 class="popup_title black small-title">Produkt bewerten</h4>
                                                                                                                        </div>
                                                                                                                        <div class="modal-body default_width">
                                                                                                                            <div class="pop_des form-horizontal default_width">
                                                                                                                                <div class="gt_forget_wrap text-center default_width mb15 gt_reg_pop_check">
                                                                                                                                    <div class="error_color" id="review_confirmation_check_error"></div>
                                                                                                                                    <div class="remember_wrap">
                                                                                                                                        <input id="review_confirmation_check" type="checkbox" name="review_confirmation">
                                                                                                                                            <label for="review_confirmation_check"><span></span>Hiermit bestätige ich, dass ich das Produkt vor der Bewertung getestet habe.</label>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="clearfix"></div>
                                                                                                                                <div class="alert alert-danger" role="alert"> <strong>Beachte: Viele Bewertungen im Internet sind fake! </strong><br> Wir achten jederzeit darauf, unserer Community den größtmöglichen Nutzen zu gewährleisten. Deshalb wollen wir dich darauf aufmerksam machen, dass jede Art von Fake-Kommentaren auf Produkte strafrechtlich verfolgt werden kann. Solltest du also dieses Produkt nicht probiert haben, raten wir dir von einer Bewertung ab.</div>
                                                                                                                                <div class="product_form text-center">
                                                                                                                                    <!--<a href="javascript:void(0)" onclick="submit_review();">SENDEN</a>-->
                                                                                                                                    <button onclick="submit_review();" class="product_form_update_email_btn" id="review_submit_btn">SENDEN</button>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="popup_heading default_width text-center theme_bg"> 
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            </body>
                                                                                                            </html>
                                                                                                            <script type="text/javascript">
                                                                                                                $("#btn-video-play").click(function () {
                                                                                                                    $('#youtube_modal .modal-body').html('<iframe width="600" height="400" src="https://www.youtube.com/embed/Ll9l0xKkfgc" frameborder="0" allowfullscreen></iframe>');
                                                                                                                });
                                                                                                                $('#youtube_modal').on('hidden.bs.modal', function () {
                                                                                                                    $('#youtube_modal .modal-body').html('');
                                                                                                                });
                                                                                                            </script>

