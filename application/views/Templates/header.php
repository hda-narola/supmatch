<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo base_url(); ?>">
<link href="assets/images/favicon.png" rel='shortcut icon' type='image/x-icon'>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="assets/admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="assets/admin/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="assets/admin/css/core.css" rel="stylesheet" type="text/css">
<link href="assets/admin/css/components.css" rel="stylesheet" type="text/css">
<link href="assets/admin/css/colors.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="assets/admin/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="assets/admin/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="assets/admin/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="assets/admin/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/visualization/d3/d3_tooltip.js"></script>

<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/switch.min.js"></script>

<script type="text/javascript" src="assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="assets/admin/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/selects/select2.min.js"></script>

<!--<script type="text/javascript" src="assets/admin/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/pickers/daterangepicker.js"></script>-->
<script type="text/javascript" src="assets/admin/js/plugins/forms/validation/validate.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/switchery.min.js"></script>	
<script type="text/javascript" src="assets/admin/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/datatables_advanced.js"></script>
<script type="text/javascript" src="assets/admin/js/core/app.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/form_layouts.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/notifications/sweet_alert.min.js"></script>
<script type="text/javascript" src="assets/admin/js/custom_narola.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/forms/inputs/touchspin.min.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/form_validation.js"></script>
<script type="text/javascript" src="assets/admin/js/plugins/pickers/color/spectrum.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/picker_color.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/form_select2.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/admin/js/pages/form_multiselect.js"></script>
<!--<script type="text/javascript" src="assets/admin/js/pages/dashboard.js"></script>--> 

