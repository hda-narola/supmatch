<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('Templates/header'); ?>
        <script type="text/javascript">
            //-- Set common javascript vairable
            var site_url = "<?php echo site_url() ?>";
            var base_url = "<?php echo base_url() ?>";

            $(document).ready(function () {
                //--Hide the alert message 
                window.setTimeout(function () {
                    $(".hide-msg").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 7000);
            });
        </script>
    </head>
    <body>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo site_url('admin/home'); ?>" style="padding: 10px 20px">
                    <!--<img src="assets/images/logo-dark.png"/>-->
                    <img src="assets/images/logo.png" alt="" style="height: 30px;">
                    <!--<img src="assets/admin/images/logo-light.png">-->
                </a>
                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/admin/images/placeholder.jpg" alt="<?php echo $this->session->userdata('supmatch_admin')['nickname'] ?>">
                            <span><?php echo $this->session->userdata('supmatch_admin')['nickname'] ?></span>
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="" target="_blank"><i class="icon-cog5"></i> Visit Site</a></li>
                            <li><a href="<?php echo site_url('admin/account_profile') ?>"><i class="icon-cog5"></i> Manage Profile</a></li>
                            <li><a href="<?php echo site_url('admin/change_password') ?>"><i class="icon-key"></i> Change password</a></li>
                            <li><a href="<?php echo site_url('admin/logout') ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a class="media-left"><img src="assets/admin/images/placeholder.jpg" class="img-circle img-sm" alt="<?php echo $this->session->userdata('supmatch_admin')['nickname'] ?>"></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Supmatch</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-user text-size-small"></i> &nbsp;Admin
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->

                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <?php
                                    $controller = $this->router->fetch_class();
                                    $action = $this->router->fetch_method();
                                    ?>
                                    <li class="<?php echo ($controller == 'home' && $action == 'index') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                                    <li class="<?php echo ($controller == 'users') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/users'); ?>"><i class="icon-users4"></i> <span>Manage Users</span></a></li>
                                    <li class="<?php echo ($controller == 'liquids') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/liquids'); ?>"><i class="icon-droplet"></i> <span>Manage Liquids</span></a></li>
                                    <li class="<?php echo ($controller == 'flavours') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/flavours'); ?>"><img src="assets/admin/images/fruits.png" class="left_menu_img"> <span style="padding-left: 10px">Manage Flavours</span></a></li>
                                    <li class="<?php echo ($controller == 'categories') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/categories'); ?>"><i class="icon-tree6"></i> <span>Manage Categories</span></a></li>
                                    <li class="<?php echo ($controller == 'ranks') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/ranks'); ?>"><i class="icon-star-full2"></i> <span>Manage Ranks</span></a></li>
                                    <li class="<?php echo ($controller == 'supplements') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/supplements'); ?>"><img src="assets/admin/images/protein-shake-bottle-container.png" class="left_menu_img"> <span style="padding-left: 10px">Manage Supplements</span></a></li>
                                    <li class="<?php echo ($controller == 'properties') ? 'active' : ''; ?>">
                                        <a href="#" class="has-ul"><i class="icon-list2"></i> <span>Manage Properties</span></a>
                                        <ul class="hidden-ul" style="<?php echo ($controller == 'properties') ? 'display: block;' : ''; ?>">
                                            <li class="<?php echo (($controller == 'properties' && $action == 'index') || ($controller == 'properties' && $action == 'edit')) ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/properties'); ?>">Primary Properties</a></li>
                                            <li class="<?php echo (($controller == 'properties' && $action == 'secondary') || ($controller == 'properties' && $action == 'secondary_edit')) ? 'active' : ''; ?>" ><a href="<?php echo site_url('admin/secondary_properties'); ?>">Secondary Properties</a></li>
                                        </ul>
                                    </li>
                                    <li class="<?php echo ($controller == 'goals') ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/goals'); ?>"><img src="assets/admin/images/crosshair.png" class="left_menu_img"> <span style="padding-left: 10px">Manage Goals</span></a></li>
                                    <li><a href="<?php echo site_url('admin/logout'); ?>"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->
                    </div>
                </div>
                <!-- /main sidebar -->
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Page header -->
                    <?php echo $body; ?>
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
</html>
