<div class="main_content" style="background-image: url(assets/images/thank-you-bg.jpg);">
    <section class="about_us_padding">
        <div class="container">
            <div class="title_wrap mb30"><h1 class="title">Dein Merkzettel</h1></div>
            <div class="gt_product_list_wrap" id="gt_product_list_wrap">
                <?php
                if ($cart_items) {
                    foreach ($cart_items as $item) {
                        ?>
                        <div class="gt_product_list mb30" id="cart_<?php echo $item['id'] ?>">
                            <div class="gt_product_img">
                                <?php $item_image = explode(",", $item['images']) ?>
                                <a href="<?php echo base_url(); ?>supplements/view_supplement/<?php echo $item['supplement_id']; ?>"><img src="<?php echo SUPPLEMENTS_IMG . $item_image[0] ?>" alt="<?php echo $item['name_gr'] ?>"></a>
                            </div>
                            <div class="gt_product_price">
                                <h5 class="theme_color mb10">Produktname</h5>
                                <h4 class="white"><a class="white" href="<?php echo base_url(); ?>supplements/view_supplement/<?php echo $item['supplement_id']; ?>"><?php echo $item['name_gr'] ?></a></h4>
                            </div>
                            <div class="gt_product_price">
                                <h5 class="theme_color mb10">Preis:</h5>
                                <h4 class="white"><?php echo number_format($item['price'],2,',',''); ?>€</h4>
                            </div>
                            <!--                            <div class="gt_product_price">
                                                            <h5 class="theme_color mb10">Quantity</h5>
                                                            <h4 class="white item_qty"><?php echo $item['qty'] ?></h4>
                                                        </div>-->
                            <div class="gt_product_cart_btn">
                                <a href="<?php echo $item['amazon_link'] ?>" class="open_amazon_product" onclick="place_orders(this)" data-id="<?php echo $item['supplement_id']; ?>" cart-id="<?php echo $item['id']; ?>" data-qty="<?php echo $item['qty']; ?>" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="removeProduct(<?php echo $item['id'] ?>)">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                    }
                } else {
                    echo '<div class="gt_product_list mb30"><center>Dein Merkzettel ist leer!</center></div>';
                }
                ?>
            </div>
            <?php if ($cart_items) { ?>
<!--                <div class="read_more default_width">
                    <a id="place_order_btn" onclick="return add_to_order();" style="cursor: pointer;">ALLE KAUFEN!</a>
                </div>-->
            <?php } ?>
        </div>
    </section>
</div>
<script type="text/javascript">
    /**
     * Remove product from cart
     * @param {int} cart_id
     * @author KU
     */
    function removeProduct(cart_id) {
        $.ajax({
            url: site_url + "cart/remove_cart_item",
            dataType: "json",
            type: "POST",
            data: {cart_id: cart_id},
            success: function (response) {
                $('.cartitem_count').html(response);
                $("#cart_" + cart_id).hide('slow', function () {
                    $("#cart_" + cart_id).remove();
                });
                $('html, body').animate({scrollTop: 0}, 500);
                /*
                 $('#message_container').html('<div class="alert alert-box success"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><span class="text-semibold">Dein Produkt wurde erfolgreich vom Merkzettel entfernt.</span></div>');
                 //-- Hide Success Message
                 $(".alert-box").fadeTo(2000, 500).slideUp(500, function () {
                 $(".alert-box").slideUp(500);
                 }); */
                $('#success_alert_message').html('Dein Produkt wurde erfolgreich vom Merkzettel entfernt.');
                $('#success_alert_modal').modal();
                setTimeout(function () {
                    $('#success_alert_modal').modal('hide');
                }, 3000);

            }
        });
    }
    /**
     * @author SG
     */
    function add_to_order() {
        $($('#place_order_btn')).data('clicked', true);
        var cart_items = $('.open_amazon_product').length;
        var order_count = 0;
//    alert(cart_items); return false;
        $('.open_amazon_product').each(function () {
            $(this).click();
            window.open($(this).attr("href"), "_blank");
            order_count++;
//            var did = $(this).attr('data-id');
//            removeProducts(did);
        });
//        if(order_count == cart_items){
        window.location.href = site_url + 'order/thank_you';
//        }
    }


    /**
     * @author SG
     */
    function place_orders(data) {
        var sup_id = $(data).data('id');
        var cart_id = $(data).attr('cart-id');
        var quantity = $(data).attr('data-qty');
        $.ajax({
            url: site_url + 'order/place_order',
//            dataType: "json",
            type: "POST",
            async: false,
            data: {sup_id: sup_id, qty: quantity},
            success: function (response) {
                if (response == 'success') {
                    removeProducts(cart_id);
                }
            }
        });
        if ($('#place_order_btn').data('clicked')) {

        } else {
            window.location.href = site_url + 'order/thank_you';
        }
    }

    /**
     * @author SG
     */
    function removeProducts(cart_id) {
        $.ajax({
            url: site_url + "cart/remove_cart_item",
            dataType: "json",
            type: "POST",
            data: {cart_id: cart_id},
            success: function (response) {
                $('.cartitem_count').html(response);
                $("#cart_" + cart_id).hide('slow', function () {
                    $("#cart_" + cart_id).remove();
                });
                $('html, body').animate({scrollTop: 0}, 500);
                /*
                 $('#message_container').html('<div class="alert alert-box success"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><span class="text-semibold">Dein Produkt wurde erfolgreich vom Merkzettel entfernt.</span></div>');
                 //-- Hide Success Message
                 $(".alert-box").fadeTo(2000, 500).slideUp(500, function () {
                 $(".alert-box").slideUp(500);
                 });*/
                $('#success_alert_message').html('Dein Produkt wurde erfolgreich vom Merkzettel entfernt.');
                $('#success_alert_modal').modal();
                setTimeout(function () {
                    $('#success_alert_modal').modal('hide');
                }, 3000);
            }
        });
    }
</script>