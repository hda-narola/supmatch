<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" data-mobile="true" dir="ltr" data-width="600" style="font-size: 16px; background-color: rgb(214, 214, 214);">
    <tbody>
        <tr>
            <td align="center" valign="top" style="margin:0;padding:0;">
                <div class="WRAPPER" style="max-width: 600px; margin: auto;">
                    <table align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="600" class="wrapper" style="width: 600px;">
                        <tbody>
                            <tr>
                                <td align="center" valign="top" bgcolor="#f5f5f5" style="margin:0;padding:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-width="0" width="269">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="left" style="display: inline-block; padding: 0px; margin: 0px;" class="tdBlock"><img src="https://multimedia.getresponse.com/901/35027901/photos/506363101.png?img1488229510455" width="269" data-src="https://multimedia.getresponse.com/901/35027901/photos/506363101.png|480|205|482|482|-125|0|1.7843866171003717" height="115" data-origsrc="https://multimedia.getresponse.com/901/35027901/photos/506362901.png?_ga=1.198225135.1783472169.1485291698" style="border-width: 0px; border-style: none; border-color: transparent; display: block;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 28px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><div style="text-align: center;"><span style="color: rgb(38, 38, 38); font-weight: bold; font-family: Helvetica, Helvetica Neue, Arial, sans-serif;"><br></span></div><div style="text-align: center;"><span style="color: rgb(38, 38, 38); font-weight: bold; font-family: Helvetica, Helvetica Neue, Arial, sans-serif;">Passwort vergessen?</span></div><span style="font-family: Arial, Helvetica, sans-serif; color: rgb(38, 38, 38);"></span></td></tr></tbody></table></td></tr><tr>
                                <td valign="top" align="left" style="padding:0;margin:0;">
                                    <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" data-editable="text">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="left" class="lh-4" style="padding: 29px 40px 26px; margin: 0px; line-height: 1.45; font-size: 16px; font-family: Times New Roman, Times, serif; border-width: 0px; border-style: none; border-color: transparent;"><div style="text-align: center;"><span style="font-family: Helvetica, Helvetica Neue, Arial, sans-serif; background-color: transparent;">Hi <?php echo $nickname ?>,&nbsp;</span></div><div style="text-align: center;"><span style="background-color: transparent; font-family: Helvetica, Helvetica Neue, Arial, sans-serif;">hier ist dein neues Passwort:</span></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr><tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15; border-width: 2px; border-style: solid; border-radius: 22px; background-color: rgb(214, 214, 214); border-color: rgb(60, 60, 60);"><div style="text-align: center;"><?php echo $password ?><br></div><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:14px"></span></td></tr></tbody></table></td></tr><tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><br></td></tr></tbody></table></td></tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 0px; margin: 0px;">
                                    <div data-box="button" style="width: 100%; margin-top: 0px; margin-bottom: 0px; text-align: center;">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center" data-editable="button" style="margin: 0px auto;">
                                            <tbody>
                                                <tr>
                                                    <td valign="middle" align="center" class="tdBlock" style="display: inline-block; padding: 9px; margin: 0px; border-radius: 4px; background-color: rgb(246, 146, 30); border-width: 1px; border-style: solid; border-color: rgb(209, 130, 38); width: 241px;">
                                                        <a href="<?php echo $link ?>" style="font-family: Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); font-size: 26px; text-decoration: none; font-weight: bold;">
                                                            Jetzt Einloggen!
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding:0;margin:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line" style="margin: 0px; padding: 0px;">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="center" style="padding: 30px 0px 20px; margin: 0px;">
                                                    <div style="height:1px;line-height:1px;border-top-width:1px; border-top-style:solid;border-top-color:#ffffff;">
                                                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;">
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15; background-color: rgb(245, 245, 245);"><span style="font-family: Helvetica, Helvetica Neue, Arial, sans-serif;"><font size="14" style="font-size: 14px;"><br><span style="color: rgb(38, 38, 38);"></span></font></span><div style="text-align: center;"><span style="font-family: Helvetica, Helvetica Neue, Arial, sans-serif;"><font size="14" style="font-size: 14px;">SupMatch UG (haftungsbeschränkt)</font></span></div><div style="text-align: center;"><span style="font-family: Helvetica, Helvetica Neue, Arial, sans-serif;"><font size="14" style="font-size: 14px;">An der Gatower Heide 96, 14089 Berlin</font></span></div><div style="text-align: center;"><span style="font-family: Helvetica, Helvetica Neue, Arial, sans-serif;"><font size="14" style="font-size: 14px;"><br></font></span></div></td></tr></tbody></table></td></tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>
