<div class="main_content default_bg" style="background-image: url(assets/images/about-us-bg.jpg);">
    <section class="about_us_bg about_us_overlay">
        <div class="container">
            <div class="title_wrap mb30"><h1 class="title">SupMatch - General User's Information</h1></div>
            <div class="service_wrap">
            	<div class="form-horizontal">
            		<form action="setting" id="change_email_form" method="post">
	            		<div class="row">
		                    <div class="col-md-4">
		                        <div class="popup_field_label default_width mb15">
		                            <span><h5 style="color:#fff">Aktuelles Passwort : </h5></span>
		                        </div>
		                    </div>
		                    <div class="col-md-8">
		                        <div class="popup_field default_width mb15">
		                            <input type="password" name="txt_current_password_email" id="txt_current_password_email" class="form-control <?php if(form_error('txt_current_password_email')!=''){ echo 'form_error_text'; } ?>" placeholder="Gib dein aktuelles Passwort ein">
		                        	<font id="txt_current_password_email_error" class="form_error" for="txt_current_password_email"><?= form_error('txt_current_password_email') ?></font>
		                        </div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col-md-4">
		                        <div class="popup_field_label default_width mb15">
		                            <span><h5 style="color:#fff">Benutzername :</h5></span>
		                        </div>
		                    </div>
		                    <div class="col-md-8">
		                        <div class="popup_field default_width mb15">
		                            <input type="text" name="txt_user_name" id="txt_user_name" class="form-control" value="<?= $this->session->userdata('session_user_name') ?>" disabled>
		                        </div>
		                    </div>
		                </div>
		                <div class="row">
	                        <div class="col-md-4">
	                            <div class="popup_field_label default_width mb15">
	                                <span><h5 style="color:#fff">E-Mail :</h5></span>
	                            </div>
	                        </div>
	                        <div class="col-md-8">
	                            <div class="popup_field default_width mb15">
	                                <input type="text" name="txt_new_email" id="txt_new_email" class="form-control <?php if(form_error('txt_new_email')!=''){ echo 'form_error_text'; } ?>" placeholder="Geben Sie Ihre neue E-Mail ein">
	                            	<font id="txt_new_email_error" class="form_error" for="txt_new_email"><?= form_error('txt_new_email') ?></font>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-4"></div>
	                        <div class="col-md-8">
			                    <div class="product_form default_width text-left mb20">
			                    	<button type="submit" name="btn_email_change" id="btn_email_change" class="btn_register">E-Mail ändern</button>
			                    </div>
			                </div>
			            </div><br>
			        </form>

			        <form action="setting/change_password" id="change_password_form" method="post">
			            <div class="row">
	                        <div class="col-md-4">
	                            <div class="popup_field_label default_width mb15">
	                                <span><h5 style="color:#fff">Aktuelles Passwort : </h5></span>
	                            </div>
	                        </div>
	                        <div class="col-md-8">
	                            <div class="popup_field default_width mb15">
	                                <input type="password" name="txt_current_password_pass" id="txt_current_password_pass" class="form-control <?php if(form_error('txt_current_password_pass')!=''){ echo 'form_error_text'; } ?>" value="<?php echo set_value('txt_current_password_pass'); ?>">
	                                <font id="txt_current_password_pass_error" class="form_error" for="txt_current_password_pass"><?= form_error('txt_current_password_pass') ?></font>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="col-md-4">
	                            <div class="popup_field_label default_width mb15">
	                                <span><h5 style="color:#fff">Neues Passwort : </h5></span>
	                            </div>
	                        </div>
	                        <div class="col-md-8">
	                            <div class="popup_field default_width mb15">
	                                <input type="password" name="txt_new_password" id="txt_new_password" class="form-control <?php if(form_error('txt_new_password')!=''){ echo 'form_error_text'; } ?>" value="<?php echo set_value('txt_new_password'); ?>">
	                                <font id="txt_new_password_error" class="form_error" for="txt_new_password"><?= form_error('txt_new_password') ?></font>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="col-md-4">
	                            <div class="popup_field_label default_width mb15">
	                                <span><h5 style="color:#fff">Neues Passwort wiederholen : </h5></span>
	                            </div>
	                        </div>
	                        <div class="col-md-8">
	                            <div class="popup_field default_width mb15">
	                                <input type="password" name="txt_new_re_password" id="txt_new_re_password" class="form-control <?php if(form_error('txt_new_re_password')!=''){ echo 'form_error_text'; } ?>" value="<?php echo set_value('txt_new_re_password'); ?>">
	                                <font id="txt_new_re_password_error" class="form_error" for="txt_new_re_password"><?= form_error('txt_new_re_password') ?></font>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row">
	                        <div class="col-md-4"></div>
	                        <div class="col-md-8">
			                	<div class="product_form default_width text-left mb20">
			                		<button type="submit" name="btn_password_change" id="btn_password_change" class="btn_register">Passwort ändern</button>
			                    </div>
			                </div>
			            </div>
			        </form>
            	</div>
            </div>
            <!-- <div class="service_wrap service_border">
                <div class="row">
                    <div class="col-md-3 visible-sm visible-xs">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon2.jpg" alt="">
                        </div>
                    </div>
                    
                    <div class="col-md-9">
                        <div class="services_des default_width padding_right2">
                            <h3 class="theme_color mb15">Unser Ziel</h3>
                            <p class="white">Unser ZielWir haben eine ganz klare Vision vor Augen:
                                <br>
                            Der Nutzen für unsere Kunden soll so enorm werden, dass jeder Sportler und Fitnessbegeisterte, ob Anfänger oder Experte, sein bestes Supplement bei uns findet. Nie wieder soll es zu falschen Kaufentscheidungen kommen.
                        </p>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="service_wrap">
                <div class="row">
                    <div class="col-md-3">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon3.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="services_des default_width padding_left padding_right3">
                            <h3 class="theme_color mb15">Warum wir?</h3>
                            <p class="white">Wir sind KEIN Supplementhersteller und verkaufen NICHT unsere eigenen Produkte. Wir arbeiten NICHT mit Herstellern zusammen und stehen NICHT für eine einzige Marke, die wir dir empfehlen! Wir sind eine unabhängige Dienstleistung und stehen für den Kunden und sein Wunsch, das optimale Supplement anhand angegebener Präferenzen zu finden. Das bedeutet, dass einzig und allein die Angaben die unser System von dir erhält, darüber entscheiden, solange das Produkt zu deinen Präferenzen optimal passt.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
</div>
<style>
	.form_error p{
		color:red;
	}
	.form_error_text{border:2px solid red !important;}
</style>
