<div class="main_content default_bg list-bg" style="background-image: url(assets/images/supmatch-productpage.jpg);">
    <section class="gt_s_product_bg_section">
        <div class="container">
            <div class="title_wrap mb30"><h1 class="title">DIESE SUPPLEMENTS PASSEN AM BESTEN ZU DIR <a href="<?php echo site_url('home') ?>" class="back-btn" id="back_btn_search">Zurück</a></h1></div>
        </div>
        <?php
        $sr_no = 1;
        foreach ($suppArr as $supp) {
            $img = $supp['images'] != '' ? explode(",", $supp['images'])[0] : 'default.jpg';
            ?>
            <div class="gt_s_product_bg default_width <?php
            if ($sr_no == 1)
                echo 'active_product theme_bg';
            else
                echo 'mb20'
                ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="s_product_img default_width">
                                <span class="number theme_color"><?php echo $sr_no; ?></span>
                                <a href="supplements/view_supplement/<?php echo $supp['id']; ?>"><img src="<?php echo SUPPLEMENTS_IMG . $img; ?>" alt="" class="listing_product_image"></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="s_product_name default_width">
                                <div class="<?php
                                if ($sr_no == 1)
                                    echo 's_title black mb10';
                                else
                                    echo 's_title theme_color mb10'
                                    ?>"><?= $supp['name_gr'] ?></div>
                                <div class="link_list_wrap default_width">
                                    <ul>
                                        <?php
                                        foreach ($supp['sec_properties'] as $sec_property) {
                                            echo "<li><a href='javascript:void(0);'>" . $sec_property['name_gr'] . "</a></li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="flex_box">
                                    <div class="inner">
                                        <div class="s_progress default_width mb10">
                                            <span class="white"><?php echo $price_property_details['name_gr'] ?></span>
                                            <div class="progress_wrapper">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $supp['price_property']['price_rating'] * 10 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $supp['price_property']['price_rating'] * 10 ?>%"></div>
                                                </div>
                                            </div>
                                            <?php
                                            $price_rating = floor($supp['price_property']['price_rating'] * 10) / 10;
                                            ?>
                                            <p class="no_margin white"><?php echo str_replace(".", ",", $price_rating) ?></p>
                                        </div>
                                        <?php
                                        foreach ($supp['properties'] as $property) {
                                            $percent = ($property['average_rating'] / 10) * 100;
                                            ?>
                                            <div class="s_progress default_width mb10">
                                                <span class="white"><?php echo $property['name_gr'] ?></span>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percent ?>%"></div>
                                                </div>
                                                <?php
                                                $average_rating = floor($property['average_rating'] * 10) / 10;
                                                ?>
                                                <p class="no_margin white"><?php echo str_replace(".", ",", $average_rating) ?></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="s_price_wrap default_width">
                                <div class="s_title white mb10">Preis:</div>
                                <div class="s_product_link default_width">
                                    <span class="theme_color mb30"><?php echo number_format($supp['price'],2,',',''); ?>€</span>
                                    <a class="mb15" href="javascript:void(0)" onclick="add_to_cart(<?php echo $supp['id'] ?>);">Auf den Merkzettel</a>
                                    <a href="<?php echo $supp['amazon_link'] ?>" class="open_amazon_product" onclick="place_order(this)" data-id="<?php echo $supp['id']; ?>" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $sr_no++;
        }
        ?>

        <?php for ($i = $sr_no; $i <= 3; $i++) { ?>
            <div class="gt_s_product_bg default_width placeholder">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="s_product_img placeholder_img default_width">
                                <img src="assets/images/default.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="s_product_name  default_width">
                                <div class="s_title theme_color mb10">PRODUCT NAME</div>
                                <div class="link_list_wrap default_width mb15">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);">Vegan</a>
                                            <a href="javascript:void(0);">Laktosefrei</a>
                                            <a href="javascript:void(0);">Glutenfrei</a>
                                            <a href="javascript:void(0);">Kölner Liste</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="s_progress default_width mb10">
                                    <span class="white">Property#01</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <p class="no_margin white">10</p>
                                </div>
                                <div class="s_progress default_width mb10">
                                    <span class="white">Property#01</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <p class="no_margin white">10</p>
                                </div>
                                <div class="s_progress default_width mb10">
                                    <span class="white">Property#01</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <p class="no_margin white">10</p>
                                </div>
                                <div class="s_progress default_width mb10">
                                    <span class="white">Property#01</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <p class="no_margin white">10</p>
                                </div>
                                <div class="s_progress default_width mb10">
                                    <span class="white">Property#01</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    </div>
                                    <p class="no_margin white">10</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="s_price_wrap default_width">
                                <div class="s_title white mb10">Preis</div>
                                <div class="s_product_link default_width">
                                    <!--<span class="theme_color mb30">Price</span>-->
                                    <span class="theme_color mb30"></span>
                                    <a class="mb15" href="#">Auf den Merkzettel</a>
                                    <a href="#">Jetzt kaufen!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
</div>
