<style>
    .heart_rating {color: #ee8b2d;}
    #hearts-existing { color: #ee8b2d;}
</style>
<!-- Bootstrap core CSS -->
<!--<link href="assets/css/bootstrap.min.css" rel="stylesheet">--> 
<!-- Font Awesome StyleSheet CSS -->
<!--<link href="assets/css/font-awesome.min.css" rel="stylesheet">-->
<!-- Typography CSS -->
<!--<link href="assets/css/typography.css" rel="stylesheet">-->
<!--<link href="assets/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />-->
<?php
$like = 'like';
$dislike = 'dislike';
?>
<div class="main_content" style="background-image: url(assets/images/supmatch-productpage.jpg)">
    <section class="gt_s_product_bg_section">
        <div class="gt_s_product_bg default_width mb20">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="s_title theme_color mb10 visible-xs"><?php echo $supplement['name_gr'] ?></div>
                        <?php
                        $images = $supplement['images'];
                        $images = explode(',', $images);
                        $images = array_slice($images, 0, 4);
                        $sub_images = array_slice($images, 1);
                        ?>
                        <div>
                            <div class="s_product_img default_width">
                                <a class="fancybox" href="<?php echo SUPPLEMENTS_IMG . $images[0] ?>" data-fancybox-group="gallery" title="<?php echo $supplement['name_gr'] ?>"><img src="<?php echo SUPPLEMENTS_IMG . $images[0] ?>" alt="<?php echo $supplement['name_gr'] ?>"></a>
                            </div>
                        </div>
                        <div class="default_width small_product mt">
                            <?php foreach ($sub_images as $image) { ?>
                                <div class="small_product_img">
                                    <a class="fancybox" href="<?php echo SUPPLEMENTS_IMG . $image ?>" data-fancybox-group="gallery" title="<?php echo $supplement['name_gr'] ?>"><img src="<?php echo SUPPLEMENTS_IMG . $image ?>" alt=""></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="product_between default_width">
                            <div class="review_scroll">
                                <form id="review_rating_form" method="post" action="<?php echo site_url('supplements/post_review') ?>" > 
                                    <input type="hidden" name="category_id" id="category_id" value="<?php echo $supplement['category_ids'] ?>"/>
                                    <input type="hidden" name="supplement_id" id="supplement_id" value="<?php echo $supplement['id'] ?>"/>
                                    <!--<div class="title_wrap mb30">-->
                                    <div class="title_wrap mb15">
                                        <h3 class="title">Produkt bewerten</h3>
                                    </div>
                                    <?php
                                    foreach ($properties as $property) {
                                        ?>
                                        <div class="rating_star">
                                            <div class="rating_wrap">
                                                <p class="title"><?php echo $property['name_gr'] ?></p>
                                            </div>
                                            <!--<div class="rating">
                                                    <span>☆</span><span>☆</span><span>☆</span><span class="ml3">☆</span> <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                                </div>-->
                                            <div class="starrr heart_rating" data-id="hearts_<?php echo $property['id'] ?>"></div>
                                            <input type="hidden" name="hearts_<?php echo $property['id'] ?>" id="hearts_<?php echo $property['id'] ?>" value="0"/>
                                            <div class="error_color" id="hearts_<?php echo $property['id'] ?>_error"></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($flavours) { ?>
                                        <div class="default_width gt_between_drop">
                                            <div class="rating_wrap">
                                                <p class=" theme_color">Geschmackssorte:</p>
                                            </div>
                                            <div class="gt_between_flavor">
                                                <select name="review_flavour" id="review_flavour">
                                                    <option value="">Wähle...</option>
                                                    <?php
                                                    foreach ($flavours as $flavour) {
                                                        echo "<option value='" . $flavour['id'] . "'>" . $flavour['name_gr'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <div class="error_color" id="review_flavour_error"></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($liquids) { ?>
                                        <div class="default_width gt_between_drop ">
                                            <div class="rating_wrap">
                                                <p class=" theme_color">Gemischt mit:</p>
                                            </div>
                                            <div class="gt_between_flavor">
                                                <select name="review_liquid" id="review_liquid">
                                                    <option value="">Wähle...</option>
                                                    <?php
                                                    foreach ($liquids as $liquid) {
                                                        echo "<option value='" . $liquid['id'] . "'>" . $liquid['name_gr'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <div class="error_color" id="review_liquid_error"></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    $disabled = 'disabled';
                                    $class = 'disabled_cursor';
                                    $disabled_div = 'disable_section';
                                    $tooltip = 'data-toggle="tooltip" data-placement="right" title="Bitte melde dich an, um zu deiner Bewertung ein Kommentar hinzuzufügen."';
                                    $placeholder = 'Bitte melde dich an, um zu deiner Bewertung ein Kommentar hinzuzufügen.';
                                    //-- If user logged in then remove disable from textarea
                                    if ($user_session_id != '') {
                                        $disabled = $class = $disabled_div = '';
                                        $tooltip = '';
                                        $placeholder = 'Dein Kommentar...';
                                    }
                                    ?>
                                    <div class="default_width gt_between_textarea <?php echo $disabled_div ?>">
                                        <textarea class="white <?php echo $class ?>" placeholder="<?php echo $placeholder ?>" name="review_comment" id="review_comment" <?php echo $disabled ?>></textarea>
                                    </div>
                                    <div class="default_width gt_between_textarea ">
                                        <!--<a href="#" data-toggle="modal" data-target="#review">Produkt bewerten</a>-->
                                        <a id="product_review_btn" href="javascript:void(0)" onclick="validateReview()">Produkt bewerten</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="row"><!-- row remove-->
                            <div class="col-md-8 col-sm-8">
                                <div class="s_product_name default_width">
                                    <div class="s_title theme_color mb10 hidden-xs"><?php echo $supplement['name_gr'] ?></div>
                                    <div class="link_list_wrap default_width mb15">
                                        <ul>
                                            <?php
                                            foreach ($sec_properties as $sec_property) {
                                                echo "<li><a href='javascript:void(0);'>" . $sec_property['name_gr'] . "</a></li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="flex_box">
                                        <div class="inner">
                                            <div class="s_progress default_width mb10">
                                                <span class="white"><?php echo $price_property_details['name_gr'] ?></span>
                                                <div class="progress_wrapper">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $price_property['price_rating'] * 10 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $price_property['price_rating'] * 10 ?>%"></div>
                                                    </div>
                                                </div>
                                                <?php
                                                $price_rating = floor($price_property['price_rating'] * 10) / 10;
                                                ?>
                                                <p class="no_margin white"><?php echo str_replace(".", ",", $price_rating) ?></p>
                                            </div>
                                            <?php
                                            foreach ($properties as $property) {
                                                $percent = ($property['average_rating'] / 10) * 100;
                                                ?>
                                                <div class="s_progress default_width mb10">
                                                    <span class="white"><?php echo $property['name_gr'] ?></span>
                                                    <div class="progress_wrapper">
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percent ?>%"></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $average_rating = floor($property['average_rating'] * 10) / 10;
                                                    ?>
                                                    <p class="no_margin white"><?php echo str_replace(".", ",", $average_rating) ?></p>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s_price_wrap default_width">
                                    <div class="s_title white mb10">Preis:</div>
                                    <div class="s_product_link default_width">
                                        <span class="theme_color mb30 "><?php echo number_format($supplement['price'],2,',',''); ?>€</span>
                                        <a class="mb15" href="javascript:void(0)" onclick="add_to_cart(<?php echo $supplement['id'] ?>);">Auf den Merkzettel</a>
                                        <a href="<?php echo $supplement['amazon_link'] ?>" target="_blank" class="open_amazon_product" onclick="place_order(this)" data-id="<?php echo $supplement['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gt_x_product_wrap default_width">
                            <div class="gt_x_product_tab default_width">
                                <ul data-tabs="tabs">
                                    <li class="active"><a data-toggle="tab" href="#product_tab1">Beschreibung</a></li>
                                    <li><a data-toggle="tab" href="#product_tab2">Bewertungen</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="product_tab1">
                                    <div class="gt_x_product_scroll" >
                                        <div class="gt_x_product_list default_width">
                                            <div class="tab_product_buyer_wrap mt15">
                                                <div class="buyer_product_des ">
                                                    <div class="buyer_des default_width fixedHeight mb20">
                                                        <p class="white"><?php echo $supplement['desc_gr'] ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="product_tab2">
                                    <div class="gt_x_product_scroll product_reviews" id="gt_product_list_wrap2">
                                        <?php foreach ($reviewsArr as $review) { ?>
                                            <div class="gt_x_product_list default_width">
                                                <div class="tab_product_buyer_wrap mb25">
                                                    <div class="buyer_product_des">
                                                        <div class="buyer_name mb15">
                                                            <?php
                                                            if ($review['color'] != '') {
                                                                $reviewuser_name_style = 'class="fbold" style="color:' . $review['color'] . '"';
                                                            } else {
                                                                $reviewuser_name_style = 'class="theme_color fbold"';
                                                            }
                                                            ?>
                                                            <?php if ($review['rank_icon'] != '') { ?>
                                                                <img src="<?php echo RANKS_IMG . $review['rank_icon'] ?>" alt="" class="user_rank_img">
                                                            <?php } else { ?>
                                                                <img src="assets/images/buyer_img.png" alt="">
                                                            <?php } ?>
                                                            <p class="no_margin white">
                                                                <span <?php echo $reviewuser_name_style ?>>
                                                                    <?php
                                                                    if ($review['username'] != '')
                                                                        echo $review['username'];
                                                                    else
                                                                        echo "Gast";
                                                                    ?> 
                                                                </span> - <?php echo date('F d , Y', strtotime($review['review_date'])); ?>.
                                                            </p>
                                                        </div>
                                                        <div class="product_gradiant mb15">
                                                            <ul>
                                                                <?php if ($review['flavour'] != '') { ?>
                                                                    <li>
                                                                        <span class="theme_color">Geschmack:</span>
                                                                        <?php echo $review['flavour']; ?>
                                                                    </li>
                                                                <?php } ?>
                                                                <?php if ($review['liquid'] != '') { ?>
                                                                    <li>
                                                                        <span class="theme_color">Gemischt mit:</span>
                                                                        <?php echo $review['liquid']; ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                        <div class="buyer_des default_width mb20">
                                                            <p class="white" style="word-wrap: break-word"><?php echo $review['comment']; ?></p>
                                                        </div>
                                                        <div class="product_rating">
                                                            <ul>
                                                                <?php
                                                                if ($review['property_rating']) {
                                                                    foreach ($review['property_rating'] as $k => $v) {
                                                                        $property_name = explode(':-:', $v)[0];
                                                                        $property_rating = explode(':-:', $v)[1];
                                                                        ?>
                                                                        <li>
                                                                            <span><?php echo $property_name ?></span>
                                                                            <i class="fa fa-star theme_color"></i>
                                                                            <p><?php echo $property_rating ?></p>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    echo "<li></li>";
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <div class="gt_product_like">
                                                            <?php
                                                            if ($review['sess_user_likes'] == 1) {
                                                                $like_img = 'assets/images/user_like.png';
                                                            } else {
                                                                $like_img = 'assets/images/like.png';
                                                            }
                                                            if ($review['sess_user_dislikes'] == 1) {
                                                                $dislike_img = 'assets/images/user_unlike.png';
                                                            } else {
                                                                $dislike_img = 'assets/images/unlike.png';
                                                            }
                                                            ?>
                                                            <ul>
                                                                <li>
                                                                    <?php
                                                                    if ($review['comment'] != '' && $user_session_id != '') {
                                                                        if ($review['user_id'] != $user_session_id) {
                                                                            ?>
                                                                            <img src="<?php echo $like_img; ?>" alt="Like" id="like_button_<?php echo $review['review_id']; ?>" onclick="supplement_likes('like',<?php echo $review['review_id']; ?>)" class="pointer_cursor">
                                                                        <?php } else { ?>
                                                                            <img src="assets/images/like.png" alt="Like" id="like_button_<?php echo $review['review_id']; ?>"  class="disabled_cursor">
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="assets/images/like.png" alt="Like" id="like_button_<?php echo $review['review_id']; ?>" class="disabled_cursor">
                                                                    <?php } ?>

                                                                    <p id="total_likes_<?php echo $review['review_id']; ?>"><?= $review['total_likes']; ?></p>
                                                                </li>
                                                                <li>
                                                                    <?php
                                                                    if ($review['comment'] != '' && $user_session_id != '') {
                                                                        if ($review['user_id'] != $user_session_id) {
                                                                            ?>
                                                                            <img src="<?php echo $dislike_img; ?>" alt="Dislike" id="dislike_button_<?php echo $review['review_id']; ?>" onclick="supplement_likes('dislike',<?php echo $review['review_id']; ?>)" class="pointer_cursor">
                                                                        <?php } else { ?>
                                                                            <img src="assets/images/unlike.png" alt="Dislike" id="dislike_button_<?php echo $review['review_id']; ?>" class="disabled_cursor">
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <img src="assets/images/unlike.png" alt="Dislike" id="dislike_button_<?php echo $review['review_id']; ?>" class="disabled_cursor">
                                                                    <?php } ?>
                                                                    <p id="total_dislikes_<?php echo $review['review_id']; ?>"><?= $review['total_dislikes']; ?></p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="s_price_wrap default_width visible-xs">
                            <div class="s_product_link default_width">
                                <a class="mb15" href="javascript:void(0)" onclick="add_to_cart(<?php echo $supplement['id'] ?>);">Auf den Merkzettel</a><br>
                                <a href="<?php echo $supplement['amazon_link'] ?>" target="_blank" class="open_amazon_product" onclick="place_order(this)" data-id="<?php echo $supplement['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                       
    </section>
</div>

<div id="fakeLoader"></div>
<script type="text/javascript">

    function validateReview() {
        is_user_posted_review = <?php echo $user_posted_review ?>;
        if (is_user_posted_review == 0) {
            valid = 1;
            $('.starrr').each(function () {
                var name_attr = $(this).attr('data-id');
                if ($('#' + name_attr).val() == 0) {
                    valid = 0;
                    $('#' + name_attr + '_error').html('Pflichtfeld');
                } else {
                    $('#' + name_attr + '_error').html('');
                }
            });
<?php if ($flavours) { ?>
                if ($('#review_flavour').val() == '') {
                    valid = 0;
                    $('#review_flavour_error').html('Pflichtfeld');
                } else {
                    $('#review_flavour_error').html('');
                }
<?php } ?>
<?php if ($liquids) { ?>
                if ($('#review_liquid').val() == '') {
                    valid = 0;
                    $('#review_liquid_error').html('Pflichtfeld');
                } else {
                    $('#review_liquid_error').html('');
                }
<?php } ?>
            if (valid == 1) {
                $('#review').modal();
            }
        } else {
            $('#review_alert_message').html('Du hast dieses Produkt bereits bewertet.');
            $('#login_check_modal').modal();
        }

    }
    function submit_review() {
        if ($('#review_confirmation_check').is(':checked')) {
            $('#review_rating_form').submit();
            $("#review_submit_btn").prop('disabled', true);
        } else {
            $('#review_confirmation_check').focus();
            $('#review_confirmation_check_error').html('Pflichtfeld');
        }
    }
</script>