<div class="main_wrapper default_bg" style="background-image: url(assets/images/thank-you-bg.jpg);">

    <div class="main_content">
        <section class="gt_thankyou_bg">
            <div class="container">
                <div class="thank_you_wrap">
                    <p class="text-center white" style="text-transform: inherit !important">
                        <?php
                            echo utf8_encode('Vielen Dank, dass du dein optimales Supplement').'<br>'.
                            utf8_encode('�ber SupMatch gesucht hast!').'<br>'.
                            utf8_encode('Wir wollen unsere Dienstleistung f�r dich stets verbessern').'<br>'.
                            utf8_encode('und w�rden uns �ber ein ehrliches Feedback freuen.');
                        ?>
                    </p>
                    <span class="theme_color text-center mb30">Was können wir besser machen?</span>

                    <form id="thank_you_form" method="POST">
                        <div class="text-center description mb30">
                            <textarea id="thank_you_content" name="thank_you_content" placeholder="Deine Nachricht…" class="white"></textarea>
                            <label id="thank_you_content_lbl" class="alert-danger" style="display: none;"></label>
                        </div>
                        <div class="submit_form text-center"><a id="send_thank_you" onclick="submit_thank_you();">SENDEN</a></div>
                    </form>
                </div>
            </div>
        </section>
    </div>

</div>