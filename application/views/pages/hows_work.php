<div class="main_content default_bg hows_work list-bg" style="background-image: url(assets/images/how-work-bg.jpg);">
    <section class="about_us_bg about_us_overlay">
        <div class="container">
            <div class="title_wrap mb30"><h1 class="title">SO FUNKTIONIERT'S</h1></div>
            <div class="hows_work_wrap default_bg">
                <img src="assets/images/how-work.png" alt="">
            </div>

            <!--How Work Item Start-->
            <div class="gt_how_work_resp default_width">
                <div class="gt_res_work default_width mb20">
                    <i class="work_number">1</i>
                    <span><img src="assets/images/work-icon-01.png" alt=""></span>
                    <h4 class="work_hdg white mb10">MATCHING STARTEN</h4>
                    <p class="white">Wähle zunächst die <b>Supplement-Kategorie</b> aus, nach der du suchen möchtest. Du brauchst Hilfe bei der Auswahl? Klicke auf <b>kostenlose Empfehlung</b>.</p>
                </div>

                <div class="gt_res_work default_width mb20">
                    <i class="work_number">2</i>
                    <span><img src="assets/images/work-icon-02.png" alt=""></span>
                    <h4 class="work_hdg white mb15">INDIVIDUALISIEREN</h4>
                    <p class="white">Nun kannst du auswählen, welche <b>Eigenschaften</b> für dich am wichtigsten sind: Soll dein Produkt lecker, günstig und dazu noch vegan sein? Du kannst genau das auswählen, was du möchtest.</p>
                </div>

                <div class="gt_res_work default_width mb20">
                    <i class="work_number">3</i>
                    <span><img src="assets/images/work-icon-03.png" alt=""></span>
                    <h4 class="work_hdg white mb15">DEINE BESTEN 3 SUPPLEMENTS</h4>
                    <p class="white">Mit einem Klick auf <b>“Dein bestes Produkt finden!”</b> sucht unser Matching-Algorithmus in der ständig wachsenden Datenbank, nach den drei besten Supplements für dich.</p>
                </div>

                <div class="gt_res_work default_width mb20">
                    <i class="work_number">4</i>
                    <span><img src="assets/images/work-icon-04.png" alt=""></span>
                    <h4 class="work_hdg white mb15">MATCHING</h4>
                    <p class="white">Unzählige Bewertungen helfen uns dabei, das perfekte Supplement für dich zu finden.	Um die <b>Qualität der Bewertungen</b> hochzuhalten, nutzen wir verschiedene Mechaniken, die beispielsweise verfälschte Bewertungen verhindern sollen.</p>
                </div>

                <div class="gt_res_work default_width mb20">
                    <i class="work_number">5</i>
                    <span><img src="assets/images/work-icon-05.png" alt=""></span>
                    <h4 class="work_hdg white mb15">SUPPLEMENT KAUFEN</h4>
                    <p class="white"><b>Das war’s auch schon!</b>

                        Du kannst nun noch mehr über dein Produkt erfahren, z.B. dessen Nährwerte, oder auf <b>“Jetzt kaufen!”</b> klicken, um auf die Amazon-Produktseite weitergeleitet zu werden.</p>
                </div>
            </div>
            <!--How Work Item End--> 

        </div>
    </section>
</div>

