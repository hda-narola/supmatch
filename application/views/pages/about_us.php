<div class="main_content default_bg list-bg" style="background-image: url(assets/images/about-us-bg.jpg);">
    <section class="about_us_bg about_us_overlay">
        <div class="container">
            <div class="title_wrap mb30"><h1 class="title">SupMatch - Für jeden das beste Supplement!</h1></div>
            <div class="service_wrap service_border">
                <div class="row">
                    <div class="col-md-3">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="services_des default_width padding_left padding_right">
                            <h3 class="theme_color mb15">Mission</h3>
                            <p class="white">Jede Person ist individuell und einzigartig! Genau deshalb hat auch jeder Mensch eigene Bedürfnisse und Vorstellungen, wie sein passendes Supplement sein sollte. SupMatch hat die Mission, dass jeder Kunde, immer sein bestes Supplement aus einer ständig wachsenden Datenbank, angepasst auf seine Bedürfnisse, erhält. Einfacher und schneller geht’s nicht!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service_wrap service_border">
                <div class="row">
                    <div class="col-md-3 visible-sm visible-xs">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon2.jpg" alt="">
                        </div>
                    </div>
                    
                    <div class="col-md-9">
                        <div class="services_des default_width padding_right2">
                            <h3 class="theme_color mb15">Unser Ziel</h3>
                            <p class="white">Wir haben eine ganz klare Vision vor Augen:
                                <br>
                            Der Nutzen für unsere Kunden soll so enorm werden, dass jeder Sportler und Fitnessbegeisterte, ob Anfänger oder Experte, sein bestes Supplement bei uns findet. Nie wieder soll es zu falschen Kaufentscheidungen kommen.
                        </p>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="service_wrap">
                <div class="row">
                    <div class="col-md-3">
                        <div class="service_icon default_width">
                            <img src="assets/images/mission-icon3.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="services_des default_width padding_left padding_right3">
                            <h3 class="theme_color mb15">Warum wir?</h3>
                            <p class="white">Wir sind KEIN Supplementhersteller und verkaufen NICHT unsere eigenen Produkte. Wir arbeiten NICHT mit Herstellern zusammen und stehen NICHT für eine einzige Marke, die wir dir empfehlen! Wir sind eine unabhängige Dienstleistung und stehen für den Kunden und seinen Wunsch, das optimale Supplement anhand seiner Vorlieben zu finden. Das bedeutet, dass einzig und allein die Angaben die unser System von dir erhält, darüber entscheiden, welches Supplement dir empfohlen wird. Die Marke spielt dabei keine Rolle, solange das Produkt zu deinen Präferenzen optimal passt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
