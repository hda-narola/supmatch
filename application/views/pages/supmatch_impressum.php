<div class="main_content" style="background-image: url(assets/images/thank-you-bg.jpg)">
    <section class="about_us_padding">
        <div class="container">
            <div class="agbs_wrap text-center">
                <h1 class="white mb40">
                    Impressum
                </h1>
                <p class="white mb25">
                    SupMatch UG(haftungsbeschränkt)<br>
                    An der Gatower Heide, 96<br>
                    14089 Berlin<br><br>
                    E-Mail: business@supmatch.de
                </p>
                <h4 class="theme_color mb25 ">Vertreten durch</h4>
                <p class="white mb25">Geschäftsführer Timoteo Cura
                </p>
                <h4 class="theme_color mb25 ">Registereintrag</h4>
                <p class="white mb25">Eingetragen im Handelsregister.<br>
                    Registergericht: Berlin-Charlottenburg<br>
                    Registernummer: HRB 179209
                </p>
                <h4 class="theme_color mb25">Umsatzsteuer-ID</h4>
                <p class="white mb25">Umsatzsteuer-Identifikationsnummer nach §27a Umsatzsteuergesetz:<br>
                    DE307561191
                </p>
                <h4 class="theme_color mb25 ">Bildrechte</h4>
                <p class="white mb25">© bondarchik – iStock<br>
                    © bondarchik – jacoblund <br>
                    © bondarchik – mel-nik  <br>
                    © bondarchik – Bojan656 <br>
                    © bondarchik – Ivanko_Brnjakovic <br>
                    © bondarchik – Ivanko_Brnjakovic <br>
                    © Icons made by Gregor Cresnar from www.flaticon.com <br>
                    © Icons made by Nikita Golubev from www.flaticon.com <br>
                    © Icon made by Revicon from www.flaticon.com <br>
                    © Icon made by Freepik from www.flaticon.com 
                </p>
            </div>
        </div>
    </section>
</div>