<div class="main_content" style="background-image: url(assets/images/thank-you-bg.jpg)">
    <section class="about_us_padding">
        <div class="container">
            <div class="agbs_wrap text-center">
                <h1 class="white mb40">
                    AGB
                </h1>
                <h4 class="theme_color mb25 ">Lorem Ipsum</h4>
                <p class="white mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>


                <h4 class="theme_color mb25 ">Donec blandit vel diam sed dapibus</h4>
                <p class="white mb25">Donec blandit vel diam sed dapibus. Donec finibus turpis tellus, vel blandit est consequat non. Nunc luctus, enim at eleifend placerat, magna diam sodales diam, a dignissim turpis nunc ac augue. Sed lorem tellus, maximus id pharetra nec, commodo vitae orci. Proin eleifend posuere orci, quis ullamcorper arcu maximus quis. Cras mattis nibh quis erat blandit mattis. Donec lobortis arcu at tincidunt congue. Nulla sit amet dui at nunc placerat laoreet nec vel augue. Cras aliquam ligula a risus porta volutpat. Morbi vitae pretium orci. Maecenas commodo, velit in laoreet consectetur, velit enim accumsan ex, vitae eleifend ex libero at nibh. Phasellus nec tortor lobortis, hendrerit tortor eget, suscipit purus. Suspendisse ullamcorper lectus ac nibh dignissim, at rutrum augue vulputate. Mauris quam augue, viverra sed porta nec, tempor ut erat. Integer tempus nisl at felis porttitor, cursus eleifend mauris mollis.

                </p>

                <h4 class="theme_color mb25 ">Aliquam erat volutpat </h4>

                <p class="white mb25">Aliquam erat volutpat. Vestibulum quis lectus nec libero maximus elementum et et massa. Phasellus eu lectus sodales, luctus metus ac, hendrerit odio. Integer feugiat id sapien eget bibendum. Suspendisse potenti. Donec elit orci, sollicitudin volutpat nisl sit amet, commodo gravida ex. Donec vitae scelerisque arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean quis sem purus. Donec mattis, ligula vitae sollicitudin vehicula, dolor metus fringilla odio, eget faucibus est erat congue ipsum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce euismod dignissim fermentum. Nam semper posuere auctor. Proin dapibus justo tellus, ut egestas tellus lacinia at.

                </p>
                <h4 class="theme_color mb25 ">Phasellus a mollis purus</h4>

                <p class="white mb25">Phasellus a mollis purus, id placerat lacus. Donec tincidunt et nulla eget consequat. Sed lorem leo, tincidunt nec accumsan sed, finibus eget odio. Phasellus a mi finibus, aliquam metus vel, faucibus nisl. Suspendisse non dignissim odio, in venenatis lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla semper volutpat est, sed iaculis sem hendrerit congue. Proin nisi dolor, bibendum nec maximus id, volutpat non dolor. Curabitur blandit molestie ligula. Pellentesque hendrerit eu velit convallis luctus. Sed posuere mauris risus, sed vehicula justo dapibus eget. Maecenas lobortis massa aliquet pulvinar sodales. Vestibulum risus odio, vestibulum mattis leo non, auctor eleifend felis.
                </p>
            </div>


        </div>
    </section>
</div>