<div id="fakeLoader"></div>
<div class="banner_outer_wrap default_bg disable_section active" id="home1" style="background-image:url(assets/images/banner-bg.jpg)">  
    <div class="banner_text">
        <h2 class="white mb15"><?= $this->lang->line('wc_to_supmatch') ?></h2>
        <p class="white">FINDE KOSTENLOS UND IN KÜRZESTER ZEIT,<br>DEIN PERFEKTES SUPPLEMENT!</p>
        <a data-toggle="modal" href="#youtube_modal"><img id="btn-video-play" src="assets/images/video-icon.png" alt=""></a>
    </div>
    <div class="banner_form form_style">
        <h2><?= $this->lang->line('home_plz_select_category') ?></h2>
        <form id="home1_form">
            <select name="home1_category" id="home1_category">
                <option value="">Wähle...</option>
                <?php foreach ($categoryArr->result() as $cat_result) { ?>
                    <option value="<?php echo $cat_result->id; ?>"><?php echo $cat_result->name_gr; ?></option>
                <?php } ?>
            </select>
            <button class="theme_bg" type="button" onclick="home1_start_matching()"><?= $this->lang->line('home_start_matching') ?></button>
        </form>
        <label class="home1_category_error red_color mb10" id="home1_category_error" style="text-align:left"></label>
        <p class="white mb20">DU WEIßT NOCH NICHT WELCHE SUPPLEMENTS ZU DEINEM ZIEL PASSEN?</p>
        <a class="form_link" href="javascript:void(0);" onclick="home1_free_recommendation()"><?php echo $this->lang->line('home_free_recommendation') ?></a>
    </div>
    <input type="hidden" name="hidden_searching_type" id="hidden_searching_type">
</div>

<div class="main_content">
    <section class="section_bg_image gallery_wrap_bg hide disable_section" id="home2" style="background-image: url(assets/images/gallery-bg.jpg);">
        <div class="container">
            <span class="gt_sec_close_btn" id="home2_close" onclick="close_home('home2')"><img src="assets/images/close-01.png" alt=""></span>
            <div class="heading_wrap mb30 default_width">
                <h5 class="white mb15"> Finde heraus, welche Supplements zu deinem Ziel passen!</h5>
                <h4 class="theme_color"><?= $this->lang->line('home_choose_your_goal') ?></h4>
            </div>
            <div class="row">
                <?php
                $cnt = 1;
                foreach ($goalsArr->result() as $goals) {
                    ?>
                    <div class="col-md-6">
                        <div class="gallery_wrap default_width mb30 goals_div" id="goal_<?php echo $goals->id; ?>" onclick="home2_goal_selection(this)">
                            <img src="<?php echo 'assets/images/gallery-0' . $cnt . '.jpg'; ?>" alt="">
                            <div class="gallery_img_des">
                                <h4 class="white"><?= $goals->name_gr ?></h4>
                            </div>
                        </div>
                    </div>
                    <?php
                    $cnt++;
                }
                ?>
                <label class="home2_goal_selection_error red_color mb10" id="home2_goal_selection_error" style="text-align:center"></label>
            </div>
            <div class="read_more default_width read_more_btn_div">
                <a href="javascript:void(0);" onclick="home2_goal_submit()"><?php echo $this->lang->line('home_continue') ?></a>
            </div>
        </div>
    </section>

    <section class="section_bg_image hide disable_section" id="home3" style="background-image: url(assets/images/product-bg.jpg);">
        <div class="container">
            <span class="gt_sec_close_btn" id="home3_close" onclick="close_home('home3')"><img src="assets/images/close-02.png" alt=""></span>
            <div class="heading_wrap mb30 default_width">
                <h5 class="white mb15">
                    Diese Produktkategorien sind für dein Ziel optimal!<br/>
                    Welche Kategorie kommt deinen Zielen am nähesten?
                </h5>
                <!--                <h5 class="white mb15"><?= $this->lang->line('home_banner3_text1') ?>
                                    <br>
                <?= $this->lang->line('home_banner3_text2') ?>
                                </h5>-->
                <h4 class="black"><?= $this->lang->line('home_plz_select_off') ?></h4>
            </div>
            <div class="row" id="home3_cat_row">
                <!-- <?php
                $cnt = 1;
                foreach ($categoryArr->result() as $category) {
                    ?>
                                                                                                                    <div class="col-md-3 col-sm-6">
                                                                                                                        <div class="product_wrap default_width category_div" id="category_<?php echo $category->id; ?>" onclick="home3_category_selection(this)">
                                                                                                                            <img class="mb20" src="<?php echo 'assets/images/product-0' . $cnt . '.png'; ?>" alt="">
                                                                                                                            <h4 class="white mb15"><?= $category->name_gr ?></h4>
                                                                                                                            <p class="white mb20"><?= $category->desc_gr ?></p>
                                                                                                                        </div>
                                                                                                                    </div>
                    <?php
                    $cnt++;
                }
                ?> -->
            </div>
            <label class="home3_category_selection_error mb10" id="home3_category_selection_error" style="text-align:center;font-weight:bold;color:#000"></label>
            <div class="more_product default_width">
                <a href="javascript:void(0);" onclick="home3_category_submit()"><?= $this->lang->line('home_start_matching') ?></a>
            </div>
        </div>
    </section>

    <section class="section_bg_image gallery_wrap_bg hide disable_section" id="home4" style="background-image: url(assets/images/gallery-bg.jpg);">
        <div class="container">
            <form action="supplements/listing" id="home4_form" method="post">
                <span class="gt_sec_close_btn" id="home4_close" onclick="close_home('home4')"><img src="assets/images/close-03.png" alt=""></span>
                <div class="heading_wrap mb30 default_width range_main_hdg responsive_hdg_show">
                    <h4 class="theme_color">DEIN PRODUKTMATCHING</h4>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="product_wrap default_width" id="selected_cateogry">
                            <img src="" alt="" id="selected_cateogry_img">
                        </div>
                    </div>

                    <div class="col-md-5 product_margin_left">
                        <div class="heading_wrap mb30 default_width range_main_hdg responsive_hdg_hide">
                            <h4 class="theme_color">DEIN PRODUKTMATCHING</h4>
                        </div>
                        <div class="gt_product_range mb30 default_width">
                            <!--<div class="product_tab1 select">-->
                            <div class="product_tab1" id="cat_powder_div">
                                <img src="assets/images/medicine-01.png" alt="">
                                <h5>PULVER</h5>
                            </div>
                            <div class="product_tab1" id="cat_pill_div">
                                <img src="assets/images/medicine-02.png" alt="">
                                <h5>PILLE</h5>
                            </div>
                        </div>
                        <div class="range_des text-center default_width">
                            <p class="white mb15">Bitte gib an, wie wichtig dir folgende Eigenschaften sind.
                                <br>
                                <b>Du kannst noch <span id="number_of_points_allowed">30</span> Punkte vergeben:</b>
                            </p>
                        </div>
                        <!--                        <div class="range_slider_list default_width mb20" id="property_slider_div">
                                                     <div class="range_slider default_width mb10">
                                                        <span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Preis</span>
                                                        <div class="range_slider_wrap default_width">
                                                            <div class="leftLabel white"></div>
                                                            <div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15" data-cur_max="5">
                                                                <div class="bar"></div>
                                                                <div class="leftGrip"></div>
                                                                <div class="rightGrip"></div>
                                                            </div>
                                                            <div class="rightLabel white">10</div>
                                                        </div>
                                                    </div>
                                                    <div class="range_slider default_width mb10">
                                                        <span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Geschmack</span>
                                                        <div class="range_slider_wrap default_width">
                                                            <div class="leftLabel white"></div>
                                                            <div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15" data-cur_max="4">
                                                                <div class="bar"></div>
                                                                <div class="leftGrip"></div>
                                                                <div class="rightGrip"></div>
                                                            </div>
                                                            <div class="rightLabel white">10</div>
                                                        </div>
                                                    </div>
                                                    <div class="range_slider default_width mb10">
                                                        <span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Effektivität</span>
                                                        <div class="range_slider_wrap default_width">
                                                            <div class="leftLabel white"></div>
                                                            <div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15" data-cur_max="3">
                                                                <div class="bar"></div>
                                                                <div class="leftGrip"></div>
                                                                <div class="rightGrip"></div>
                                                            </div>
                                                            <div class="rightLabel white">10</div>
                                                        </div>
                                                    </div>
                                                    <div class="range_slider default_width mb10">
                                                        <span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Verdaulichkeit</span>
                                                        <div class="range_slider_wrap default_width">
                                                            <div class="leftLabel white"></div>
                                                            <div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15" data-cur_max="7">
                                                                <div class="bar"></div>
                                                                <div class="leftGrip"></div>
                                                                <div class="rightGrip"></div>
                                                            </div>
                                                            <div class="rightLabel white">10</div>
                                                        </div>
                                                    </div>
                                                    <div class="range_slider default_width mb10">
                                                        <span class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Lösbarkeit</span>
                                                        <div class="range_slider_wrap default_width">
                                                            <div class="leftLabel white"></div>
                                                            <div class="nstSlider" data-range_min="0" data-range_max="10" data-cur_min="0.15" data-cur_max="6">
                                                                <div class="bar"></div>
                                                                <div class="leftGrip"></div>
                                                                <div class="rightGrip"></div>
                                                            </div>
                                                            <div class="rightLabel_range white">10</div>
                                                        </div>
                                                    </div> 
                                                </div>-->
                        <div class="range_slider_list default_width mb20" id="js_slider_div"></div>
                        <div class="range_des text-center default_width">
                            <p class="white mb15">Optionale Eigenschaften
                            </p>
                        </div>
                        <div class="link_list_wrap default_width mb20">
                            <div id="sec_properties_div">
                                <ul class="mb10">
                                    <li>
                                        <a href="javascript:void(0);" class="sec_properties" id="sec_vegan" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Vegan</a>
                                        <a href="javascript:void(0);" class="sec_properties" id="sec_lactose_free" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Laktosefrei</a>
                                        <a href="javascript:void(0);" class="sec_properties" id="sec_gluten_free" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Glutenfrei</a>
                                        <a href="javascript:void(0);" class="sec_properties" id="sec_koelnerliste" data-toggle="tooltip" data-placement="bottom" title="Lieber Kunde, über unseren Referallink zu bestellen unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern! Vielen Dank!">Kölner Liste</a>
                                    </li>
                                </ul>
                            </div>
                            <button type="button" name="btn_home4_submit" id="btn_home4_submit" class="btn_home_page_submit" onclick="home_page_submit()">DEIN BESTES PRODUKT FINDEN!</button>
                            <!-- <a href="javascript:void(0);" onclick="home_page_submit()">DAS BESTE PRODUKT FINDEN!</a> -->
                        </div>
                    </div>
                </div>
                <input type="hidden" name="hidden_goal_id" id="hidden_goal_id">
                <input type="hidden" name="hidden_category_id" id="hidden_category_id">
                <input type="hidden" name="hidden_supplement_type" id="hidden_supplement_type">
                <input type="hidden" name="hidden_number_of_points_allowed" id="hidden_number_of_points_allowed">
                <input type="hidden" name="hidden_primary_property_ids" id="hidden_primary_property_ids">
                <input type="hidden" name="hidden_primary_properties" id="hidden_primary_properties">
                <input type="hidden" name="hidden_secondary_property_ids" id="hidden_secondary_property_ids">
                <input type="hidden" name="hidden_secondary_properties" id="hidden_secondary_properties">
            </form>
        </div>
    </section>
</div>
<style>
    #btn-video-play:hover{opacity: 0.8;}
</style>