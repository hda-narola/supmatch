<?php

/**
 * Check_admin_login Hook Class to check Admin/Business User is logged in or not on every page 
 * @author KU
 */
class Check_admin_login {

    /**
     * initialize function
     * Checks admin/business user is loggedin or not if not loggedin then redirect to login page
     * @return void
     * */
    function initialize() {

        $CI = & get_instance();
        $admin = $CI->session->userdata('supmatch_admin');
        $directory = $CI->router->fetch_directory();
        $controller = $CI->router->fetch_class();
        $action = $CI->router->fetch_method();
        //-- Get directory to check admin/business directory
        if (!empty($directory)) {
            $directory = explode('/', $directory);
            $directory = $directory[0];
        }
       
        if (!(empty($directory))) {
            //-- If admin is not logged in then redirect to login page with agent referrer set
            if (empty($admin) && $controller != 'login') {
                $redirect = site_url(uri_string());
                redirect('admin?redirect=' . base64_encode($redirect));
            }
            //-- If admin is already logged in and access the login page then redirect to home page 
            if (!empty($admin) && $controller == 'login' && $action != 'logout') {
                //-- If Admin is logged in then
                redirect('admin/home');
            }
        } else {
            
        }
    }

}

?>