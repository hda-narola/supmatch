<?php

/**
 * Home Controller - Landing Page of Supmatch
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('categories_model', 'goals_model'));
    }

    /**
     * Display Landing page of Supmatch
     */
    public function index() {
        $data['title'] = 'Supmatch';
        $data['user_session_id'] = $this->checkLogin('ID');
        $condition = array('status' => 'active');
        $sort_condition = array(array('field' => 'id', 'type' => 'ASC'));
        //$limit = array('l1' => 4,'l2' => 0);
        $data['categoryArr'] = $categoryArr = $this->categories_model->get_all_details(TBL_CATEGORIES, $condition, $sort_condition);

        $condition = array('status' => 'active');
        $data['goalsArr'] = $goalsArr = $this->goals_model->get_all_details(TBL_GOALS, $condition);

        $this->template->load('frontend', 'home', $data);
    }

    /**
     * Ajax function to get category details when user select goals from #home2 screen page
     * @author pav
     */
    public function ajax_get_cat_by_goals() {
        $goal_id = $this->input->post('goal_id');
        $result = $this->goals_model->get_cat_by_goal_id($goal_id);
        $str = '';
        foreach ($result->result() as $cat) {
            $str.= '<div class="col-md-3 col-sm-6">' .
                    '<div class="product_wrap default_width category_div" id="category_' . $cat->cat_id . '" onclick="home3_category_selection(this)">' .
                    '<div class="cat_img_goal"><img class="mb20" src="' . CATEGORIES_IMG . $cat->cat_image . '" alt="" id="img_category_' . $cat->cat_id . '"></div>' .
                    '<h4 class="white mb15">' . $cat->cat_name_gr . '</h4>' .
                    '<p class="white mb20">' . $cat->cat_desc_gr . '</p>' .
                    '</div>' .
                    '</div>';
        }
        echo json_encode($str);
        exit;
    }

    /**
     * Send Email to admin on Add product request
     * @author KU
     */
    public function add_product_request() {
        $user_session_id = $this->checkLogin('ID');
        //-- Check if request is made by logged in user or guest
        $user = "Guest";
        if ($user_session_id != '') {
            $user = $this->checkLogin('N') . ' (' . $this->checkLogin('E') . ')';
        }
        $msg = "Hi New request is made by <b>" . $user . "</b> to add product. Following are details for the same.<br/>";
        $category = $this->categories_model->get_all_details(TBL_CATEGORIES, array('id' => trim($this->input->post('add_product_category'))))->row_array();
        $msg.= "<b>Product Name :</b> " . trim($this->input->post('add_product_name')) . "<br/>";
        $msg.= "<b>category :</b> " . $category['name_gr'] . "/" . $category['name_en'] . "<br/>";
        $msg.= "<b>Amazon Link :</b> " . trim($this->input->post('add_product_amazonlink')) . "<br/>";
        $msg.= "<b>Product Description :</b> " . trim($this->input->post('add_product_description')) . "<br/>";
        $msg.= "<b>Email :</b> " . trim($this->input->post('add_product_email')) . "<br/>";
        $email_array = array(
            'mail_type' => 'html',
            'from_mail_id' => $this->config->item('smtp_user'),
            'from_mail_name' => '',
//            'to_mail_id' => 'ku@narola.email',
            'to_mail_id' => ADDPRODUCT_EMAIL,
            'cc_mail_id' => '',
            'subject_message' => 'Supmatch | Add Product Request',
            'body_messages' => $msg
        );
        $this->users_model->common_email_send($email_array);
        $this->session->set_flashdata('success', 'Deine Anfrage wurde erfolgreich versandt.');
        redirect('home');
    }

    /**
     * Ajax call to this function returns all user reviews
     */
    public function get_user_reviews() {
        $user_session_id = $this->checkLogin('ID');
        $order_by = $this->input->post('review_sort_option');
        $user_reviews = $this->review_model->get_user_reviews($user_session_id, $order_by);
        $view = '';
        foreach ($user_reviews as $user_review) {

            $view.='<div class="tab_product_buyer_wrap mb25">
                <a href="' . site_url('supplements/view_supplement/' . $user_review['supplement_id']) . '">
                                                    <div class="buyer_product_img">';
            $image = explode(",", $user_review['images']);
            $view.='<img src="' . SUPPLEMENTS_IMG . $image[0] . '" alt="' . $user_review['product_name'] . '">
                                                    </div>
                                                    </a>
                                                    <div class="buyer_product_des">
                                                        <div class="buyer_name mb15">
                                                            <img src="assets/images/buyer_img.png" alt="">
                                                            <p class="no_margin black">
                                                                <span style="color:' . $user_review['color'] . '">' . $user_review['nickname'] . '</span> - ' . date('F d , Y', strtotime($user_review['created'])) . '.</p>
                                                        </div>
                                                        <div class="product_gradiant mb15">
                                                            <ul>
                                                                <li>
                                                                    <span class="theme_color">Geschmack:</span>
                                                                    ' . $user_review['flavour'] . '
                                                                </li>
                                                                <li>
                                                                    <span class="theme_color">Gemischt mit:</span>
                                                                    ' . $user_review['liquid'] . '
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="buyer_des default_width mb20">
                                                            <p>' . $user_review['comment'] . '</p>
                                                        </div>
                                                        <div class="product_rating">';
            $properties = explode(",", $user_review['properties']);
            $property_ratings = explode(",", $user_review['property_ratings']);
            $view.='<ul>';
            foreach ($properties as $key => $property) {
                $view.='<li>
                            <span>' . $property . '</span>
                                <i class="fa fa-star theme_color"></i>
                                <p>' . $property_ratings[$key] . '</p>
                                </li>';
            }
            $view.='</ul>
                        </div>
                        <div class="gt_product_like">
                            <ul>
                                <li>
                                <img src="assets/images/like.png" alt="Like" id="like_button_' . $user_review['id'] . '">
                                <p id="total_likes_' . $user_review['id'] . '" class="user_review_like_txt">' . $user_review['likes'] . '</p>
                                </li>
                                <li>
                                <img src="assets/images/unlike.png" alt="Dislike" id="dislike_button_' . $user_review['id'] . '">
                                <p id="total_dislikes_' . $user_review['id'] . '" class="user_review_like_txt">' . $user_review['dislikes'] . '</p>
                                </li>
                            </ul>
                            </div>
                            </div>
                            </div>';
        }
        echo $view;
        exit;
    }

    public function send_contact_email() {
        $ref = $this->agent->referrer();
        if ($this->input->post()) {
            $msg = "Hi New contact request received. Following are details for the same.<br/>";
            $msg .= '<strong>From:</strong> ' . $this->input->post('sender_email_contact') . '<br>';
            $msg .= '<strong>Subject:</strong> ' . $this->input->post('sender_subject_contact') . '<br>';
            $msg .= '<strong>Message:</strong> ' . $this->input->post('email_content_text');

            $email_array = array(
                'mail_type' => 'html',
                'from_mail_id' => $this->config->item('smtp_user'),
                'from_mail_name' => '',
//                'to_mail_id' => 'sg@narola.email',
                'to_mail_id' => CONTACT_SUPPMATCHEMAIL,
                'cc_mail_id' => '',
                'subject_message' => 'Supmatch | Contact request',
                'body_messages' => $msg
            );

            $this->users_model->common_email_send($email_array);
            $this->session->set_flashdata('success', 'Deine Nachricht wurde erfolgreich versandt.');
            if (!empty($ref)) {
                redirect($ref);
            } else {
                redirect('home');
            }
        }
    }

    /**
     * Ajax function to get category details
     * @author KU
     */
    public function get_cat_details() {
        $category_id = $this->input->get_post('category_id');
        $result = $this->categories_model->get_category($category_id);
        $result['properties'] = $this->properties_model->get_properties_by_category($category_id);
        echo json_encode($result);
        exit;
    }

}
