<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crone extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('AmazonECS');
        $this->load->model(array('supplements_model'));
    }

    /**
     * This function is run automatically and update price of supplements
     * @param NULL
     * @author pav
     */
    public function get_supplements_price() {
        //echo urldecode('AHqgHSQ4rUxaTaLkokhgyZKXtUAdI%2BnwZwCWUuKSG2Ge27f2W6EzxzBhEfpHnUqQ%2Fogl87OwpXdVBLSgWocNmM8lIfjGcpffumx9uFATHrU%3D'); die;
        $aws_access_key_id = AWS_API_KEY;
        $aws_secret_key = AWS_API_SECRET_KEY;
        $endpoint = "webservices.amazon.de";
        $uri = "/onca/xml";
        $supp_sort_condition = array(array('field' => 'id', 'type' => 'ASC'));
        $suppArr = $this->supplements_model->get_all_details(TBL_SUPPLEMENTS, array(), $supp_sort_condition)->result();
        foreach ($suppArr as $supp) {
            $price = $supp->price;
            $asin = $supp->amazon_ASIN;

            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "ItemLookup",
                "AWSAccessKeyId" => AWS_API_KEY,
                "AssociateTag" => AWS_ASSOCIATE_TAG,
                "ItemId" => "B000QSNYGI",
                "IdType" => "ASIN",
                "ResponseGroup" => "Images,ItemAttributes,Offers"
            );
            if (!isset($params["Timestamp"])) {
                $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
            }
            ksort($params);
            $pairs = array();
            foreach ($params as $key => $value) {
                array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
            }
            $canonical_query_string = join("&", $pairs);
            $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
            $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
            $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);
            $response = simplexml_load_file($request_url);
//            print_r($response); die;
            if (!empty($response)) {
                if (!empty($response->Items)) {
                    if (!empty($response->Items->Item->ItemAttributes)) {
                        if (!empty($response->Items->Item->Offers)) {
//                            $price = str_replace(',','.',explode(' ',$response->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice)[1]);
                            $price = str_replace(',', '.', explode(' ', $response->Items->Item->Offers->Offer->OfferListing->SalePrice->FormattedPrice)[1]);
                            if ($price == '') {
                                $price = str_replace(',', '.', explode(' ', $response->Items->Item->ItemAttributes->ListPrice->FormattedPrice)[1]);
                            }
                        } else if (!empty($response->Items->Item->ItemAttributes->ListPrice)) {
                            $price = str_replace(',', '.', explode(' ', $response->Items->Item->ItemAttributes->ListPrice->FormattedPrice)[1]);
                        }
                        echo " ";
                    } else {
                        $price = $supp->price;
                    }
                }
            }

            $update_price[] = array(
                'id' => $supp->id,
                'price' => number_format($price, 2)
            );
        }
        $this->db->update_batch(TBL_SUPPLEMENTS, $update_price, 'id');
    }

    public function get_availability() {
        $aws_access_key_id = AWS_API_KEY;
        $aws_secret_key = AWS_API_SECRET_KEY;
        $endpoint = "webservices.amazon.de";
        $uri = "/onca/xml";
        $supp_sort_condition = array(array('field' => 'id', 'type' => 'ASC'));
        $suppArr = $this->supplements_model->get_all_details(TBL_SUPPLEMENTS, array(), $supp_sort_condition)->result();

        foreach ($suppArr as $supp) {
            $asin = $supp->amazon_ASIN;
            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "ItemLookup",
                "AWSAccessKeyId" => AWS_API_KEY,
                "AssociateTag" => AWS_ASSOCIATE_TAG,
                "Availability" => "Available",
                "ItemId" => $asin,
                "IdType" => "ASIN",
                "ResponseGroup" => "Images,ItemAttributes,Offers"
            );


            if (!isset($params["Timestamp"])) {
                $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
            }
            ksort($params);
            $pairs = array();
            foreach ($params as $key => $value) {
                array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
            }
            $canonical_query_string = join("&", $pairs);
            $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
            $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
            $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);
            $response = simplexml_load_file($request_url);
            $offerId = '';
            if (!empty($response)) {
                if (!empty($response->Items)) {
                    if (!empty($response->Items->Item->ItemAttributes)) {

                        if (!empty($response->Items->Item->Offers)) {
                            $offerId = str_replace(',', '.', explode(' ', $response->Items->Item->Offers->Offer->OfferListing->OfferListingId)[0]);
                        }
                    }
                }
            }

            if (!empty($offerId)) {
                $update_available[] = array(
                    'id' => $supp->id,
                    'availibility' => 1
                );
            } else {
                $update_available[] = array(
                    'id' => $supp->id,
                    'availibility' => 0
                );
            }
        }
        $this->db->update_batch(TBL_SUPPLEMENTS, $update_available, 'id');
    }

    public function create_cart() {
// Your AWS Access Key ID, as taken from the AWS Your Account page
        $aws_access_key_id = "AKIAJOAMIH2FPHYZFDDQ";

// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
        $aws_secret_key = "uudZm99RbFpWN04u5p3KZcePfqfCPT3SnUDvvXt/";

// The region you are interested in
        $endpoint = "webservices.amazon.de";

        $uri = "/onca/xml";

        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "CartCreate",
            "AWSAccessKeyId" => "AKIAJOAMIH2FPHYZFDDQ",
            "AssociateTag" => "hilfespielsuc-21",
            "Item-1-ASIN" => "B01CJS8CF0",
            "Item-1-Quantity" => "1",
            "ResponseGroup" => "Cart"
        );

// Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

// Sort the parameters by key
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }

// Generate the canonical query
        $canonical_query_string = join("&", $pairs);

// Generate the string to be signed
        $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;

// Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

// Generate the signed URL
        $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);

        echo "Signed URL: \"" . $request_url . "\"";
    }

}
