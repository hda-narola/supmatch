<?php

/**
 * Supplements Controller - Manage Supplements
 * @author pav
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplements extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('supplements_model', 'properties_model', 'flavour_model', 'liquid_model', 'review_model', 'users_model', 'ranks_model'));
    }

    public function index() {
        
    }

    /**
     * Display supplements as per user's requirements
     * @author pav
     * @modify KU (included property ratings and review weight)
     */
    public function display_supplements() {
        $data['title'] = 'Supmatch | Supplements';
        $data['user_session_id'] = $this->checkLogin('ID');
        if ($this->input->post()) {
            $this->session->set_userdata('matching_data', $this->input->post());
            $category_id = $this->input->post('hidden_category_id');
            $supplement_type = $this->input->post('hidden_supplement_type');
            $primary_properties = $this->input->post('hidden_primary_properties');
            $primary_property_ids = $this->input->post('hidden_primary_property_ids');
            $secondary_properties = $this->input->post('hidden_secondary_properties');
            $secondary_property_ids = $this->input->post('hidden_secondary_property_ids');
        } else if ($this->session->userdata('matching_data')) {
            $category_id = $this->session->userdata('matching_data')['hidden_category_id'];
            $supplement_type = $this->session->userdata('matching_data')['hidden_supplement_type'];
            $primary_properties = $this->session->userdata('matching_data')['hidden_primary_properties'];
            $primary_property_ids = $this->session->userdata('matching_data')['hidden_primary_property_ids'];
            $secondary_properties = $this->session->userdata('matching_data')['hidden_secondary_properties'];
            $secondary_property_ids = $this->session->userdata('matching_data')['hidden_secondary_property_ids'];
        } else {
            show_404();
        }

        //-- Explode primary property's id and values into array
        $sup_properties = explode(",", $primary_property_ids);
        $property_vals = explode(",", $primary_properties);

        //-- Explode secondary property's id and values into array
//        $sup_secondary_properties = explode(",", $secondary_property_ids);
        $secondary_property_vals = array();
        if ($secondary_properties != '') {
            $secondary_property_vals = explode(",", $secondary_properties);
        }

        /*
          $condition = array(
          's.status' => 'active',
          's.category_ids' => $category_id,
          's.type' => $supplement_type,
          ); */

        $condition = 's.status=\'active\' AND s.category_ids=' . $category_id . ' AND s.type=' . $supplement_type;
        //-- If user has selected secondary properties then check for secondary properties also
        $condition1 = '';
        if (!empty($secondary_property_vals)) {
            $condition1 = 'FIND_IN_SET(' . $secondary_property_vals[0] . ',sp.secondary_properties) != 0';
            if (count($secondary_property_vals) > 2) {
                for ($i = 1; $i < count($secondary_property_vals); $i++) {
                    $condition1.=' OR FIND_IN_SET(' . $secondary_property_vals[$i] . ',sp.secondary_properties) != 0';
                }
            }
        }

        //-- If condition1 is not empty
        if ($condition1 != '') {
            $condition.=' AND (' . $condition1 . ')';
        }

        $select = "s.id,s.name_gr,s.price,s.images,s.amazon_link,s.accuracy,s.category_ids,"
                . "cp.property_id,ar.average_rating,p.price_rating,s.type,sp.secondary_properties";
        $downgrade = "sum(CASE";
        foreach ($property_vals as $key => $property_val) {
            if ($key == 0) {
                $downgrade.=" WHEN p.price_rating < " . $property_val . " THEN (" . $property_val . " - p.price_rating) * (" . $property_val . "/10)";
            } else {
                $downgrade.=" WHEN cp.property_id=" . $sup_properties[$key] . " AND ar.average_rating < " . $property_val . " THEN (" . $property_val . " - ar.average_rating) * (" . $property_val . "/10)";
            }
        }
        $downgrade.= " ELSE NULL END) AS DOWNGRADE";

        $upgrade = "sum(CASE";
        foreach ($property_vals as $key => $property_val) {
            if ($key == 0) {
                $upgrade.=" WHEN p.price_rating > " . $property_val . " THEN (p.price_rating - " . $property_val . ") * (" . $property_val . "/10)";
            } else {
                $upgrade.=" WHEN cp.property_id=" . $sup_properties[$key] . " AND ar.average_rating > " . $property_val . " THEN (ar.average_rating - " . $property_val . ") * (" . $property_val . "/10)";
            }
        }

        $upgrade.=" ELSE NULL END) AS UPGRADE";
        $select.="," . $downgrade . "," . $upgrade;
        $suppArr = $this->supplements_model->get_supplements_for_matching($select, $condition, $category_id, $supplement_type);
        $supplements = array();
        //-- Loop all three supplements and get primary properties and secondary prperties  
        foreach ($suppArr as $key => $value) {
            $supplements[$key] = $value;
            $supplements[$key]['price_property'] = $this->supplements_model->get_supplement_price_rating($value['category_ids'], $value['type'], $value['id']);
            $supplements[$key]['properties'] = $this->properties_model->get_properties_rating_cat_sup($value['category_ids'], $value['id'], $value['type']);
            $supplements[$key]['sec_properties'] = $this->properties_model->get_secproperties_by_supplement($value['id']);
        }
        $data['suppArr'] = $supplements;
        $data['price_property_details'] = $this->properties_model->get_price_property();
        $this->template->load('frontend', 'supplements/display_supplements', $data);
    }

    /**
     * View page for supplements
     * @param int $supplement_id
     * @author KU
     */
    public function view_supplement($supplement_id = NULL) {
        if (is_numeric($supplement_id)) {
            $data['supplement'] = $this->supplements_model->get_all_supplement_details_by_id($supplement_id)->row_array();
            if ($data['supplement']) {
                $session_user_id = $this->checkLogin('ID');
                if ($session_user_id == '') {
                    $session_user_id = 0;
                }
                $data['price_property'] = $this->supplements_model->get_supplement_price_rating($data['supplement']['category_ids'], $data['supplement']['type'], $supplement_id);
                $data['properties'] = $this->properties_model->get_properties_rating_cat_sup($data['supplement']['category_ids'], $supplement_id, $data['supplement']['type']);
                $data['flavours'] = $this->flavour_model->get_flavours_by_supplement($data['supplement']['id']);
                $data['liquids'] = $this->liquid_model->get_liquids_by_category($data['supplement']['category_ids'], $data['supplement']['type']);
                $data['sec_properties'] = $this->properties_model->get_secproperties_by_supplement($supplement_id);
                $reviews = $this->supplements_model->get_supplement_review_by_id($supplement_id, $session_user_id)->result();
                $reviewsArr = array();
                foreach ($reviews as $k => $v) {
                    $tot_likeArr = $this->supplements_model->get_total_likes_dislikes_by_supp($v->review_id)->result();
                    $sess_likeArr = $this->supplements_model->get_sess_user_like_dislikes($session_user_id, $v->review_id)->result();
                    $propertyArr = array();
                    if ($v->prop_name_gr != '') {
                        $propertyArr = explode(",", $v->prop_name_gr);
                    }

                    $ratingArr = explode(",", $v->prop_rating);
                    $reviewsArr[$k]['review_id'] = $v->review_id;
                    $reviewsArr[$k]['user_id'] = $v->user_id;
                    $reviewsArr[$k]['username'] = $v->user_name;
                    $reviewsArr[$k]['review_date'] = $v->review_date;
                    $reviewsArr[$k]['flavour'] = $v->flavour_name_gr;
                    $reviewsArr[$k]['liquid'] = $v->liquid_name_gr;
                    $reviewsArr[$k]['comment'] = $v->review_comment;
                    $reviewsArr[$k]['flavour'] = $v->flavour_name_gr;
                    $reviewsArr[$k]['color'] = $v->color;
                    $reviewsArr[$k]['rank_icon'] = $v->rank_icon;

                    $reviewsArr[$k]['property_rating'] = array();
                    for ($i = 0; $i < count($propertyArr); $i++) {
                        $reviewsArr[$k]['property_rating'][] = $propertyArr[$i] . ':-:' . $ratingArr[$i];
                    }
                    $reviewsArr[$k]['total_likes'] = $tot_likeArr[0]->tot_likes; //$v->total_likes;
                    $reviewsArr[$k]['total_dislikes'] = $tot_likeArr[0]->tot_dislikes; //$v->total_dislikes;
                    $reviewsArr[$k]['sess_user_likes'] = $sess_likeArr[0]->sess_user_likes;
                    $reviewsArr[$k]['sess_user_dislikes'] = $sess_likeArr[0]->sess_user_dislikes;
                }
                //p($reviewsArr); die;
                $data['reviewsArr'] = $reviewsArr;
                $data['title'] = 'Supmatch | Supplements';
                $data['user_session_id'] = $this->checkLogin('ID');
                //--if user is logged in then check whether user has posted review or not
                $data['user_posted_review'] = 0;
                if ($data['user_session_id'] != '') {
                    $condition = array('user_id' => $data['user_session_id'], 'supplement_id' => $supplement_id);
                } else {
                    //-- get user's ip adderess and check if he has posted review or not
                    $ip_address = $this->input->ip_address();
                    $condition = array('user_id' => NULL, 'supplement_id' => $supplement_id, 'ip_address' => $ip_address);
                }
                $user_review = $this->review_model->get_review($condition);
                if ($user_review) {
                    $data['user_posted_review'] = 1;
                    $data['user_review'] = $user_review;
                }
                $data['price_property_details'] = $this->properties_model->get_price_property();
                $this->template->load('frontend', 'supplements/view_supplement', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Return supplement result for searchbar autocomplete
     * @author KU
     */
    public function search_suggest() {
        $query = $this->input->get_post('query');
        $result = $this->supplements_model->search_suggest($query);
        echo json_encode($result);
        exit;
    }

    /**
     * Post Review to a supplement
     * @author KU
     */
    public function post_review() {
        $user_id = $this->checkLogin('ID');
        $weight = 1.0; //-- User post review without comment
        $comment = NULL;
        if (trim($this->input->post('review_comment')) != '') {
            $comment = trim($this->input->post('review_comment'));
            $weight = 2.0; //- User post review with comment
        }
        if ($user_id == '') {
            $user_id = NULL;
            $weight = 0.5; //-- If guest post review
            $comment = NULL;
        }
        $review_data = array(
            'user_id' => $user_id,
            'supplement_id' => $this->input->post('supplement_id'),
            'flavour_id' => $this->input->post('review_flavour'),
            'liquid_id' => $this->input->post('review_liquid'),
            'category_id' => $this->input->post('category_id'),
            'comment' => $comment,
            'ip_address' => $this->input->ip_address(),
            'weight' => $weight
        );

        //-- Get supplement's detail
        $supplement_data = $this->supplements_model->get_all_details(TBL_SUPPLEMENTS, array('id' => $this->input->post('supplement_id')))->row_array();
        $supplement_rating_id = $this->review_model->insert_review($review_data);
        $properties = $this->properties_model->get_properties_by_category($this->input->post('category_id'), $supplement_data['type']);
        $property_rating_array = array();
        foreach ($properties as $key => $property) {
            $property_rating_array[$key]['supplement_rating_id'] = $supplement_rating_id;
            $property_rating_array[$key]['property_id'] = $property['id'];
            $property_rating_array[$key]['rating'] = $this->input->post('hearts_' . $property['id']);
        }
        if ($property_rating_array) {
            $this->review_model->insert_property_ratings($property_rating_array);
        }
        //-- updates user rank if user is logged in 
        if ($user_id != '') {
            $this->update_user_rank($user_id);
        }
        $this->supplements_model->update_accuracy($this->input->post('supplement_id')); //- Update accuracy of the supplement

        $total_reviews = $this->supplements_model->total_reviews($this->input->post('supplement_id'));
        $total_reviews['total'];
        if ($total_reviews['total'] > 0) {
            $this->supplements_model->insert_update_supplement_properties_rating($this->input->post('supplement_id')); //-- Updates average rating property of supplement
        } else {
            $this->supplements_model->delete_properties_average_rating($this->input->post('supplement_id')); //-- Delete already exist property average rating
        }

        $this->session->set_flashdata('success', 'Deine Bewertung wurde erfolgreich gepostet.');
        redirect('supplements/view_supplement/' . $this->input->post('supplement_id'));
    }

    /**
     * Likes / Dislikes to supplement's review
     * @author pav
     * @modify KU (Update rank and update review weight)
     */
    public function like_dislike_supp_review() {
        $like_img = 'assets/images/like.png';
        $dislike_img = 'assets/images/unlike.png';
        $review_id = $this->input->post('review_id');
        $status = $this->input->post('status');
        $user_id = $this->checkLogin('ID');
        $supp_rating_result = $this->supplements_model->get_all_details(TBL_SUPPLEMENT_RATING, array('id' => $review_id))->row_array();
        if ($status == 'like') {
            $condition = array('srl.status' => 1, 'srl.supplement_rating_id' => $review_id, 'srl.user_id' => $user_id);
            $num_rows = $this->supplements_model->get_review_details_by_user_id($condition)->num_rows();
            if ($num_rows == 1) {
                $condition = array('supplement_rating_id' => $review_id, 'user_id' => $user_id);
                $this->db->where($condition);
                $this->db->delete(TBL_SUPPLEMENT_RATING_LIKE);
                $like_img = 'assets/images/like.png';
            } else {
                $condition = array('srl.status' => 0, 'srl.supplement_rating_id' => $review_id, 'srl.user_id' => $user_id);
                $num_dislike_rows = $this->supplements_model->get_review_details_by_user_id($condition)->num_rows();
                if ($num_dislike_rows == 0) {
                    $dataArr = array('status' => 1, 'supplement_rating_id' => $review_id, 'user_id' => $user_id);
                    $this->supplements_model->common_insert_update('insert', TBL_SUPPLEMENT_RATING_LIKE, $dataArr);
                    $like_img = 'assets/images/user_like.png';
                } else {
                    $dataArr = array('status' => 1);
                    $condition = array('supplement_rating_id' => $review_id, 'user_id' => $user_id);
                    $this->supplements_model->common_insert_update('update', TBL_SUPPLEMENT_RATING_LIKE, $dataArr, $condition);
                    $like_img = 'assets/images/user_like.png';
                    $dislike_img = 'assets/images/unlike.png';
                }
            }
            //-- Updates rank on particular rating like 
            $this->update_user_rank($supp_rating_result['user_id']);
        }
        if ($status == 'dislike') {
            $condition = array('srl.status' => 0, 'srl.supplement_rating_id' => $review_id, 'srl.user_id' => $user_id);
            $num_rows = $this->supplements_model->get_review_details_by_user_id($condition)->num_rows();
            if ($num_rows == 1) {
                $condition = array('supplement_rating_id' => $review_id, 'user_id' => $user_id);
                $this->db->where($condition);
                $this->db->delete(TBL_SUPPLEMENT_RATING_LIKE);
                $dislike_img = 'assets/images/unlike.png';
            } else {
                $condition = array('srl.status' => 1, 'srl.supplement_rating_id' => $review_id, 'srl.user_id' => $user_id);
                $num_dislike_rows = $this->supplements_model->get_review_details_by_user_id($condition)->num_rows();
                if ($num_dislike_rows == 0) {
                    $dataArr = array('status' => 0, 'supplement_rating_id' => $review_id, 'user_id' => $user_id);
                    $this->supplements_model->common_insert_update('insert', TBL_SUPPLEMENT_RATING_LIKE, $dataArr);
                    $dislike_img = 'assets/images/user_unlike.png';
                } else {
                    $dataArr = array('status' => 0);
                    $condition = array('supplement_rating_id' => $review_id, 'user_id' => $user_id);
                    $this->supplements_model->common_insert_update('update', TBL_SUPPLEMENT_RATING_LIKE, $dataArr, $condition);
                    $dislike_img = 'assets/images/user_unlike.png';
                    $like_img = 'assets/images/like.png';
                }
            }
        }
        $this->db->select('srl.id');
        $this->db->from(TBL_SUPPLEMENT_RATING_LIKE . ' as srl');
        $this->db->where(array('srl.supplement_rating_id' => $review_id, 'srl.status' => '1'));
        $tot_likes = $this->db->get()->num_rows();

        $this->db->select('srl.id');
        $this->db->from(TBL_SUPPLEMENT_RATING_LIKE . ' as srl');
        $this->db->where(array('srl.supplement_rating_id' => $review_id, 'srl.status' => 0));
        $tot_dislikes = $this->db->get()->num_rows();

        //-- Calculate like dislike ratio and update review weight
        if ($tot_dislikes != 0 && $tot_likes != 0) {
            if ($tot_likes > $tot_dislikes) {
                $like_ratio = $tot_likes / $tot_dislikes;
            } elseif ($tot_likes < $tot_dislikes) {
                $like_ratio = -($tot_dislikes / $tot_likes);
            } elseif ($tot_likes == $tot_dislikes) {
                $like_ratio = 0;
            }
        }

        //-- initial review weight
        if ($supp_rating_result['user_id'] == NULL) {
            $weight = 0.5;
        } else if ($supp_rating_result['comment'] == NULL) {
            $weight = 1;
        } else {
            $weight = 2;
        }
        // Review weight based on like ratio
        if ($tot_dislikes != 0 && $tot_likes != 0) {
            if ($like_ratio > 10) {
                $weight = 4;
            } else if ($like_ratio < -10) {
                $weight = 0;
            } else {
                $weight = $weight + ($like_ratio / 5);
            }
        }

        $rank = $this->users_model->get_user_rank($supp_rating_result['user_id']);
        $rank_factor = ($rank['rank'] / 10) * 2;

        $weight = $weight + $rank_factor;
        //-- Update weight of review
        $this->supplements_model->common_insert_update('update', TBL_SUPPLEMENT_RATING, array('weight' => $weight), array('id' => $review_id));
        $this->supplements_model->update_accuracy($supp_rating_result['supplement_id']); //- Update accuracy of the supplement

        $total_reviews = $this->supplements_model->total_reviews($supp_rating_result['supplement_id']);
        $total_reviews['total'];
        if ($total_reviews['total'] > 0) {
            $this->supplements_model->insert_update_supplement_properties_rating($supp_rating_result['supplement_id']); //-- Updates average rating property of supplement
        } else {
            $this->supplements_model->delete_properties_average_rating($supp_rating_result['supplement_id']); //-- Delete already exist property average rating
        }

        $resultArr = array(
            'tot_likes' => $tot_likes,
            'tot_dislikes' => $tot_dislikes,
            'like_img' => $like_img,
            'dislike_img' => $dislike_img
        );
        echo json_encode($resultArr);
        die;
    }

    /**
     * Updates user rank on review post and any like is recieved 
     * @param int $user_id
     */
    public function update_user_rank($user_id = NULL) {
        if (is_numeric($user_id)) {
            //-- Get user's review count and reviwe likes count 
            $result = $this->users_model->get_user_total_like_counts($user_id);
            $total_likes = $result['total'];
            $result = $this->users_model->get_user_total_reviews($user_id);
            $total_reviews = $result['total'];
            $rank_detail = $this->ranks_model->get_rank_by_reviews_likes($total_reviews, $total_likes);
            //-- If rank details found from database then update user rank 
            $rank_id = 0;
            $default_rank = $this->ranks_model->get_default_rank();
            if ($default_rank) {
                $rank_id = $default_rank['id'];
            }
            if ($rank_detail) {
                //-- Get user's old rank
                $old_rank = $this->users_model->get_user_rank($user_id);
                $rank_id = $rank_detail['id'];
                //-- If old rank is not same as new rank then send email to user regarding rank update
                if ($rank_id != $old_rank['rank_id']) {
//                    $msg = "Hello " . $old_rank['nickname'] . ", Your rank has been updated to <b>" . $rank_detail['name'] . "</b>";
                    $msg = $this->load->view('email_templates/rank_update', array('nickname' => $old_rank['nickname'], 'rank' => $rank_detail['name']), true);
                    $email_array = array(
                        'mail_type' => 'html',
                        'from_mail_id' => $this->config->item('smtp_user'),
                        'from_mail_name' => '',
                        'to_mail_id' => $old_rank['email'],
                        'cc_mail_id' => '',
                        'subject_message' => utf8_encode('Du hast einen neuen Rang erreicht!'),
                        'body_messages' => $msg
                    );
                    $this->users_model->common_email_send($email_array);
                }
            }
            $this->ranks_model->common_insert_update('update', TBL_USERS, array('rank_id' => $rank_id, 'total_reviews' => $total_reviews), array('id' => $user_id));
        }
    }

}

/* End of file Supplements.php */
/* Location: ./application/controllers/Supplements.php */