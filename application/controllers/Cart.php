<?php

/**
 * Cart Controller - Manage cart
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('cart_model', 'supplements_model', 'orders_model'));
    }

    /**
     * Lists all cart products
     */
    public function index() {
        $data['title'] = 'Supmatch | Cart';
        $data['user_session_id'] = $this->checkLogin('ID');
        if ($data['user_session_id'] != '') {
            $data['cart_items'] = $this->cart_model->get_cartitems($data['user_session_id'], 'result');
        } else {
            $data['cart_items'] = array();
        }

        if ($this->input->post()) {

            $today = date('Y-m-d H:i:s');
            $order_cnt = 0;
            foreach ($data['cart_items'] as $sup):
                $data_arr = array();
                $data_arr['user_id'] = $_SESSION['session_user_id'];
                $data_arr['sup_id'] = $sup['id'];
                $data_arr['quantity'] = $sup['qty'];
                $data_arr['order_date'] = $today;
                $data_arr['created'] = $today;
                $data_arr['modified'] = $today;
                $insert = $this->orders_model->insert_category($data_arr);
                $order_cnt++;
            endforeach;
            if ($order_cnt == sizeof($data['cart_items'])) {
                echo 'success';
            } else {
                echo 'fail';
            }
            exit;
        }

        $this->template->load('frontend', 'cart/list', $data);
    }

    /**
     * Ajax call to this function add product into user cart
     */
    public function add_cart_item() {
        $supplement_id = $this->input->post('supplement_id');
        $user_id = $this->checkLogin('ID');
        $quantity = 1;
        $supplement = $this->supplements_model->get_all_supplement_details_by_id($supplement_id)->row_array();
        $dataArr = array(
            'user_id' => $user_id,
            'supplement_id' => $supplement_id,
            'qty' => $quantity,
            'flavour_ids' => $supplement['flavour_ids'],
            'price' => $supplement['price']
        );
        //-- Check whether this product exist or not in cart if exist then update quantity of that product
        $cart = $this->cart_model->get_cart_item(array("user_id" => $user_id, "supplement_id" => $supplement_id));
        if ($cart) {
            $quantity = $cart['qty'] + 1;
            $dataArr['modified'] = date('Y-m-d H:i:s');
            $total = $quantity * $supplement['price'];
            $dataArr['qty'] = $quantity;
            $dataArr['total'] = $total;
            $this->cart_model->update_cartitem(array('id' => $cart['id']), $dataArr);
        } else {
            $total = $quantity * $supplement['price'];
            $dataArr['total'] = $total;
            $this->cart_model->insert_cartitem($dataArr);
        }
        //-- Get number of cart items 
        $result = $this->cart_model->get_cartitems($user_id, 'count');
        echo json_encode($result);
        exit;
    }

    /**
     * Ajax call to this function removes cart item 
     */
    public function remove_cart_item() {
        $user_id = $this->checkLogin('ID');
        $cart_id = $this->input->post('cart_id');
        $this->cart_model->delete_cartitem(array('id' => $cart_id));
        //-- Get number of cart items 
        $result = $this->cart_model->get_cartitems($user_id, 'count');
        echo json_encode($result);
        exit;
    }

}

/* End of file Cart.php */
/* Location: ./application/controllers/Cart.php */