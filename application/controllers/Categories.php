<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Manage Category related functionalities for home page
 * @author PAV
 */
class Categories extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('supplements_model', 'categories_model', 'properties_model', 'flavour_model', 'liquid_model'));
    }

    public function index() {
        
    }

    public function ajax_get_cat_by_id() {
        $category_id = $this->input->post('category_id');
        $cat_type = $this->categories_model->get_cat_type($category_id);
        if ($cat_type['type'] == 3 || $cat_type['type'] == 1) {
            $type = 1;
        } else {
            $type = 2;
        }
        //-- Get primary properties
        $result = $this->categories_model->get_cat_by_id($category_id, $type)->result();
        $property_ids = array();
        $property_ids[0] = 0;
        $price_property = $this->properties_model->get_price_property();
        $str = '<div class="range_slider default_width mb10">' .
                '<span class="theme_color mb10"><div data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($price_property['desc_gr']) . '" data-html="true">' . $price_property['name_gr'] . '</div></span>' .
                '<div class="range_slider_wrap default_width">' .
                '<div class="leftLabel white"></div>' .
                '<div class="nstSlider" data-range_min="1" data-range_max="10" data-cur_min="1" data-cur_max="5">' .
                '<div class="bar"></div>' .
                '<div class="leftGrip"></div>' .
                '<div class="rightGrip"></div>' .
                '</div>' .
                '<div class="rightLabel white">10</div>' .
                '</div>' .
                '</div>';
        $new_str = '<div class="range_slider default_width mb10">
                    <span class="theme_color mb10"><div data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($price_property['desc_gr']) . '" data-html="true">' . $price_property['name_gr'] . '</div></span>
                    <div class="range_slider_wrap default_width range-slide">
                        <div class="leftLabel white range-cell">1</div>
                       <div class="range-cell middel">
                        <div class="jsSlider"></div>
                        </div>
                        <div class="rightLabel white range-cell">1</div>
                    </div>
                </div>';
        foreach ($result as $prop) {
            if ($prop->prop_id != '') {
                $property_ids[] = $prop->prop_id;
                $str.= '<div class="range_slider default_width mb10">' .
                        '<span class="theme_color mb10"><div data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop->prop_desc_gr) . '" data-html="true">' . $prop->prop_name_gr . '</div></span>' .
                        '<div class="range_slider_wrap default_width">' .
                        '<div class="leftLabel white"></div>' .
                        '<div class="nstSlider" data-range_min="1" data-range_max="10" data-cur_min="1" data-cur_max="5">' .
                        '<div class="bar"></div>' .
                        '<div class="leftGrip"></div>' .
                        '<div class="rightGrip"></div>' .
                        '</div>' .
                        '<div class="rightLabel white">10</div>' .
                        '</div>' .
                        '</div>';
                $new_str.='<div class="range_slider default_width mb10">
                    <span class="theme_color mb10"><div data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop->prop_desc_gr) . '" data-html="true">' . $prop->prop_name_gr . '</div></span>
                    <div class="range_slider_wrap default_width range-slide">
                        <div class="leftLabel white range-cell">1</div>
                        <div class="range-cell middel">
                        <div class="jsSlider"></div>
                        </div>
                        <div class="rightLabel white range-cell">1</div>
                    </div>
                </div>';
            }
        }


        //-- Get secondary properties
        $secondary_properties = $this->categories_model->get_secproperties_by_catandtype($category_id, $type);
        $secondary_property_ids = array();
        $sec_string = '<ul class="mb10">';
        foreach ($secondary_properties as $prop) {
            $secondary_property_ids[] = $prop['id'];
            $sec_string.= '<li><a href="javascript:void(0);" class="sec_properties" id="sec_' . $prop['id'] . '" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop['desc_gr']) . '" onclick="sec_properties_click(this)" data-id="' . $prop['id'] . '" data-html="true">' . $prop['name_gr'] . '</a></li>';
        }
        $sec_string.='</ul>';
        $resultArr = array(
            'cat_image' => CATEGORIES_IMG . $result[0]->cat_image,
            'property_ids' => implode(",", $property_ids),
            'type' => $result[0]->type,
            'response' => $str,
            'new_slider' => $new_str,
            'secondary_property_ids' => implode(",", $secondary_property_ids),
            'secondary_properties' => $sec_string,
            'allowed_points' => (5 * count($property_ids)),
            'calculated_allowed_points' => ((5 * count($property_ids)) + count($property_ids))
        );
        echo json_encode($resultArr);
        exit;
    }

    /**
     * Returns all properties of category by its type selection(Either powder or pill)
     * @author KU
     */
    public function ajax_get_properties_by_cat_and_type() {
        $category_id = $this->input->post('category_id');
        $type = $this->input->post('type');
        $result = $this->categories_model->get_cat_by_id($category_id, $type)->result();
        $property_ids = array();
        $property_ids[0] = 0;
        $price_property = $this->properties_model->get_price_property();
        $str = '<div class="range_slider default_width mb10">' .
                '<span><div class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($price_property['desc_gr']) . '" data-html="true">' . $price_property['name_gr'] . '</div></span>' .
                '<div class="range_slider_wrap default_width">' .
                '<div class="leftLabel white"></div>' .
                '<div class="nstSlider" data-range_min="1" data-range_max="10" data-cur_min="1" data-cur_max="5">' .
                '<div class="bar"></div>' .
                '<div class="leftGrip"></div>' .
                '<div class="rightGrip"></div>' .
                '</div>' .
                '<div class="rightLabel white">10</div>' .
                '</div>' .
                '</div>';
        $new_str = '<div class="range_slider default_width mb10">
                    <span><div class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($price_property['desc_gr']) . '" data-html="true">' . $price_property['name_gr'] . '</div></span>
                    <div class="range_slider_wrap default_width range-slide">
                        <div class="leftLabel white range-cell">1</div>
                       <div class="range-cell middel">
                        <div class="jsSlider"></div>
                        </div>
                        <div class="rightLabel white range-cell">1</div>
                    </div>
                </div>';
        foreach ($result as $prop) {
            if ($prop->prop_id != '') {
                $property_ids[] = $prop->prop_id;
                $str.= '<div class="range_slider default_width mb10">' .
                        '<span><div class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop->prop_desc_gr) . '" data-html="true">' . $prop->prop_name_gr . '</div></span>' .
                        '<div class="range_slider_wrap default_width">' .
                        '<div class="leftLabel white"></div>' .
                        '<div class="nstSlider" data-range_min="1" data-range_max="10" data-cur_min="1" data-cur_max="5">' .
                        '<div class="bar"></div>' .
                        '<div class="leftGrip"></div>' .
                        '<div class="rightGrip"></div>' .
                        '</div>' .
                        '<div class="rightLabel white">10</div>' .
                        '</div>' .
                        '</div>';
                $new_str.='<div class="range_slider default_width mb10">
                    <span><div class="theme_color mb10" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop->prop_desc_gr) . '" data-html="true">' . $prop->prop_name_gr . '</div></span>
                    <div class="range_slider_wrap default_width range-slide">
                        <div class="leftLabel white range-cell">1</div>
                        <div class="range-cell middel">
                        <div class="jsSlider"></div>
                        </div>
                        <div class="rightLabel white range-cell">1</div>
                    </div>
                </div>';
            }
        }

        //-- Get secondary properties
        $secondary_properties = $this->categories_model->get_secproperties_by_catandtype($category_id, $type);
        $secondary_property_ids = array();
        $sec_string = '<ul class="mb10">';
        foreach ($secondary_properties as $prop) {
            $secondary_property_ids[] = $prop['id'];
            $sec_string.= '<li><a href="javascript:void(0);" class="sec_properties" id="sec_' . $prop['id'] . '" data-toggle="tooltip" data-placement="bottom" title="' . htmlentities($prop['desc_gr']) . '" onclick="sec_properties_click(this)" data-id="' . $prop['id'] . '" data-html="true">' . $prop['name_gr'] . '</a></li>';
        }
        $sec_string.='</ul>';

        $resultArr = array(
            'cat_image' => CATEGORIES_IMG . $result[0]->cat_image,
            'property_ids' => implode(",", $property_ids),
            'type' => $result[0]->type,
            'response' => $str,
            'new_slider' => $new_str,
            'secondary_property_ids' => implode(",", $secondary_property_ids),
            'secondary_properties' => $sec_string,
            'allowed_points' => (5 * count($property_ids)),
            'calculated_allowed_points' => ((5 * count($property_ids)) + count($property_ids))
        );
        echo json_encode($resultArr);
        exit;
    }

}

/* End of file Categories.php */
/* Location: ./application/controllers/Categories.php */