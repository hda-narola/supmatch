<?php

/**
 * Home Controller - Landing Page of Supmatch
 * @author SG
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('orders_model'));
    }

    /**
     * @author SG
     */
    public function place_order() {
        if ($this->input->post()) {
            if (!empty($_SESSION['session_user_id'])) {
                $today = date('Y-m-d H:i:s');
                $data_arr = array();
                $data_arr['user_id'] = $_SESSION['session_user_id'];
                $data_arr['sup_id'] = $this->input->post('sup_id');
                $data_arr['quantity'] = 1;
                if ($this->input->post('qty')) {
                    $data_arr['quantity'] = $this->input->post('qty');
                }
                $data_arr['order_date'] = $today;
                $data_arr['created'] = $today;
                $data_arr['modified'] = $today;
                $insert = $this->orders_model->insert_category($data_arr);
                if ($insert > 0) {
                    echo 'success';
                } else {
                    echo 'fail';
                }
            } else {
                echo 'success';
            }
        }
        exit;
    }

    /**
     * @author SG
     */
    public function thank_you() {
        $data['title'] = 'Supmatch | Thank You';
        $data['user_session_id'] = $this->checkLogin('ID');

        if ($this->input->post()) {
            $user_session_id = $this->checkLogin('ID');
            $user = "Guest";
            if ($user_session_id != '') {
                $user = $this->checkLogin('N') . ' (' . $this->checkLogin('E') . ')';
            }
            $msg = "Hi New feedback received from <b>" . $user . "</b> on suppliment purchase. Following are details for the same.<br/>";
            $msg .= $this->input->post('thank_you_content');
            $email_array = array(
                'mail_type' => 'html',
                'from_mail_id' => $this->config->item('smtp_user'),
                'from_mail_name' => '',
//                'to_mail_id' => 'sg@narola.email',
                'to_mail_id' => FEEDBACK_EMAIL,
                'cc_mail_id' => '',
                'subject_message' => 'Supmatch | Feedback on purchase',
                'body_messages' => $msg
            );
            $this->users_model->common_email_send($email_array);
            $this->session->set_flashdata('success', 'Your suggestion has been mailed to Administrator successfully!');
            redirect('home');
        }

        $this->template->load('frontend', 'orders/thankyou', $data);
    }

    /**
     * @author SG
     */
    public function get_user_orders() {
        if ($this->input->post()) {
            if ($this->input->post('order_sort_option')) {
                if (!empty($_SESSION['session_user_id'])) {
                    $order_condition = array('o.user_id' => $_SESSION['session_user_id']);
                    $orders_user = $this->orders_model->get_all_orders(TBL_ORDERS, $order_condition, $this->input->post('order_sort_option'));
//                    p($orders_user); exit;
                    $res_str = '';
                    foreach ($orders_user as $order):
                        $images_arr = explode(',', $order['images']);

                        $res_str .= '<div class="gt_product_list mb30">';
                        $res_str .= '<div class="gt_product_img">';
                        $res_str .= '<img src="uploads/supplements/' . $images_arr[0] . '" alt="">';
                        $res_str .= '</div>';
                        $res_str .= '<div class="gt_product_price">';
                        $res_str .= '<h5 class="theme_color mb10">Product name</h5>';
                        $res_str .= '<h4 class="black">' . $order['name_gr'] . '</h4>';
                        $res_str .= '</div>';
                        $res_str .= '<div class="gt_product_price">';
                        $res_str .= '<h5 class="theme_color mb10">Preis:</h5>';
                        $res_str .= '<h4 class="black">' . $order['price'] . '€</h4>';
                        $res_str .= '</div>';
                        $res_str .= '<div class="gt_product_cart_btn">';
                        $res_str .= '<a href="' . $order['amazon_link'] . '" target="_blnk" data-toggle="tooltip" data-placement="bottom" title="Lieber Nutzer, über unseren Referallink zu bestellen, unterstützt diesen Service und gibt uns die Möglichkeit unsere Leistung für euch stets zu verbessern. Vielen Dank!">Jetzt kaufen!</a>';
                        $res_str .= '</div>';
                        $res_str .= '<span class="date">' . date('d-m-Y', strtotime($order['order_date'])) . '</span>';
                        $res_str .= '</div>';
                    endforeach;

                    echo $res_str;
                    exit;
                }
            }
        }
    }

}
