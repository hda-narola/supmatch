<?php

/**
 * PAGES Controller - Display Pages
 * @author pav
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function index() {
        
    }

    public function hows_work() {
        $data['title'] = 'Supmatch | How its work';
        $data['user_session_id'] = $this->checkLogin('ID');
        $this->template->load('frontend', 'pages/hows_work', $data);
    }

    public function about_us() {
        $data['title'] = 'Supmatch | About Us';
        $data['user_session_id'] = $this->checkLogin('ID');
        $this->template->load('frontend', 'pages/about_us', $data);
    }

    /**
     * agbs page
     * @author KU
     */
    public function agbs() {
        $data['title'] = 'AGB';
        $data['user_session_id'] = $this->checkLogin('ID');
        $this->template->load('frontend', 'pages/supmatch_agb', $data);
    }

    /**
     * impressum page
     * @author KU
     */
    public function impressum() {
        $data['title'] = 'Supmatch | Impressum';
        $data['user_session_id'] = $this->checkLogin('ID');
        $this->template->load('frontend', 'pages/supmatch_impressum', $data);
    }

    /**
     * datenschutzerklarung page
     * @author KU
     */
    public function datenschutzerklarung() {
        $data['title'] = utf8_encode('Datenschutzerkl�rung');
        $data['user_session_id'] = $this->checkLogin('ID');
        $this->template->load('frontend', 'pages/supmatch_datenschutzerklarung', $data);
    }

}

/* End of file Pages.php */
/* Location: ./application/controllers/Pages.php */