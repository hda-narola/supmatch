<?php

/**
 * Goals Controller - Manage goals
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Goals extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('goals_model', 'categories_model'));
    }

    /**
     * Load view of goals list
     * */
    public function index() {
        $data['title'] = 'Supmatch | Goals';
        $data['goals'] = $this->goals_model->get_allgoals();
        $this->template->load('admin', 'admin/goals/list', $data);
    }

    /**
     * Add/Edit goal details
     */
    public function edit() {
        $goal_id = $this->uri->segment(4);
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name', 'trim|required');

        $data['table'] = TBL_GOALS;

        if (is_numeric($goal_id)) {
            $check_goal = $this->goals_model->get_goal($goal_id);
            if ($check_goal) {
                $data['categories'] = $this->categories_model->get_categoriesforgoal($goal_id);
                $data['goal_data'] = $check_goal;
                $data['title'] = 'Supmatch | Edit Goal';
                $data['heading'] = 'Edit ' . $check_goal['name_en'] . ' Goal';
            } else {
                show_404();
            }
        } else {
            $data['categories'] = $this->categories_model->get_categoriesforgoal();
            $data['heading'] = 'Add Goal';
            $data['title'] = 'Supmatch | Add Goal';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {
            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'status' => $this->input->post('status') == 'on' ? 'active' : 'inactive',
                'modified' => date('Y-m-d H:i:s')
            );
            $category_ids = $this->input->post('category_ids');

            if (is_numeric($goal_id)) {
                $this->goals_model->common_insert_update('update', TBL_GOALS, $data_array, array('id' => $goal_id));
                $im_category_ids = implode(",", $category_ids);
                if ($im_category_ids != $check_goal['category_ids']) {
                    $this->goals_model->delete_goal_category_links($goal_id);
                    $links_arr = array();
                    foreach ($category_ids as $key => $category_id) {
                        $links_arr[$key]['category_id'] = $category_id;
                        $links_arr[$key]['goal_id'] = $goal_id;
                        $links_arr[$key]['modified'] = date('Y-m-d H:i:s');
                    }
                    $this->goals_model->insert_goal_category_links($links_arr);
                }
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" goal updated successfully!');
            } else { //-- If goal id is not present then add new goal details
                $goal_id = $this->goals_model->insert_goal($data_array);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" goal added successfully!');
                $links_arr = array();
                foreach ($category_ids as $key => $category_id) {
                    $links_arr[$key]['category_id'] = $category_id;
                    $links_arr[$key]['goal_id'] = $goal_id;
                    $links_arr[$key]['modified'] = date('Y-m-d H:i:s');
                }
                $this->goals_model->insert_goal_category_links($links_arr);
            }


            redirect('admin/goals');
        }

        $this->template->load('admin', 'admin/goals/form', $data);
    }

    /**
     * Delete Goal 
     * @param int $goal_id - Goal id
     */
    public function delete($goal_id) {
        $check_goal = $this->goals_model->get_goal($goal_id);
        if ($check_goal) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->goals_model->common_insert_update('update', TBL_GOALS, $update_array, array('id' => $goal_id));

            $this->session->set_flashdata('success', '"' . $check_goal['name_en'] . '" goal deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/goals');
    }

    /**
     * View Goal details
     * @param int $goal_id Goal Id
     */
    public function view($goal_id = NULL) {
        if (is_numeric($goal_id)) {
            $check_goal = $this->goals_model->get_goal($goal_id);
            if ($check_goal) {
                $data['goal_data'] = $check_goal;
                $data['title'] = 'Supmatch | View Goal';
                $data['heading'] = 'View Goal';
                $this->template->load('admin', 'admin/goals/view', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Get goals for data table
     * */
    public function get_goals() {
        $final['recordsTotal'] = $this->goals_model->get_all_goals('count');
        $final['redraw'] = 1;
        $final['recordsFiltered'] = $final['recordsTotal'];
        $goals = $this->goals_model->get_all_goals();
        $start = $this->input->get('start') + 1;

        foreach ($goals as $key => $val) {
            $goals[$key] = $val;
            $goals[$key]['sr_no'] = $start++;
            $goals[$key]['created'] = date('d M Y', strtotime($val['created']));
        }

        $final['data'] = $goals;
        echo json_encode($final);
    }

}
