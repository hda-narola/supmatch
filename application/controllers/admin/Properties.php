<?php

/**
 * Properties Controller - Manage primary and secondary property
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('properties_model'));
    }

    /**
     * Load view of primary properties list
     * */
    public function index() {
        $data['title'] = 'Supmatch | Primary properties';
        $data['heading'] = "Primary properties";
        $data['add_heading'] = "Add Primary property";
        $data['link'] = "properties";
        $data['price_property'] = $this->properties_model->get_price_property();
        $data['properties'] = $this->properties_model->get_allproperties(1);
        $this->template->load('admin', 'admin/properties/list', $data);
    }

    /**
     * Load view of secondary properties list
     * */
    public function secondary() {
        $data['title'] = 'Supmatch | Secondary Properties';
        $data['heading'] = "Secondary properties";
        $data['add_heading'] = "Add Secondary property";
        $data['link'] = "secondary_properties";
        $data['properties'] = $this->properties_model->get_allproperties(2);
        $this->template->load('admin', 'admin/properties/list', $data);
    }

    /**
     * Add/Edit Primary property details
     */
    public function edit() {
        $property_id = $this->uri->segment(4);
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_en', 'Description', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_gr', 'Description', 'trim|required');

        $data['table'] = TBL_PROPERTIES;
        $data['link_title'] = "Primary";
        $data['link'] = "properties";

        if (is_numeric($property_id)) {
            $check_property = $this->properties_model->get_property($property_id);
            if ($check_property) {
                $data['property_data'] = $check_property;
                $data['title'] = 'Supmatch | Edit Property';
                $data['heading'] = 'Edit ' . $check_property['name_en'] . ' Property';
            } else {
                show_404();
            }
        } else {
            $data['heading'] = 'Add Primary Property';
            $data['title'] = 'Supmatch | Add Primary Property';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {
            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'desc_en' => trim($this->input->post('desc_en')),
                'desc_gr' => trim($this->input->post('desc_gr')),
                'status' => $this->input->post('status') == 'on' ? 'active' : 'inactive',
                'type' => 1,
                'modified' => date('Y-m-d H:i:s')
            );
            if (is_numeric($property_id)) {
                $this->properties_model->common_insert_update('update', TBL_PROPERTIES, $data_array, array('id' => $property_id));
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" property updated successfully!');
            } else { //-- If property id is not present then add new property details
                $this->properties_model->common_insert_update('insert', TBL_PROPERTIES, $data_array);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" property added successfully!');
            }
            redirect('admin/properties');
        }

        $this->template->load('admin', 'admin/properties/form', $data);
    }

    /**
     * Add/Edit Secondary Property Details
     */
    public function secondary_edit() {
        $property_id = $this->uri->segment(4);
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_en', 'Description', 'trim|required');
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_gr', 'Description', 'trim|required');

        $data['table'] = TBL_PROPERTIES;
        $data['link_title'] = "Secondary";
        $data['link'] = "secondary_properties";

        if (is_numeric($property_id)) {
            $check_property = $this->properties_model->get_property($property_id);
            if ($check_property) {
                $data['property_data'] = $check_property;
                $data['title'] = 'Supmatch | Edit Secondary Property';
                $data['heading'] = 'Edit ' . $check_property['name_en'] . ' Property';
            } else {
                show_404();
            }
        } else {
            $data['heading'] = 'Add Secondary Property';
            $data['title'] = 'Supmatch | Add Secondary Property';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {
            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'desc_en' => trim($this->input->post('desc_en')),
                'desc_gr' => trim($this->input->post('desc_gr')),
                'status' => $this->input->post('status') == 'on' ? 'active' : 'inactive',
                'type' => 2,
                'modified' => date('Y-m-d H:i:s')
            );
            if (is_numeric($property_id)) {
                $this->properties_model->common_insert_update('update', TBL_PROPERTIES, $data_array, array('id' => $property_id));
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" property updated successfully!');
            } else { //-- If property id is not present then add new property details
                $this->properties_model->common_insert_update('insert', TBL_PROPERTIES, $data_array);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" property added successfully!');
            }
            redirect('admin/secondary_properties');
        }

        $this->template->load('admin', 'admin/properties/form', $data);
    }

    /**
     * Delete Property 
     * @param int $property_id - Property id
     */
    public function secondary_delete($property_id) {
        $check_property = $this->properties_model->get_property($property_id);
        if ($check_property) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->properties_model->common_insert_update('update', TBL_PROPERTIES, $update_array, array('id' => $property_id));

            $this->session->set_flashdata('success', '"' . $check_property['name_en'] . '" property deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/secondary_properties');
    }

    /**
     * Delete Property 
     * @param int $property_id - Property id
     */
    public function delete($property_id) {
        $check_property = $this->properties_model->get_property($property_id);
        if ($check_property) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->properties_model->common_insert_update('update', TBL_PROPERTIES, $update_array, array('id' => $property_id));

            $this->session->set_flashdata('success', '"' . $check_property['name_en'] . '" property deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/properties');
    }

    /**
     * View Property details
     * @param int $property_id Property Id
     */
    public function view($property_id = NULL) {
        if (is_numeric($property_id)) {
            $check_property = $this->properties_model->get_property($property_id);
            if ($check_property) {
                $data['property_data'] = $check_property;
                $data['title'] = 'Supmatch | View Property';
                $data['heading'] = 'View Property';
                $this->template->load('admin', 'admin/properties/view', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Get properties for data table
     * */
    public function get_properties() {
        $final['recordsTotal'] = $this->properties_model->get_all_properties('count');
        $final['redraw'] = 1;
        $final['recordsFiltered'] = $final['recordsTotal'];
        $properties = $this->properties_model->get_all_properties();
        $start = $this->input->get('start') + 1;

        foreach ($properties as $key => $val) {
            $properties[$key] = $val;
            $properties[$key]['sr_no'] = $start++;
            $properties[$key]['created'] = date('d M Y', strtotime($val['created']));
        }

        $final['data'] = $properties;
        echo json_encode($final);
    }

    public function edit_priceproperty() {
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_en', 'Description', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_gr', 'Description', 'trim|required');

        $data['table'] = TBL_PRICE_PROPERTY;
        $data['link_title'] = "Primary";
        $data['link'] = "properties";

        $check_property = $this->properties_model->get_price_property();
        if ($check_property) {
            $data['property_data'] = $check_property;
            $data['title'] = 'Supmatch | Edit Property';
            $data['heading'] = 'Edit ' . $check_property['name_en'] . ' Property';
        } else {
            show_404();
        }
        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {
            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'desc_en' => trim($this->input->post('desc_en')),
                'desc_gr' => trim($this->input->post('desc_gr')),
            );
            $this->properties_model->common_insert_update('update', TBL_PRICE_PROPERTY, $data_array, array('id' => 1));
            $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" property updated successfully!');
            redirect('admin/properties');
        }

        $this->template->load('admin', 'admin/properties/form', $data);
    }

}
