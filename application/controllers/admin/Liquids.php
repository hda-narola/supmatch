<?php

/**
 * Liquids Controller - Manage liquids
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Liquids extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('liquid_model');
    }

    /**
     * Load view of liquid list
     * */
    public function index() {
        $data['title'] = 'Supmatch | Liquids';
        $data['liquids'] = $this->liquid_model->get_allliquids();
        $this->template->load('admin', 'admin/liquids/list', $data);
    }

    /**
     * Get liquids for data table
     * */
    public function get_liquids() {
        $final['recordsTotal'] = $this->liquid_model->get_all_liquids('count');
        $final['redraw'] = 1;
        $final['recordsFiltered'] = $final['recordsTotal'];
        $liquids = $this->liquid_model->get_all_liquids();
        $start = $this->input->get('start') + 1;

        foreach ($liquids as $key => $val) {
            $liquids[$key] = $val;
            $liquids[$key]['sr_no'] = $start++;
            $liquids[$key]['created'] = date('d M Y', strtotime($val['created']));
        }

        $final['data'] = $liquids;
        echo json_encode($final);
    }

    /**
     * Add/Edit Liquid Details
     */
    public function edit() {
        $liquid_id = $this->uri->segment(4);
        $data['table'] = TBL_LIQUIDS;
        $this->form_validation->set_rules('name_en', 'Name (English)', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name (German)', 'trim|required');

        if (is_numeric($liquid_id)) {
            $check_liquid = $this->liquid_model->get_liquid($liquid_id);
            if ($check_liquid) {
                $data['liquid_data'] = $check_liquid;
                $data['title'] = 'Supmatch | Edit Liquid';
                $data['heading'] = 'Edit ' . $check_liquid['name_en'] . ' Liquid';
            } else {
                show_404();
            }
        } else {
            $data['heading'] = 'Add Liquid';
            $data['title'] = 'Supmatch | Add Liquid';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {

            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'modified' => date('Y-m-d H:i:s')
            );
            if (is_numeric($liquid_id)) {
                $this->liquid_model->common_insert_update('update', TBL_LIQUIDS, $data_array, 'id=' . $liquid_id);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" liquid updated successfully!');
            } else { //-- If liquid id is not present then add new liquid details
//                $data_array['created'] = date('Y-m-d H:i:s');
                $this->liquid_model->common_insert_update('insert', TBL_LIQUIDS, $data_array);

                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" liquid added successfully!');
            }
            redirect('admin/liquids');
        }

        $this->template->load('admin', 'admin/liquids/form', $data);
    }

    /**
     * Delete Liquid 
     * @param int $liquid_id - Liquid id
     */
    public function delete($liquid_id) {
        $check_liquid = $this->liquid_model->get_liquid($liquid_id);
        if ($check_liquid) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->liquid_model->common_insert_update('update', TBL_LIQUIDS, $update_array, 'id=' . $liquid_id);

            $this->session->set_flashdata('success', '"' . $check_liquid['name_en'] . '" liquid deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/liquids');
    }

    /**
     * View Liquid details
     * @param int $liquid_id Liquid Id
     */
    public function view($liquid_id = NULL) {
        if (is_numeric($liquid_id)) {
            $check_liquid = $this->liquid_model->get_liquid($liquid_id);
            if ($check_liquid) {
                $data['liquid_data'] = $check_liquid;
                $data['title'] = 'Supmatch | View Liquid';
                $data['heading'] = 'View Liquid';
                $this->template->load('admin', 'admin/liquids/view', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

}
