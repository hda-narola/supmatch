<?php

/**
 * Flavours Controller - Manage flavours
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Flavours extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('flavour_model');
    }

    /**
     * Load view of flavour list
     * */
    public function index() {
        $data['title'] = 'Supmatch | flavours';
        $data['flavours'] = $this->flavour_model->get_allflavours();
        $this->template->load('admin', 'admin/flavours/list', $data);
    }

    /**
     * Get flavours for data table
     * */
    public function get_flavours() {
        $final['recordsTotal'] = $this->flavour_model->get_all_flavours('count');
        $final['redraw'] = 1;
        $final['recordsFiltered'] = $final['recordsTotal'];
        $flavours = $this->flavour_model->get_all_flavours();
        $start = $this->input->get('start') + 1;

        foreach ($flavours as $key => $val) {
            $flavours[$key] = $val;
            $flavours[$key]['sr_no'] = $start++;
            $flavours[$key]['created'] = date('d M Y', strtotime($val['created']));
        }

        $final['data'] = $flavours;
        echo json_encode($final);
    }

    /**
     * Add/Edit Luquid Details
     */
    public function edit() {
        $flavour_id = $this->uri->segment(4);
        $data['table'] = TBL_FLAVOURS;
        $this->form_validation->set_rules('name_en', 'Name (English)', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name (German)', 'trim|required');

        if (is_numeric($flavour_id)) {
            $check_flavour = $this->flavour_model->get_flavour($flavour_id);
            if ($check_flavour) {
                $data['flavour_data'] = $check_flavour;
                $data['title'] = 'Supmatch | Edit Flavour';
                $data['heading'] = 'Edit ' . $check_flavour['name_en'] . ' Flavour';
            } else {
                show_404();
            }
        } else {
            $data['heading'] = 'Add Flavour';
            $data['title'] = 'Supmatch | Add Flavour';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {

            $data_array = array(
                'name_en' => trim($this->input->post('name_en')),
                'name_gr' => trim($this->input->post('name_gr')),
                'modified' => date('Y-m-d H:i:s')
            );
            if (is_numeric($flavour_id)) {
                $this->flavour_model->common_insert_update('update', TBL_FLAVOURS, $data_array, 'id=' . $flavour_id);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" flavour updated successfully!');
            } else { //-- If flavour id is not present then add new flavour details
                $this->flavour_model->common_insert_update('insert', TBL_FLAVOURS, $data_array);
                $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" flavour added successfully!');
            }
            redirect('admin/flavours');
        }

        $this->template->load('admin', 'admin/flavours/form', $data);
    }

    /**
     * Delete Flavour 
     * @param int $flavour_id - Flavour id
     */
    public function delete($flavour_id) {
        $check_flavour = $this->flavour_model->get_flavour($flavour_id);
        if ($check_flavour) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->flavour_model->common_insert_update('update', TBL_FLAVOURS, $update_array, 'id=' . $flavour_id);

            $this->session->set_flashdata('success', '"' . $check_flavour['name_en'] . '" flavour deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/flavours');
    }

    /**
     * View Flavour details
     * @param int $flavour_id Flavour Id
     */
    public function view($flavour_id = NULL) {
        if (is_numeric($flavour_id)) {
            $where = 'b.id = ' . $this->db->escape($business_id);
            $check_flavour = $this->flavour_model->get_flavour($flavour_id);
            if ($check_flavour) {
                $data['flavour_data'] = $check_flavour;
                $data['title'] = 'Supmatch | View Flavour';
                $data['heading'] = 'View Flavour';
                $this->template->load('admin', 'admin/flavours/view', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Check unique flavour for english
     * Called throught ajax
     */
    public function checkUniqueFlavourEn($id = NULL) {
        $name_en = trim($this->input->get('name_en'));
        $condition = 'name_en="' . $name_en . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $user = $this->flavour_model->check_unique_name($condition);
        if ($user) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    /**
     * Check unique flavour for german
     * Called throught ajax
     */
    public function checkUniqueFlavourGr($id = NULL) {
        $name_gr = trim($this->input->get('name_gr'));
        $condition = 'name_gr="' . $name_gr . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $user = $this->flavour_model->check_unique_name($condition);
        if ($user) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

}
