<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ranks extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ranks_model');
        $this->load->library('upload');
    }

    public function index() {
        
    }

    /**
     * This function used to display rank's data at admin side. 
     * @author pav
     * */
    public function display_ranks() {
        $data['title'] = 'Supmatch | Ranks';
        $condition = array('status!=' => 'deleted');
        $sort = array(
            array('field' => 'rank', 'type' => 'ASC')
        );
        $data['resultArr'] = $this->ranks_model->get_all_details(TBL_RANK_DETAILS, $condition, $sort);
        $this->template->load('admin', 'admin/ranks/display_ranks', $data);
    }

    /**
     * This function used to add / edit rank's data at admin side. 
     * @param Integer $id
     * @author pav
     * */
    public function add_ranks($id = '') {
        $rank_id = $id;
        if (is_numeric($rank_id)) {
            $rankdata = $this->ranks_model->get_all_rank_details_by_id($rank_id)->result_array();
            if ($rankdata) {
                $data['rankdata'] = $rankdata;
                $data['title'] = 'Supmatch | Edit Rank';
            } else {
                show_404();
            }
        } else {
            $data['title'] = 'Supmatch | Add Rank';
        }

        if ($this->add_ranks_validate()) {
            $dataArr = array(
                'name' => $this->input->post('txt_name'),
                'rank' => $this->input->post('txt_rank'),
                'requirement' => $this->input->post('txt_requirement_text'),
                'reward' => $this->input->post('txt_reward'),
                'color' => $this->input->post('txt_color'),
                'required_reviews' => $this->input->post('txt_req_reviews'),
                'required_likes' => $this->input->post('txt_req_likes'),
                //'required_dislikes'	=> $this->input->post('txt_req_dislikes'),
                'status' => $this->input->post('txt_status') == 'on' ? 'active' : 'inactive'
            );

            if ($_FILES['txt_icon']['name'] != '') {

                $extension = explode('/', $_FILES['txt_icon']['type']);
                $randname = uniqid() . time() . '.' . end($extension);

                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                $config['max_size'] = 10000;
                $config['upload_path'] = './uploads/ranks';
                $config['file_name'] = $randname;

                $this->upload->initialize($config);
                if ($this->upload->do_upload('txt_icon')) {
                    $thumbnail = $this->upload->data();
                    $icon = $thumbnail['file_name'];
                    $dataArr['icon'] = $icon;
                } else {
                    $thumbnail = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $logoDetails);
                    redirect('admin/add_ranks');
                }
            } else {
                $icon = '';
            }
            if (is_numeric($rank_id)) {
                $condition = array('id' => $rank_id);
                $this->ranks_model->common_insert_update('update', TBL_RANK_DETAILS, $dataArr, $condition);
                $this->session->set_flashdata('success', 'Rank\'s data has been updated successfully.');
            } else {
                $this->ranks_model->common_insert_update('insert', TBL_RANK_DETAILS, $dataArr, $condition);
                $this->session->set_flashdata('success', 'New Rank has been added successfully.');
            }
            redirect('admin/ranks');
        }
        $this->template->load('admin', 'admin/ranks/add_ranks', $data);
    }

    /**
     * This function used to edit rank's data at admin side. 
     * @param Integer $id
     * @author pav
     * */
    public function edit_ranks() {
        $this->add_ranks($this->uri->segment(3));
    }

    /**
     * This function used to check validation of add / edit rank's form. 
     * @return Bollean
     * @author pav
     * */
    public function add_ranks_validate() {
        $this->form_validation->set_rules('txt_name', 'Name', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('txt_requirement_text', 'Requirement Text', 'trim|required');
        $this->form_validation->set_rules('txt_reward', 'Reward', 'trim|required');
        $this->form_validation->set_rules('txt_color', 'Color', 'trim|required');
        $this->form_validation->set_rules('txt_req_reviews', 'Required Reviews No.', 'trim|required');
        $this->form_validation->set_rules('txt_req_likes', 'Required Likes No.', 'trim|required');
        //$this->form_validation->set_rules('txt_req_dislikes', 'Required Dislikes No.', 'trim|required');
        return $this->form_validation->run();
    }

    /**
     * This function used to delete rank's data at admin side. 
     * @param Integer $rank_id
     * @author pav
     * */
    public function delete_ranks($rank_id) {
        $rank_data = $this->ranks_model->get_all_rank_details_by_id($rank_id)->result_array();
        if ($rank_data) {
            $update_array = array(
                'status' => 'deleted'
            );
            $condition = array('id' => $rank_id);
            $this->ranks_model->common_insert_update('update', TBL_RANK_DETAILS, $update_array, $condition);
            $this->session->set_flashdata('success', 'Rank\'s data has been deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/ranks');
    }

    /**
     * Check unique rank name
     * Called throught ajax
     * */
    public function checkUniqueRankName($id = NULL) {
        $rank_name = trim($this->input->get('txt_name'));
        $condition = 'name="' . $rank_name . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $result = $this->ranks_model->check_unique_name(TBL_RANK_DETAILS, $condition);
        if ($result) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    /**
     * Called throught ajax - Check unique rank
     * @author KU
     * @param type $id
     */
    public function checkUniqueRank($id = NULL) {
        $rank = trim($this->input->get('txt_rank'));
        $condition = 'rank=' . $rank;
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $result = $this->ranks_model->check_unique_name(TBL_RANK_DETAILS, $condition);
        if ($result) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    public function test() {
        $default_rank = $this->ranks_model->get_default_rank();
        p($default_rank);
    }

}

/* End of file Ranks.php */
/* Location: ./application/controllers/admin/Ranks.php */