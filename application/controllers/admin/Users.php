<?php

/**
 * Users Controller - Manage Users
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('ranks_model');
        $this->load->library('upload');
        $GLOBALS['unique_uname'] = $GLOBALS['unique_email'] = '';
    }

    public function index() {
        
    }

    /**
     * This function used to display user's data at admin side. 
     * @author pav
     * */
    public function display_users() {
        $data['title'] = 'Supmatch | Users';
        $data['usersArr'] = $this->users_model->get_all_users_details();
        $this->template->load('admin', 'admin/users/display_users', $data);
    }

    /**
     * This function used to add / edit user's data at admin side. 
     * @param Integer $id
     * @author pav
     * */
    public function add_users($id = '') {
        $user_id = $id;
        $exist_pass = '';
        if (is_numeric($user_id)) {
            $userdata = $this->users_model->get_all_users_details_by_id($user_id)->result_array();
            $exist_pass = $userdata[0]['password'];
            if ($userdata) {
                $data['userdata'] = $userdata;
                $data['title'] = 'Supmatch | Edit User';
            } else {
                show_404();
            }
        } else {
            $cstrong = '';
            $GLOBALS['unique_uname'] = '|is_unique[users.nickname]';
            $GLOBALS['unique_email'] = '|is_unique[users.email]';
            $data['random_string'] = bin2hex(openssl_random_pseudo_bytes(10, $cstrong));
            $data['title'] = 'Supmatch | Add User';
        }

        if ($this->add_users_validate()) {
            $verification_code = rand(100000, 999999);
            $dataArr = array(
                'role_id' => 2,
                'nickname' => $this->input->post('txt_username'),
                'email' => $this->input->post('txt_email'),
                'password' => $this->input->post('txt_password') == $exist_pass ? $exist_pass : md5($this->input->post('txt_password')),
                'status' => $this->input->post('txt_status') == 'on' ? 'active' : 'inactive',
                'verification_code' => $verification_code
            );
            if ($_FILES['txt_profile_pic']['name'] != '') {
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                $config['max_size'] = 10000;
                $config['upload_path'] = './uploads/users';
                $this->upload->initialize($config);
                if ($this->upload->do_upload('txt_profile_pic')) {
                    $thumbnail = $this->upload->data();
                    $profile_pic = $thumbnail['file_name'];
                    $dataArr['image'] = $profile_pic;
                } else {
                    $thumbnail = $this->upload->display_errors();
                    $this->set->flashdata('error', $logoDetails);
                    redirect('admin/add_users');
                }
            } else {
                $profile_pic = '';
            }

            if (is_numeric($user_id)) {
                $condition = array('id' => $user_id);
                $this->users_model->common_insert_update('update', TBL_USERS, $dataArr, $condition);
                $this->session->set_flashdata('success', 'User\'s data has been updated successfully.');
            } else {
                $default_rank = $this->ranks_model->get_default_rank();
                if ($default_rank) {
                    $dataArr['rank_id'] = $default_rank['id'];
                }
                $this->users_model->common_insert_update('insert', TBL_USERS, $dataArr);
                $href = site_url() . 'activate/' . urlencode(base64_encode($this->input->post('txt_email'))) . '/' . base64_encode($verification_code);
                $msg = "<a href='" . $href . "'>Click here</a> for verify your account";
                $email_array = array(
                    'mail_type' => 'html',
                    'from_mail_id' => $this->config->item('smtp_user'),
                    'from_mail_name' => '',
                    'to_mail_id' => $this->input->post('txt_email'),
                    'cc_mail_id' => '',
                    'subject_message' => 'Supmatch | Account Verification',
                    'body_messages' => $msg
                );
                $this->users_model->common_email_send($email_array);
                $this->session->set_flashdata('success', 'New User has been added successfully.');
            }
            redirect('admin/users');
        }
        $this->template->load('admin', 'admin/users/add_users', $data);
    }

    /**
     * This function used to check validation of add / edit user's form. 
     * @return Bollean
     * @author pav
     * */
    public function add_users_validate() {
        $this->form_validation->set_rules('txt_username', 'Username', 'trim|required|min_length[5]' . $GLOBALS['unique_uname']);
        $this->form_validation->set_rules('txt_email', 'Email', 'trim|required' . $GLOBALS['unique_email']);
        $this->form_validation->set_rules('txt_password', 'Password', 'trim|required|min_length[5]');
        return $this->form_validation->run();
    }

    /**
     * This function used to edit user's data at admin side. 
     * @param Integer $id
     * @author pav
     * */
    public function edit_users() {
        $this->add_users($this->uri->segment(3));
    }

    /**
     * This function used to delete user's data at admin side. 
     * @param Integer $user_id
     * @author pav
     * */
    public function delete_users($user_id) {
        $user_data = $this->users_model->get_all_users_details_by_id($user_id)->result_array();
        if ($user_data) {
            $update_array = array(
                'status' => 'deleted'
            );
            $condition = array('id' => $user_id);
            $this->users_model->common_insert_update('update', TBL_USERS, $update_array, $condition);
            $this->session->set_flashdata('success', 'User\'s data has been deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/users');
    }

    /**
     * This function used to check Unique User Name at the time of user's add at admin side
     * @param Integer $id
     * @author pav
     * */
    public function checkUniqueUserName($id = NULL) {
        $user_name = trim($this->input->get('txt_username'));
        $condition = 'nickname="' . $user_name . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $result = $this->users_model->check_unique_name(TBL_USERS, $condition);
        if ($result) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    /**
     * This function used to check Unique email at the time of user's add at admin side
     * @param Integer $id
     * @author pav
     * */
    public function checkUniqueEmail($id = NULL) {
        $email = trim($this->input->get('txt_email'));
        $condition = 'email="' . $email . '"';
        if ($id != '') {
            $condition.=" AND id!=" . $id;
        }
        $result = $this->users_model->check_unique_name(TBL_USERS, $condition);
        if ($result) {
            echo "false";
        } else {
            echo "true";
        }
        exit;
    }

    /**
     * This function used to activate user's account after registration
     * @author pav
     * */
    public function account_activation() {
        $email = base64_decode(urldecode($this->uri->segment(2)));
        $verif_code = base64_decode($this->uri->segment(3));
        $condition = array('u.email' => $email, 'u.verification_code' => $verif_code);
        $result = $this->users_model->account_activation($condition);
        if ($result == 1) {
            redirect(base_url());
        } else {
            redirect(base_url());
        }
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/admin/Users.php */