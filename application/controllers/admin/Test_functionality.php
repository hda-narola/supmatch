<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test_functionality extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function index() {
        
    }

    public function test_email() {
        $href = base_url() . 'activate/' . base64_encode(urlencode('pav@narola.email')) . '/' . base64_encode('123456');
        $msg = "<a href='" . $href . "'>Click here</a> for verify your account";
        $email_array = array(
            'mail_type' => 'html',
            'from_mail_id' => 'pav@narola.email',
            'from_mail_name' => '',
            'to_mail_id' => 'ku@narola.email',
            'cc_mail_id' => '',
            'subject_message' => 'Testing',
            'body_messages' => $msg
        );
        $result = $this->users_model->common_email_send($email_array);
        echo $result;
        die;
    }

}

/* End of file Test_functionality.php */
/* Location: ./application/controllers/admin/Test_functionality.php */