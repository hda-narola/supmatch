<?php

/**
 * Categories Controller - Manage Categories
 * @author KU
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('categories_model', 'liquid_model', 'properties_model'));
    }

    /**
     * Load view of categories list
     * */
    public function index() {
        $data['title'] = 'Supmatch | categories';
        $data['categories'] = $this->categories_model->get_allcategories();
        $this->template->load('admin', 'admin/categories/list', $data);
    }

    /**
     * Add/Edit Category Details
     */
    public function edit() {
        $category_id = $this->uri->segment(4);
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_en', 'Description', 'trim|required');
        $this->form_validation->set_rules('name_en', 'Name', 'trim|required');
        $this->form_validation->set_rules('desc_gr', 'Description', 'trim|required');

        $data['table'] = TBL_CATEGORIES;
        $data['liquids'] = $this->liquid_model->get_all_details(TBL_LIQUIDS, array('status' => 'active'))->result_array();
        $data['properties'] = $this->properties_model->get_all_details(TBL_PROPERTIES, array('type' => 1, 'status' => 'active'))->result_array();
        $data['secondary_properties'] = $this->properties_model->get_all_details(TBL_PROPERTIES, array('type' => 2, 'status' => 'active'))->result_array();

        if (is_numeric($category_id)) {
            $check_category = $this->categories_model->get_category($category_id);
            if ($check_category) {
                $data['category_data'] = $check_category;
                $data['title'] = 'Supmatch | Edit category';
                $data['heading'] = 'Edit ' . $check_category['name_en'] . ' category';
            } else {
                show_404();
            }
        } else {
            $data['heading'] = 'Add Category';
            $data['title'] = 'Supmatch | Add Category';
        }

        if ($this->form_validation->run() == FALSE) {
//            $this->form_validation->set_error_delimiters('<label class="validation-error-label">', '</label>');
        } else {
            $icon = '';
            if (is_numeric($category_id)) {
                $icon = $check_category['icon'];
            }
            $flag = 0;
            if ($_FILES['icon']['name'] != '') {
                $img_array = array('png', 'jpeg', 'jpg', 'PNG', 'JPEG', 'JPG');
                $exts = explode(".", $_FILES['icon']['name']);
                $name = $exts[0] . time() . "." . $exts[1];
                $name = "icon" . date("mdYhHis") . "." . end($exts);
                $config['upload_path'] = CATEGORIES_IMG;
                $config['allowed_types'] = implode("|", $img_array);
                $config['max_size'] = 10000;
                $config['file_name'] = $name;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('icon')) {
                    $flag = 1;
//                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    $data['icon_validation'] = $this->upload->display_errors();
                } else {
                    $file_info = $this->upload->data();
                    $icon = $file_info['file_name'];
                }
            }

            if ($flag != 1) {
                $data_array = array(
                    'icon' => $icon,
                    'name_en' => trim($this->input->post('name_en')),
                    'name_gr' => trim($this->input->post('name_gr')),
                    'desc_en' => trim($this->input->post('desc_en')),
                    'desc_gr' => trim($this->input->post('desc_gr')),
                    'type' => $this->input->post('category_type'),
                    'status' => $this->input->post('status') == 'on' ? 'active' : 'inactive',
                    'modified' => date('Y-m-d H:i:s')
                );
                if (is_numeric($category_id)) {
                    //--Unlink the previosly uploaded image if new image is uploaded
                    if ($_FILES['icon']['name'] != '') {
                        unlink(CATEGORIES_IMG . $check_category['icon']);
                    }
                    $this->categories_model->common_insert_update('update', TBL_CATEGORIES, $data_array, 'id=' . $category_id);
                    //-- Delete links from database
                    $this->categories_model->delete_links($category_id, TBL_CATEGORIES_LIQUIDS_LINK);
                    $this->categories_model->delete_links($category_id, TBL_CATEGORIES_PROPERTIES_LINK);
                    $this->categories_model->delete_links($category_id, TBL_CATEGORIES_SECONDARYPROPERTIES_LINK);

                    $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" category updated successfully!');
                } else { //-- If category id is not present then add new category details
                    $category_id = $this->categories_model->insert_category($data_array);
                    $this->session->set_flashdata('success', '"' . trim($this->input->post('name_en')) . '" category added successfully!');
                }
                $property_links_1 = array();
                $secondaryproperty_links_1 = array();
                $liquid_links_1 = array();
                $property_links_2 = array();
                $secondaryproperty_links_2 = array();
                $liquid_links_2 = array();

                //-- if category type is powder
                if ($this->input->post('property_ids_1')) {
                    foreach ($this->input->post('property_ids_1') as $key => $property) {
                        $property_links_1[$key]['category_id'] = $category_id;
                        $property_links_1[$key]['property_id'] = $property;
                        $property_links_1[$key]['type'] = 1;
                    }
                }
                if ($this->input->post('secondaryproperty_ids_1')) {
                    foreach ($this->input->post('secondaryproperty_ids_1') as $key => $property) {
                        $secondaryproperty_links_1[$key]['category_id'] = $category_id;
                        $secondaryproperty_links_1[$key]['property_id'] = $property;
                        $secondaryproperty_links_1[$key]['type'] = 1;
                    }
                }
                if ($this->input->post('liquid_ids_1')) {
                    foreach ($this->input->post('liquid_ids_1') as $key => $liquid) {
                        $liquid_links_1[$key]['category_id'] = $category_id;
                        $liquid_links_1[$key]['liquid_id'] = $liquid;
                        $liquid_links_1[$key]['type'] = 1;
                    }
                }

                //-- if category type is pill then insert properties and liquid

                if ($this->input->post('property_ids_2')) {
                    foreach ($this->input->post('property_ids_2') as $key => $property) {
                        $property_links_2[$key]['category_id'] = $category_id;
                        $property_links_2[$key]['property_id'] = $property;
                        $property_links_2[$key]['type'] = 2;
                    }
                }
                if ($this->input->post('secondaryproperty_ids_2')) {
                    foreach ($this->input->post('secondaryproperty_ids_2') as $key => $property) {
                        $secondaryproperty_links_2[$key]['category_id'] = $category_id;
                        $secondaryproperty_links_2[$key]['property_id'] = $property;
                        $secondaryproperty_links_2[$key]['type'] = 2;
                    }
                }
                if ($this->input->post('liquid_ids_2')) {
                    foreach ($this->input->post('liquid_ids_2') as $key => $liquid) {
                        $liquid_links_2[$key]['category_id'] = $category_id;
                        $liquid_links_2[$key]['liquid_id'] = $liquid;
                        $liquid_links_2[$key]['type'] = 2;
                    }
                }
                //-- insert property,flavour and secondary property links
                if ($this->input->post('category_type') == 1) {
                    if ($property_links_1)
                        $this->categories_model->insert_category_property_links($property_links_1);
                    if ($secondaryproperty_links_1)
                        $this->categories_model->insert_category_secondaryproperty_links($secondaryproperty_links_1);
                    if ($liquid_links_1)
                        $this->categories_model->insert_category_liquid_links($liquid_links_1);
                }
                //-- insert property,flavour and secondary property links
                if ($this->input->post('category_type') == 2) {
                    if ($property_links_2)
                        $this->categories_model->insert_category_property_links($property_links_2);
                    if ($secondaryproperty_links_2)
                        $this->categories_model->insert_category_secondaryproperty_links($secondaryproperty_links_2);
                    if ($liquid_links_2)
                        $this->categories_model->insert_category_liquid_links($liquid_links_2);
                }
                if ($this->input->post('category_type') == 3) {
                    if ($property_links_1)
                        $this->categories_model->insert_category_property_links($property_links_1);
                    if ($secondaryproperty_links_1)
                        $this->categories_model->insert_category_secondaryproperty_links($secondaryproperty_links_1);
                    if ($liquid_links_1)
                        $this->categories_model->insert_category_liquid_links($liquid_links_1);
                    if ($property_links_2)
                        $this->categories_model->insert_category_property_links($property_links_2);
                    if ($secondaryproperty_links_2)
                        $this->categories_model->insert_category_secondaryproperty_links($secondaryproperty_links_2);
                    if ($liquid_links_2)
                        $this->categories_model->insert_category_liquid_links($liquid_links_2);
                }

                redirect('admin/categories');
            }
        }
        $this->template->load('admin', 'admin/categories/form', $data);
    }

    /**
     * Delete Category 
     * @param int $category_id - Category id
     */
    public function delete($category_id) {
        $check_category = $this->categories_model->get_category($category_id);
        if ($check_category) {
            $update_array = array(
                'status' => 'deleted'
            );
            $this->categories_model->common_insert_update('update', TBL_CATEGORIES, $update_array, 'id=' . $category_id);

            $this->session->set_flashdata('success', '"' . $check_category['name_en'] . '" category deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/categories');
    }

    /**
     * View Category details
     * @param int $category_id Category Id
     */
    public function view($category_id = NULL) {
        if (is_numeric($category_id)) {
            $check_category = $this->categories_model->get_category($category_id);
            if ($check_category) {
                $data['category_data'] = $check_category;
                $data['title'] = 'Supmatch | View Category';
                $data['heading'] = 'View Category';
                $this->template->load('admin', 'admin/categories/view', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Get categories for data table
     * */
    public function get_categories() {
        $final['recordsTotal'] = $this->categories_model->get_all_categories('count');
        $final['redraw'] = 1;
        $final['recordsFiltered'] = $final['recordsTotal'];
        $categories = $this->categories_model->get_all_categories();
        $start = $this->input->get('start') + 1;

        foreach ($categories as $key => $val) {
            $categories[$key] = $val;
            $categories[$key]['sr_no'] = $start++;
            $categories[$key]['created'] = date('d M Y', strtotime($val['created']));
        }

        $final['data'] = $categories;
        echo json_encode($final);
    }

}
