<?php

/**
 * Manage supplements 
 * @author KU 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplements extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('supplements_model', 'review_model', 'categories_model', 'ranks_model'));
        $this->load->library('upload');
        //$GLOBALS['unique_uname'] = $GLOBALS['unique_email'] = '';
    }

    public function index() {
        
    }

    /**
     * This function used to display supplement's data at admin side. 
     * @author pav
     * */
    public function display_supplements() {
        $data['title'] = 'Supmatch | Supplements';
        $data['resultArr'] = $resultArr = $this->supplements_model->get_all_supplements_details()->result_array();
        $flavour_ids_arr = array_column($resultArr, 'id');
        $data['flavourArr'] = $flavourArr = $this->supplements_model->get_all_flavour_details_by_supp_id_arr($flavour_ids_arr)->result_array();
        $cnt = 0;
        foreach ($resultArr as $result) {
            $displayArr['supplements'][$cnt]['id'] = $result['id'];
            $displayArr['supplements'][$cnt]['name_en'] = $result['name_en'];
            $displayArr['supplements'][$cnt]['name_gr'] = $result['name_gr'];
            $displayArr['supplements'][$cnt]['images'] = $result['images'];
            $displayArr['supplements'][$cnt]['amazon_asin'] = $result['amazon_ASIN'];
            $displayArr['supplements'][$cnt]['weight'] = $result['weight'];
            $displayArr['supplements'][$cnt]['price'] = $result['price'];
            $displayArr['supplements'][$cnt]['cat_name_en'] = $result['cat_name_en'];
            $displayArr['supplements'][$cnt]['status'] = $result['status'];
            $displayArr['supplements'][$cnt]['reviews'] = $result['reviews'];
            $displayArr['supplements'][$cnt]['created'] = $result['created'];
            $displayArr['supplements'][$cnt]['flavour'] = array();
            foreach ($flavourArr as $flavour) {
                if ($flavour['supplement_id'] == $result['id']) {
                    $displayArr['supplements'][$cnt]['flavour'][] = $flavour['fla_name_en'];
                }
            }
            $cnt++;
        }
        $data['displayArr'] = $displayArr;
        //p(array_column($resultArr->result_array(),'id')); die;
        $this->template->load('admin', 'admin/supplements/display_supplements', $data);
    }

    /**
     * This function used to add / edit supplement's data at admin side. 
     * @param Integer $id
     * @author pav
     * @modify KU (manage secodary properties)
     * */
    public function add_supplements($id = '') {
        $data['table'] = TBL_SUPPLEMENTS;
        $flavour_condition = array('status' => 'active');
        $flavour_sort_condition = array(array('field' => 'name_en', 'type' => 'ASC'));
        $data['flavourArr'] = $flavourArr = $this->supplements_model->get_all_details(TBL_FLAVOURS, $flavour_condition, $flavour_sort_condition);

        $category_condition = array('status' => 'active');
        $category_sort_condition = array(array('field' => 'name_en', 'type' => 'ASC'));
        $data['categoryArr'] = $categoryArr = $this->supplements_model->get_all_details(TBL_CATEGORIES, $category_condition, $category_sort_condition);

        $supplement_id = $id;
        if (is_numeric($supplement_id)) {
            $resultdata = $this->supplements_model->get_all_supplement_details_by_id($supplement_id, array('s.id' => $supplement_id))->result_array();
            $supp_flavourArr = $this->supplements_model->get_all_flavour_details_by_supp_id($supplement_id)->result_array();
            $flavour_list = '';
            foreach ($supp_flavourArr as $supp_flavour) {
                $flavour_list.=$supp_flavour['id'] . ',';
            }
            $data['supp_flavourArr'] = rtrim($flavour_list, ',');
            if ($resultdata) {
                //-- Get secondary properties based on supplement category and supplement's type
                $data['secondary_properties'] = $this->categories_model->get_secproperties_by_catandtype($resultdata[0]['category_ids'], $resultdata[0]['type']);
                $data['supplements_data'] = $resultdata;
                $data['title'] = 'Supmatch | Edit Supplements';
            } else {
                show_404();
            }
        } else {
            $data['title'] = 'Supmatch | Add Supplements';
        }

        if ($this->add_supplements_validate()) {
            $images = '';
            if ($_FILES['txt_main_image']['name'] != '') {
                $img_name_arr = explode('.', $_FILES['txt_main_image']['name']);
                $image_name_1 = date('Ymdhis') . '_' . rand(111, 999) . '.' . $img_name_arr[(count($img_name_arr) - 1)];
                $_FILES['txt_main_image']['name'] = $image_name_1;
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                $config['max_size'] = 10000;
                $config['upload_path'] = './uploads/supplements';
                $this->upload->initialize($config);
                if ($this->upload->do_upload('txt_main_image')) {
                    $thumbnail = $this->upload->data();
                    $main_image = $thumbnail['file_name'];
                } else {
                    $thumbnail = $this->upload->display_errors();
                    $this->set->flashdata('error', $logoDetails);
                    redirect('admin/add_supplements');
                }
            } else {
                $main_image = $this->input->post('hidden_main_image');
            }

            $other_images = '';
            $post_other_images = $this->input->post('hidden_other_image');
            if (!empty($post_other_images)) {
                $other_images = $other_images . ',' . implode(',', $post_other_images);
            }
            if (!empty($_FILES['txt_other_images']['name'])) {
                $filesCount = count($_FILES['txt_other_images']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    $img_name_arr = explode('.', $_FILES['txt_other_images']['name'][$i]);
                    $image_name = date('Ymdhis') . '_' . rand(111, 999) . '.' . $img_name_arr[(count($img_name_arr) - 1)];
                    $_FILES['supp_file']['name'] = $image_name;
                    $_FILES['supp_file']['type'] = $_FILES['txt_other_images']['type'][$i];
                    $_FILES['supp_file']['tmp_name'] = $_FILES['txt_other_images']['tmp_name'][$i];
                    $_FILES['supp_file']['error'] = $_FILES['txt_other_images']['error'][$i];
                    $_FILES['supp_file']['size'] = $_FILES['txt_other_images']['size'][$i];

                    $config['overwrite'] = FALSE;
                    $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                    $config['max_size'] = 10000;
                    $config['upload_path'] = './uploads/supplements';

                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('supp_file')) {
                        $other_image_arr = $this->upload->data();
                        $other_img = $other_image_arr['file_name'];
                        $other_images = $other_images . ',' . $other_img;
                    }
                }
            }
            $images.=$main_image;
            if ($other_images != '') {
                $images.=$other_images;
            }
            /*
              $boolean_value = array();
              if ($this->input->post('txt_boolean_checkbox'))
              $boolean_value = $this->input->post('txt_boolean_checkbox'); */

            $post_flavours = $this->input->post('txt_flavours');

            $dataArr = array(
                'name_en' => $this->input->post('name_en'),
                'name_gr' => $this->input->post('name_gr'),
                'desc_en' => $this->input->post('txt_supplemets_desc_en'),
                'desc_gr' => $this->input->post('txt_supplemets_desc_gr'),
                'images' => $images,
//                'availibility' => (in_array('availiblity', $boolean_value)) ? '1' : '0',
                'amazon_link' => $this->input->post('txt_amazon_link'),
                'amazon_ASIN' => $this->input->post('txt_amazon_asin'),
                'weight' => $this->input->post('txt_weight'),
                'price' => $this->input->post('txt_price'),
                'category_ids' => $this->input->post('txt_category'),
                'type' => $this->input->post('supplement_type'),
                'status' => $this->input->post('txt_status') == 'on' ? 'active' : 'inactive'
            );

            if (is_numeric($supplement_id)) {
                $condition = array('id' => $supplement_id);
                $this->supplements_model->common_insert_update('update', TBL_SUPPLEMENTS, $dataArr, $condition);
                $this->db->where('supplement_id', $supplement_id);
                $this->db->delete(TBL_SUPPLEMENTS_FLAVOURS_LINK);
                if (!empty($post_flavours)) {
                    $cnt = 0;
                    foreach ($post_flavours as $flavour) {
                        $flavour_dataArr[$cnt]['supplement_id'] = $supplement_id;
                        $flavour_dataArr[$cnt]['flavour_id'] = $flavour;
                        $cnt++;
                    }
                    $this->db->insert_batch(TBL_SUPPLEMENTS_FLAVOURS_LINK, $flavour_dataArr);
                }

                $this->session->set_flashdata('success', 'Supplement\'s data has been updated successfully.');
                $this->supplements_model->delete_secondprop_links($supplement_id);
            } else {
                $this->supplements_model->common_insert_update('insert', TBL_SUPPLEMENTS, $dataArr);
                $supplement_id = $this->db->insert_id();
                if (!empty($post_flavours)) {
                    $cnt = 0;
                    foreach ($post_flavours as $flavour) {
                        $flavour_dataArr[$cnt]['supplement_id'] = $supplement_id;
                        $flavour_dataArr[$cnt]['flavour_id'] = $flavour;
                        $cnt++;
                    }
                    $this->db->insert_batch(TBL_SUPPLEMENTS_FLAVOURS_LINK, $flavour_dataArr);
                }
                $this->session->set_flashdata('success', 'New Supplement has been added successfully.');
            }
            //-- If secondary properties are selected then insert them into table
            if ($this->input->post('txt_boolean_checkbox')) {
                $second_property_arr = array();
                foreach ($this->input->post('txt_boolean_checkbox') as $key => $secondary_property) {
                    $second_property_arr[$key]['supplement_id'] = $supplement_id;
                    $second_property_arr[$key]['property_id'] = $secondary_property;
                }
                $this->supplements_model->insert_secondprop_links($second_property_arr);
            }
            redirect('admin/supplements');
        }
        $this->template->load('admin', 'admin/supplements/add_supplements', $data);
    }

    /**
     * This function used to check validation of add / edit supplement's form. 
     * @return Bollean
     * @author pav
     * */
    public function add_supplements_validate() {
        $this->form_validation->set_rules('name_en', 'Name (English)', 'trim|required');
        $this->form_validation->set_rules('name_gr', 'Name (German)', 'trim|required');
        $this->form_validation->set_rules('txt_supplemets_desc_en', 'Desc (English)', 'required');
        $this->form_validation->set_rules('txt_supplemets_desc_gr', 'Desc (German)', 'required');
        $this->form_validation->set_rules('txt_amazon_link', 'Amazon Link', 'trim|required');
        $this->form_validation->set_rules('txt_amazon_asin', 'Amazon ASIN', 'trim|required');
        $this->form_validation->set_rules('txt_weight', 'Weight', 'trim|required');
        $this->form_validation->set_rules('txt_price', 'Price', 'trim|required');
        $this->form_validation->set_rules('txt_category', 'Category', 'trim|required');
//        $this->form_validation->set_rules('txt_flavours[]', 'Flavours', 'trim|required');
        return $this->form_validation->run();
    }

    /**
     * This function used to edit supplement's data at admin side. 
     * @param Integer $id
     * @author pav
     * */
    public function edit_supplements() {
        $this->add_supplements($this->uri->segment(3));
    }

    /**
     * This function used to delete supplement's data at admin side. 
     * @param Integer $user_id
     * @author pav
     * */
    public function delete_supplements($s_id) {
        $supplement_data = $this->supplements_model->get_all_supplement_details_by_id($s_id, array('s.id' => $s_id))->result_array();
        if ($supplement_data) {
            $update_array = array(
                'status' => 'deleted'
            );
            $condition = array('id' => $s_id);
            $this->supplements_model->common_insert_update('update', TBL_SUPPLEMENTS, $update_array, $condition);
            $this->session->set_flashdata('success', 'supplement\'s data has been deleted successfully!');
        } else {
            $this->session->set_flashdata('error', 'Invalid request. Please try again!');
        }
        redirect('admin/supplements');
    }

    /**
     * Display all supplement reviews
     * @author KU
     * @param int $supplement_id
     */
    public function reviews($supplement_id = NULL) {
        if (is_numeric($supplement_id)) {
            $data['title'] = 'Supmatch | Reviews';
            $data['supplement'] = $this->supplements_model->get_all_details(TBL_SUPPLEMENTS, array('id' => $supplement_id))->row_array();
            $data['reviews'] = $this->review_model->get_supplement_reviews($supplement_id);
            $this->template->load('admin', 'admin/supplements/display_reviews', $data);
        } else {
            show_404();
        }
    }

    /**
     * Delete Reviews posted by user
     * @author KU
     * @param int $review_id
     */
    public function delete_review($review_id = NULL) {
        if (is_numeric($review_id)) {
            $supplement = $this->review_model->get_all_details(TBL_SUPPLEMENT_RATING, array('id' => $review_id))->row_array();
            if ($supplement) {
                $update_array = array(
                    'status' => 'deleted'
                );
                $condition = array('id' => $review_id);
                $this->review_model->common_insert_update('update', TBL_SUPPLEMENT_RATING, $update_array, $condition);
                $this->session->set_flashdata('success', 'Review been deleted successfully!');
                //-- updates user rank if his review is deleted by admin
                $this->update_user_rank($supplement['user_id']);
                $this->supplements_model->update_accuracy($supplement['supplement_id']); //- Update accuracy of the supplement

                $total_reviews = $this->supplements_model->total_reviews($supplement['supplement_id']);
                $total_reviews['total'];
                if ($total_reviews['total'] > 0) {
                    $this->supplements_model->insert_update_supplement_properties_rating($supplement['supplement_id']); //-- Updates average rating property of supplement
                } else {
                    $this->supplements_model->delete_properties_average_rating($supplement['supplement_id']); //-- Delete already exist property average rating
                }
                redirect('admin/supplements/reviews/' . $supplement['supplement_id']);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    /**
     * Get category details based on category selection
     * @author KU
     */
    public function get_cat_details() {
        $category_id = $this->input->post('category_id');
        $category_data = $this->supplements_model->get_all_details(TBL_CATEGORIES, array('id' => $category_id))->row_array();
        if ($category_data['type'] == 3 || $category_data['type'] == 1) {
            $type = 1;
        } else {
            $type = 2;
        }
        $properties = $this->categories_model->get_secproperties_by_catandtype($category_id, $type);

        $result = array('type' => $category_data['type'], 'second_properties' => $properties);
        echo json_encode($result);
        exit;
    }

    /**
     * Get secondary property details based on category and its type selection
     * @author KU
     */
    public function get_secondary_property_details() {
        $category_id = $this->input->post('category_id');
        $type = $this->input->post('type');
        $properties = $this->categories_model->get_secproperties_by_catandtype($category_id, $type);
        $result = array('second_properties' => $properties);
        echo json_encode($result);
        exit;
    }

    /**
     * Updates user rank if review is deleted by admin
     * @param int $user_id
     */
    public function update_user_rank($user_id = NULL) {
        if (is_numeric($user_id)) {
            //-- Get user's review count and reviwe likes count 
            $result = $this->users_model->get_user_total_like_counts($user_id);
            $total_likes = $result['total'];
            $result = $this->users_model->get_user_total_reviews($user_id);
            $total_reviews = $result['total'];
            $rank_detail = $this->ranks_model->get_rank_by_reviews_likes($total_reviews, $total_likes);
            //-- If rank details found from database then update user rank 
            $rank_id = 0;
            $default_rank = $this->ranks_model->get_default_rank();
            if ($default_rank) {
                $rank_id = $default_rank['id'];
            }
            if ($rank_detail) {
                //-- Get user's old rank
                $old_rank = $this->users_model->get_user_rank($user_id);
                $rank_id = $rank_detail['id'];
                //-- If old rank is not same as new rank then send email to user regarding rank update
                if ($rank_id != $old_rank['rank_id']) {
//                    $msg = "Hello " . $old_rank['nickname'] . ", Your rank has been updated to <b>" . $rank_detail['name'] . "</b>";
                    $msg = $this->load->view('email_templates/rank_update', array('nickname' => $old_rank['nickname'], 'rank' => $rank_detail['name']), true);
                    $email_array = array(
                        'mail_type' => 'html',
                        'from_mail_id' => $this->config->item('smtp_user'),
                        'from_mail_name' => '',
                        'to_mail_id' => $old_rank['email'],
                        'cc_mail_id' => '',
                        'subject_message' => utf8_encode('Du hast einen neuen Rang erreicht!'),
                        'body_messages' => $msg
                    );
                    $this->users_model->common_email_send($email_array);
                }
            }
            $this->ranks_model->common_insert_update('update', TBL_USERS, array('rank_id' => $rank_id, 'total_reviews' => $total_reviews), array('id' => $user_id));
        }
    }

    /*
     * Get price of supplement
     * @author SG
     */

    public function get_suppliment_price() {
        if ($this->input->post()) {
            $aws_access_key_id = AWS_API_KEY;
            $aws_secret_key = AWS_API_SECRET_KEY;
            $endpoint = "webservices.amazon.de";
            $uri = "/onca/xml";
            $asin = $this->input->post('txt_amazon_asin');
            $params = array(
                "Service" => "AWSECommerceService",
                "Operation" => "ItemLookup",
                "AWSAccessKeyId" => AWS_API_KEY,
                "AssociateTag" => AWS_ASSOCIATE_TAG,
                "ItemId" => $asin,
                "IdType" => "ASIN",
                "ResponseGroup" => "Images,ItemAttributes,Offers"
            );
            if (!isset($params["Timestamp"])) {
                $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
            }
            ksort($params);
            $pairs = array();
            foreach ($params as $key => $value) {
                array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
            }
            $canonical_query_string = join("&", $pairs);
            $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;
            $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
            $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);
            $response = simplexml_load_file($request_url);
//            $response = (array)$response;
//            $price = $response['OperationRequest']['Items']['ListPrice']['FormattedPrice'];
//            p($response); exit;
            if (!empty($response)) {
                if (!empty($response->Items)) {
                    if (!empty($response->Items->Item->ItemAttributes)) {
                        if (!empty($response->Items->Item->Offers)) {
//                            $price = str_replace(',', '.', explode(' ', $response->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice)[1]);
                            $price = str_replace(',', '.', explode(' ', $response->Items->Item->Offers->Offer->OfferListing->SalePrice->FormattedPrice)[1]);
                            if ($price == '') {
                                $price = str_replace(',', '.', explode(' ', $response->Items->Item->ItemAttributes->ListPrice->FormattedPrice)[1]);
                            }
                        } else if (!empty($response->Items->Item->ItemAttributes->ListPrice)) {
                            $price = str_replace(',', '.', explode(' ', $response->Items->Item->ItemAttributes->ListPrice->FormattedPrice)[1]);
                        }
                    } else {
                        $price = '';
                    }
                }
            }
            echo $price;
            exit;
        }
    }

    public function test() {
        $total_reviews = $this->supplements_model->total_reviews(82);
        $total_reviews['total'];
        if ($total_reviews['total'] > 0) {
            $this->supplements_model->insert_update_supplement_properties_rating(82);
        } else {
            $this->supplements_model->delete_properties_average_rating(82);
        }
    }

}

/* End of file Supplements.php */
/* Location: ./application/controllers/admin/Supplements.php */