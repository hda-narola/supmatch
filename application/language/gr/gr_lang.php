<?php
	$lang['home_product_search'] = 'Produkt suchen';
	$lang['home_start_page'] = 'Startseite';
	$lang['home_this_is_how_it_work'] = 'So Funktioniert\'s';
	$lang['home_about_us'] = 'Über Uns';
	$lang['home_contact_us'] = 'Kontakt';
	$lang['home_log_in'] = 'Anmelden';
	$lang['wc_to_supmatch'] = 'willkommen bei supmatch';
	$lang['home_banner_text1'] = 'FINDE KOSTENLOS DEIN OPTIMALES SUPPLEMENT IN WENI';
	$lang['home_banner_text2'] = 'GER ALS 2 MINUTEN!';
	$lang['home_plz_select_category'] = 'BITTE WÄHLE EINE KATEGORIE';
	//$lang['home_start_matching'] = 'Matching Starten';
	$lang['home_you_dn\'t_know'] = 'du weisst noch nicht, welche supplements fur deine ziele geeignet sind?';
	$lang['home_free_recommendation'] = 'Kostenlose Empfehlung';
	$lang['home_find_out_which_supp_fit_your_goal'] = 'Finde heraus, welche Supplements zu deinem Ziel passen!';
	$lang['home_choose_your_goal'] = 'WÄHLE DEIN ZIEL';
	$lang['home_muscle_mass'] = 'muskelmasse';
	$lang['home_ext_energy'] = 'AUSDAUER & ENERGIE';
	$lang['home_defination_muscle_construction'] = 'DEFINITION & MUSKELAUFBAU';
	$lang['home_lose_weight'] = 'abhnehmen';
	$lang['home_continue'] = 'weiter';
	$lang['home_banner3_text1'] = 'Diese Produktkategorien sind für dein Ziel optimal! ';
	$lang['home_banner3_text2'] = 'Welche Kategorie kommt deinem Ziel am nächsten?';
	$lang['home_plz_select_off'] = 'BITTE WÄHLE AUS';
	$lang['home_start_matching'] = 'matching starten';

	$lang['home_log_in'] = 'EINLOGGEN';
	$lang['home_username'] = 'Benutzername';
	$lang['home_email'] = 'E-Mail';
	$lang['home_password'] = 'Passwort';
	$lang['home_forgot_password'] = 'Passwort vergessen?';
	$lang['home_stay_logged_in'] = 'Eingeloggt bleiben';
	$lang['home_not_member_yet'] = 'Noch kein Mitglied?';
	$lang['home_register_now'] = 'Jetzt registrieren!';
	$lang['or'] = 'oder';

	$lang['home_registration'] = 'Registrieren';
	$lang['home_reenter_password'] = 'Passwort erneut eingeben';
	$lang['home_i_agree'] = 'Hiermit stimme ich den AGBs zu';
	$lang['home_subscribe_to_newsletter'] = 'Newsletter abonnieren';
	$lang['home_back_to'] = 'Zurück zum';

	
?>