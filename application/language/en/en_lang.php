<?php
	
	$lang['home_product_search'] = 'Product Search';
	$lang['home_start_page'] = 'Start Page';
	$lang['home_this_is_how_it_work'] = 'This is how it works';
	$lang['home_about_us'] = 'About Us';
	$lang['home_contact_us'] = 'Contact Us';
	$lang['home_log_in'] = 'Log In';
	$lang['wc_to_supmatch'] = 'welcome to supmatch';
	$lang['home_banner_text1'] = 'find free your optimal supplement in weni';
	$lang['home_banner_text2'] = 'minutes than 2!';
	$lang['home_plz_select_category'] = 'please select a category';
	$lang['home_start_matching'] = 'Start matching';
	$lang['home_you_dn\'t_know'] = 'you do not know yet which supplements are appropriate for your goals?';
	$lang['home_free_recommendation'] = 'free recommendation';
	$lang['home_find_out_which_supp_fit_your_goal'] = 'Find out which supplements suit your target!';
	$lang['home_choose_your_goal'] = 'choose your destination';
	$lang['home_muscle_mass'] = 'muscle mass';
	$lang['home_ext_energy'] = 'stamina & energy';
	$lang['home_defination_muscle_construction'] = 'definition & muscle construction';
	$lang['home_lose_weight'] = 'lose weight';
	$lang['home_continue'] = 'continue';
	$lang['home_banner3_text1'] = 'These product categories are ideal for your goal! ';
	$lang['home_banner3_text2'] = 'Which category is closest to your goal?';
	$lang['home_plz_select_off'] = 'please select off';
	$lang['home_start_matching'] = 'start matching';

	$lang['home_log_in'] = 'Log In';
	$lang['home_username'] = 'Username';
	$lang['home_email'] = 'E-Mail';
	$lang['home_password'] = 'Password';
	$lang['home_forgot_password'] = 'Forgot Password?';
	$lang['home_stay_logged_in'] = 'Stay Logged In';
	$lang['home_not_member_yet'] = 'Not Member Yet?';
	$lang['home_register_now'] = 'Register Now!';
	$lang['or'] = 'OR';

	$lang['home_registration'] = 'Registration';
	$lang['home_reenter_password'] = 'Re-enter password';
	$lang['home_i_agree'] = 'I agree to the terms and conditions';
	$lang['home_subscribe_to_newsletter'] = 'Subscribe to Newsletter';
	$lang['home_back_to'] = 'Back to';
?>