<?php

/**
 * Liquid Model - Manage Liquids
 * @author KU
 */
class Liquid_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all liquids from database
     */
    public function get_allliquids() {
        $this->db->select('l.id,l.name_en,l.name_gr,l.created,ll.category');
        $this->db->join('(SELECT GROUP_CONCAT(c.name_en) category,liquid_id FROM ' . TBL_CATEGORIES_LIQUIDS_LINK . ' cl JOIN ' . TBL_CATEGORIES . ' c ON cl.category_id=c.id WHERE c.status=\'active\' GROUP BY cl.liquid_id) ll', 'l.id=ll.liquid_id', 'left');
        $this->db->where('l.status', 'active');
        $this->db->order_by('l.created', 'desc');
        $query = $this->db->get(TBL_LIQUIDS . ' l');
        return $query->result_array();
    }

    /**
     * Get results based on datatable in liquids list page
     * @param string $type - count or result 
     * @return array
     */
    public function get_all_liquids($type = 'result') {
        $columns = ['id', 'name_en', 'name_gr', 'category', 'created'];

        $keyword = $this->input->get('search');
        $this->db->select('id,name_en,name_gr,created,(select GROUP_CONCAT(name_en) FROM ' . TBL_CATEGORIES . ' WHERE FIND_IN_SET(id,liquid_ids)!=0 AND status=\'active\') as category');

        if (!empty($keyword['value'])) {
            $where = '(name_en LIKE "%' . $keyword['value'] . '%" OR name_gr LIKE "%' . $keyword['value'] . '%")';
            $this->db->where($where);
        }
        $this->db->where('status', 'active');

        $this->db->order_by($columns[$this->input->get('order')[0]['column']], $this->input->get('order')[0]['dir']);

        if ($type == 'count') {
            $query = $this->db->get(TBL_LIQUIDS);
            return $query->num_rows();
        } else {
            $this->db->limit($this->input->get('length'), $this->input->get('start'));
            $query = $this->db->get(TBL_LIQUIDS);
            return $query->result_array();
        }
    }

    /**
     * Get liquid details
     * @param int $liquid_id Id of liquid
     * @return array
     */
    public function get_liquid($liquid_id) {
        $this->db->where('id', $liquid_id);
        $this->db->where('status', 'active');
        $query = $this->db->get(TBL_LIQUIDS);
        return $query->row_array();
    }

    /**
     * Get all flavours of particular category
     * @param int $category_id
     * @return array
     */
    public function get_liquids_by_category($category_id, $type) {
        $this->db->select('l.name_en,l.name_gr,l.id');
        $this->db->join(TBL_CATEGORIES_LIQUIDS_LINK . ' ll', 'l.id=ll.liquid_id AND ll.type=' . $type);
        $this->db->where('ll.category_id', $category_id);
        $this->db->where('l.status', 'active');
        $query = $this->db->get(TBL_LIQUIDS . ' l');
        return $query->result_array();
    }

}
