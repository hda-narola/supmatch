<?php

/**
 * Categories Model - Manage Categories
 * @author KU
 */
class Categories_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all categories from database
     */
    public function get_allcategories() {
        $this->db->select('c.*,lp.properties,ll.liquids,lsp.secondary_properties,lp2.properties as properties2,lsp2.secondary_properties as secondary_properties2,ll2.liquids as liquids2');
        $this->db->join('(SELECT GROUP_CONCAT(p.name_en) properties,category_id FROM ' . TBL_CATEGORIES_PROPERTIES_LINK . ' l JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id WHERE p.status=\'active\' AND l.type=1 GROUP BY l.category_id) lp', 'c.id=lp.category_id', 'left');
        $this->db->join('(SELECT GROUP_CONCAT(p.name_en) secondary_properties,category_id FROM ' . TBL_CATEGORIES_SECONDARYPROPERTIES_LINK . ' l JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id WHERE p.status=\'active\' AND l.type=1 GROUP BY l.category_id) lsp', 'c.id=lsp.category_id', 'left');
        $this->db->join('(SELECT GROUP_CONCAT(l.name_en) liquids,category_id FROM ' . TBL_CATEGORIES_LIQUIDS_LINK . ' cl JOIN ' . TBL_LIQUIDS . ' l ON cl.liquid_id=l.id WHERE l.status=\'active\' AND cl.type=1 GROUP BY cl.category_id) ll', 'c.id=ll.category_id', 'left');
        $this->db->join('(SELECT GROUP_CONCAT(p.name_en) properties,category_id FROM ' . TBL_CATEGORIES_PROPERTIES_LINK . ' l JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id WHERE p.status=\'active\' AND l.type=2 GROUP BY l.category_id) lp2', 'c.id=lp2.category_id', 'left');
        $this->db->join('(SELECT GROUP_CONCAT(p.name_en) secondary_properties,category_id FROM ' . TBL_CATEGORIES_SECONDARYPROPERTIES_LINK . ' l JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id WHERE p.status=\'active\' AND l.type=2 GROUP BY l.category_id) lsp2', 'c.id=lsp2.category_id', 'left');
        $this->db->join('(SELECT GROUP_CONCAT(l.name_en) liquids,category_id FROM ' . TBL_CATEGORIES_LIQUIDS_LINK . ' cl JOIN ' . TBL_LIQUIDS . ' l ON cl.liquid_id=l.id WHERE l.status=\'active\' AND cl.type=2 GROUP BY cl.category_id) ll2', 'c.id=ll2.category_id', 'left');
        $this->db->where('c.status!=', 'deleted');
        $this->db->order_by('c.created', 'desc');
        $query = $this->db->get(TBL_CATEGORIES . ' c');
        return $query->result_array();
    }

    /**
     * Get category details
     * @param int $category_id Id of category
     * @return array
     */
    public function get_category($category_id) {
        $select = '(SELECT GROUP_CONCAT(l.liquid_id) FROM ' . TBL_CATEGORIES_LIQUIDS_LINK . ' l LEFT JOIN ' . TBL_LIQUIDS . ' lq ON l.liquid_id=lq.id AND l.category_id=' . $category_id . ' WHERE lq.status=\'active\' AND l.type=1 GROUP BY l.category_id) liquid_ids';
        $select1 = '(SELECT GROUP_CONCAT(l.property_id) FROM ' . TBL_CATEGORIES_SECONDARYPROPERTIES_LINK . ' l LEFT JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id AND l.category_id=' . $category_id . ' WHERE p.status=\'active\' AND l.type=1 GROUP BY l.category_id) secondaryproperty_ids';
        $select2 = '(SELECT GROUP_CONCAT(l.property_id) FROM ' . TBL_CATEGORIES_PROPERTIES_LINK . ' l LEFT JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id AND l.category_id=' . $category_id . ' WHERE p.status=\'active\' AND l.type=1 GROUP BY l.category_id) property_ids';
        $select3 = '(SELECT GROUP_CONCAT(l.liquid_id) FROM ' . TBL_CATEGORIES_LIQUIDS_LINK . ' l LEFT JOIN ' . TBL_LIQUIDS . ' lq ON l.liquid_id=lq.id AND l.category_id=' . $category_id . ' WHERE lq.status=\'active\' AND l.type=2 GROUP BY l.category_id) liquid_ids_2';
        $select4 = '(SELECT GROUP_CONCAT(l.property_id) FROM ' . TBL_CATEGORIES_SECONDARYPROPERTIES_LINK . ' l LEFT JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id AND l.category_id=' . $category_id . ' WHERE p.status=\'active\' AND l.type=2 GROUP BY l.category_id) secondaryproperty_ids_2';
        $select5 = '(SELECT GROUP_CONCAT(l.property_id) FROM ' . TBL_CATEGORIES_PROPERTIES_LINK . ' l LEFT JOIN ' . TBL_PROPERTIES . ' p ON l.property_id=p.id AND l.category_id=' . $category_id . ' WHERE p.status=\'active\' AND l.type=2 GROUP BY l.category_id) property_ids_2';
        $this->db->select('*,' . $select . ',' . $select1 . ',' . $select2 . ',' . $select3 . ',' . $select4 . ',' . $select5);
        $this->db->where('id', $category_id);
//        $this->db->where('status!=', 'deleted');
        $query = $this->db->get(TBL_CATEGORIES);
        return $query->row_array();
    }

    /**
     * Get all categories for goals dropdwon
     * @param int $goal_id
     * @return array
     */
    public function get_categoriesforgoal($goal_id = NULL) {
        $this->db->select('c.id,c.name_en,c.name_gr');
        $this->db->where('c.status', 'active');
        $this->db->where('(l.count < 4 OR l.count IS NULL)');
        if ($goal_id != NULL) {
            $this->db->join('(SELECT count(goal_id) count,category_id FROM ' . TBL_CATEGORIES_GOAL_LINK . ' WHERE goal_id!=' . $goal_id . ' group by category_id) l', 'c.id=l.category_id', 'left');
        } else {
            $this->db->join('(SELECT count(goal_id) count,category_id FROM ' . TBL_CATEGORIES_GOAL_LINK . ' group by category_id) l', 'c.id=l.category_id', 'left');
        }
        $query = $this->db->get(TBL_CATEGORIES . ' c');
        return $query->result_array();
    }

    /**
     * Insert category into table
     * @param array $dataArr
     * @return int 
     */
    public function insert_category($dataArr) {
        $this->db->insert(TBL_CATEGORIES, $dataArr);
        return $this->db->insert_id();
    }

    /**
     * Insert multiples links in categories_goals_link table
     * @param array $data
     * @return boolean
     */
    public function insert_category_property_links($data) {
        if ($this->db->insert_batch(TBL_CATEGORIES_PROPERTIES_LINK, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Insert multiples links in categories_goals_link table
     * @param array $data
     * @return boolean
     */
    public function insert_category_secondaryproperty_links($data) {
        if ($this->db->insert_batch(TBL_CATEGORIES_SECONDARYPROPERTIES_LINK, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Insert multiples links in categories_goals_link table
     * @param array $data
     * @return boolean
     */
    public function insert_category_liquid_links($data) {
        if ($this->db->insert_batch(TBL_CATEGORIES_LIQUIDS_LINK, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Delete category links from database
     * @param int $category_id
     * @param string $table
     * @return boolean
     */
    public function delete_links($category_id, $table) {
        $this->db->where(array('category_id' => $category_id));
        $this->db->delete($table);
    }

    /**
     * Get results based on datatable in categories list page
     * @param string $type - count or result 
     * @return array
     */
    public function get_all_categories($type = 'result') {
        $columns = ['id', 'icon', 'name_en', 'price_property', 'properties', 'liquids', 'created'];

        $keyword = $this->input->get('search');
        $this->db->select('*,(select GROUP_CONCAT(name_en) FROM ' . TBL_PROPERTIES . ' WHERE FIND_IN_SET(id,property_ids)!=0 AND status=\'active\') as properties,(select GROUP_CONCAT(name_en) FROM ' . TBL_LIQUIDS . ' WHERE FIND_IN_SET(id,liquid_ids)!=0 AND status=\'active\') as liquids');

        if (!empty($keyword['value'])) {
            $where = '(name_en LIKE "%' . $keyword['value'] . '%" OR name_gr LIKE "%' . $keyword['value'] . '%")';
            $this->db->where($where);
        }
        $this->db->where('status', 'active');

        $this->db->order_by($columns[$this->input->get('order')[0]['column']], $this->input->get('order')[0]['dir']);

        if ($type == 'count') {
            $query = $this->db->get(TBL_CATEGORIES);
            return $query->num_rows();
        } else {
            $this->db->limit($this->input->get('length'), $this->input->get('start'));
            $query = $this->db->get(TBL_CATEGORIES);
            return $query->result_array();
        }
    }

    /**
     * Get catgeory results based on category id
     * @param string $category_id - integer
     * @param int $type - integer
     * @return Object
     * @author pav
     */
    public function get_cat_by_id($category_id, $type) {
        $this->db->select('c.icon as cat_image,prop.id as prop_id,prop.name_gr as prop_name_gr,prop.desc_gr as prop_desc_gr,c.type');
        $this->db->from(TBL_CATEGORIES . ' as c');
        $this->db->join(TBL_CATEGORIES_PROPERTIES_LINK . ' as cat_prop', 'c.id=cat_prop.category_id AND cat_prop.type=' . $type, 'left');
        $this->db->join(TBL_PROPERTIES . ' as prop', 'cat_prop.property_id=prop.id', 'left');
        $this->db->where('c.id', $category_id);
        return $this->db->get();
    }

    /**
     * Returns all active categories 
     * @return array
     */
    public function get_all_active_cats() {
        $this->db->where('status', 'active');
        $query = $this->db->get(TBL_CATEGORIES);
        return $query->result_array();
    }

    /**
     * Get Categroy type
     * @param int $category_id
     * @author KU
     */
    public function get_cat_type($category_id) {
        $this->db->select('type');
        $this->db->from(TBL_CATEGORIES);
        $this->db->where('id', $category_id);
        return $this->db->get()->row_array();
    }

    /**
     * Get catgeory results based on category id
     * @param string $category_id - integer
     * @param int $type - integer
     * @return array
     * @author KU
     */
    public function get_secproperties_by_catandtype($category_id, $type) {
        $this->db->select('prop.id,prop.name_en,prop.name_gr,prop.desc_en,prop.desc_gr');
        $this->db->join(TBL_PROPERTIES . ' prop', 'l.property_id=prop.id', 'left');
        $this->db->from(TBL_CATEGORIES_SECONDARYPROPERTIES_LINK . ' l');
        $this->db->where('l.category_id', $category_id);
        $this->db->where('l.type', $type);
        $this->db->where('prop.status', 'active');
        return $this->db->get()->result_array();
    }

}
