<?php

/**
 * Flavour Model - Manage flavours
 * @author KU
 */
class Flavour_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all flavours from database
     */
    public function get_allflavours() {
        $this->db->where('status', 'active');
        $this->db->order_by('created', 'desc');
        $query = $this->db->get(TBL_FLAVOURS);
        return $query->result_array();
    }

    /**
     * Get results based on datatable in flavours list page
     * @param string $type - count or result 
     * @return array
     */
    public function get_all_flavours($type = 'result') {
        $columns = ['id', 'name_en', 'name_gr', 'created'];

        $keyword = $this->input->get('search');

        if (!empty($keyword['value'])) {
            $where = '(name_en LIKE "%' . $keyword['value'] . '%" OR name_gr LIKE "%' . $keyword['value'] . '%")';
            $this->db->where($where);
        }
        $this->db->where('status', 'active');

        $this->db->order_by($columns[$this->input->get('order')[0]['column']], $this->input->get('order')[0]['dir']);

        if ($type == 'count') {
            $query = $this->db->get(TBL_FLAVOURS);
            return $query->num_rows();
        } else {
            $this->db->limit($this->input->get('length'), $this->input->get('start'));
            $query = $this->db->get(TBL_FLAVOURS);
            return $query->result_array();
        }
    }

    /**
     * Get flavour details
     * @param int $flavour_id Id of Flavour
     * @return array
     */
    public function get_flavour($flavour_id) {
        $this->db->where('id', $flavour_id);
        $this->db->where('status', 'active');
        $query = $this->db->get(TBL_FLAVOURS);
        return $query->row_array();
    }

    /**
     * Get all flavours of particular supplement
     * @param int $supplement_id
     * @return array
     */
    public function get_flavours_by_supplement($supplement_id) {
        $this->db->select('f.name_en,f.name_gr,f.id');
        $this->db->join(TBL_SUPPLEMENTS_FLAVOURS_LINK . ' l', 'f.id=l.flavour_id');
        $this->db->where('l.supplement_id', $supplement_id);
        $this->db->where('f.status', 'active');
        $query = $this->db->get(TBL_FLAVOURS . ' f');
        return $query->result_array();
    }

}
