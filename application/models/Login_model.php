<?php

class Login_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Used to check Super admin/Business user login credentials is correct or wrong
     * @param string $email Email id of Super admin/Business user
     * @param string $password Password 
     * @return result of array if success else retrun false
     * */
    public function get_admin($email, $password) {
        $this->db->select('u.id,u.role_id,u.nickname,u.email,u.is_verified,u.status,u.image');
        $this->db->join('roles r', 'u.role_id=r.id', 'left');
        $this->db->where('r.name', 'admin');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $query = $this->db->get('users u');
        return $query->row_array();
    }

}
