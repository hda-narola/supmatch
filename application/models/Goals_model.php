<?php

/**
 * Goals Model - Manage Goals
 * @author KU
 */
class Goals_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all goals from database
     * @return array
     */
    public function get_allgoals() {
        $this->db->select('g.*,(SELECT GROUP_CONCAT(c.name_en) FROM ' . TBL_CATEGORIES . ' c LEFT JOIN ' . TBL_CATEGORIES_GOAL_LINK . ' gl ON c.id=gl.category_id WHERE c.status=\'active\' AND gl.goal_id=g.id) category');
        $this->db->where('g.status!=', 'deleted');
        $this->db->order_by('g.created', 'desc');
        $query = $this->db->get(TBL_GOALS . ' g');
        return $query->result_array();
    }

    /**
     * Get goal details
     * @param int $goal_id Id of goal
     * @return array
     */
    public function get_goal($goal_id) {
        $this->db->select('*,(SELECT GROUP_CONCAT(category_id) FROM ' . TBL_CATEGORIES_GOAL_LINK . ' WHERE goal_id=' . $goal_id . ') category_ids');
        $this->db->where('id', $goal_id);
//        $this->db->where('status', 'active');
        $query = $this->db->get(TBL_GOALS);
        return $query->row_array();
    }

    /**
     * Insert multiples links in categories_goals_link table
     * @param array $data
     * @return boolean
     */
    public function insert_goal_category_links($data) {
        if ($this->db->insert_batch(TBL_CATEGORIES_GOAL_LINK, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Delete goal and category link from database
     * @param int $goal_id
     * @param string $table
     * @return boolean
     */
    public function delete_goal_category_links($goal_id) {
        $this->db->where(array('goal_id' => $goal_id));
        $this->db->delete(TBL_CATEGORIES_GOAL_LINK);
    }

    /**
     * Insert goal into table
     * @param array $dataArr
     * @return int 
     */
    public function insert_goal($dataArr) {
        $this->db->insert(TBL_GOALS, $dataArr);
        return $this->db->insert_id();
    }

    /**
     * Get results based on datatable in goals list page
     * @param string $type - count or result 
     * @return array
     */
    public function get_all_goals($type = 'result') {
        $columns = ['id', 'name_en', 'name_gr', 'created'];

        $keyword = $this->input->get('search');
        $this->db->select('*,(select GROUP_CONCAT(name_en) FROM ' . TBL_GOALS . ' WHERE FIND_IN_SET(id,goal_ids)!=0 AND status=\'active\') as goals,(select GROUP_CONCAT(name_en) FROM ' . TBL_LIQUIDS . ' WHERE FIND_IN_SET(id,liquid_ids)!=0 AND status=\'active\') as liquids');

        if (!empty($keyword['value'])) {
            $where = '(name_en LIKE "%' . $keyword['value'] . '%" OR name_gr LIKE "%' . $keyword['value'] . '%")';
            $this->db->where($where);
        }
        $this->db->where('status', 'active');

        $this->db->order_by($columns[$this->input->get('order')[0]['column']], $this->input->get('order')[0]['dir']);

        if ($type == 'count') {
            $query = $this->db->get(TBL_GOALS);
            return $query->num_rows();
        } else {
            $this->db->limit($this->input->get('length'), $this->input->get('start'));
            $query = $this->db->get(TBL_GOALS);
            return $query->result_array();
        }
    }

    /**
     * Get category results based on goal id
     * @param string $goal_id - integer
     * @return Object
     */
    public function get_cat_by_goal_id($goal_id) {
        $this->db->select('cat.id as cat_id,cat.name_gr as cat_name_gr,cat.icon as cat_image,cat.desc_gr as cat_desc_gr');
        $this->db->from(TBL_CATEGORIES_GOAL_LINK . ' as cat_goal');
        $this->db->where('cat_goal.goal_id', $goal_id);
        $this->db->join(TBL_CATEGORIES . ' as cat', 'cat_goal.category_id=cat.id');
        return $this->db->get();
    }

}
