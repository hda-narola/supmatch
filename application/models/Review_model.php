<?php

/**
 * Review model
 * manage supplement reviews
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Review_model extends MY_Model {

    /**
     * Insert goal into table
     * @param array $dataArr
     * @return int 
     */
    public function insert_review($dataArr) {
        $this->db->insert(TBL_SUPPLEMENT_RATING, $dataArr);
        return $this->db->insert_id();
    }

    /**
     * Insert property ratings
     * @param array $data
     * @return boolean
     */
    public function insert_property_ratings($data) {
        if ($this->db->insert_batch(TBL_PROPERTIES_RATING_LINK, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Get review as per passed condition
     * @param string $condition
     */
    public function get_review($condition) {
        $this->db->where($condition);
        $query = $this->db->get(TBL_SUPPLEMENT_RATING);
        return $query->row_array();
    }

    /**
     * Get all reviews posted by user 
     * @param int $user_id
     */
    public function get_user_reviews($user_id, $order_by = 'created') {
        $sort = 'DESC';
        if ($order_by == 'product_name') {
            $sort = 'ASC';
        }
        $this->db->select('r.*,s.name_gr as product_name,s.images,f.name_gr as flavour,l.name_gr as liquid,pr.properties,pr.property_ratings,rd.color,rd.icon as rank_icon,u.nickname,IF(tbllike.likes IS NULL,0,tbllike.likes) likes,IF(dlike.dislikes IS NULL,0,dlike.dislikes) dislikes');
        $this->db->join(TBL_SUPPLEMENTS . ' s', 'r.supplement_id=s.id', 'left');
        $this->db->join(TBL_FLAVOURS . ' f', 'r.flavour_id=f.id', 'left');
        $this->db->join(TBL_LIQUIDS . ' l', 'r.liquid_id=l.id', 'left');
        $this->db->join(TBL_USERS . ' u', 'r.user_id=u.id', 'left');
        $this->db->join(TBL_RANK_DETAILS . ' rd', 'u.rank_id=rd.id AND rd.status=\'active\'', 'left');
        $this->db->join('(SELECT supplement_rating_id,GROUP_CONCAT(p.name_gr) as properties,GROUP_CONCAT(rl.rating) as property_ratings FROM ' . TBL_PROPERTIES_RATING_LINK . ' rl  LEFT JOIN  ' . TBL_PROPERTIES . ' p ON rl.property_id=p.id GROUP BY rl.supplement_rating_id) pr', 'r.id=pr.supplement_rating_id', 'left');
        $this->db->join('(SELECT count(id) likes,supplement_rating_id FROM ' . TBL_SUPPLEMENT_RATING_LIKE . ' WHERE status=1 GROUP BY supplement_rating_id) tbllike', 'r.id=tbllike.supplement_rating_id', 'left');
        $this->db->join('(SELECT count(id) dislikes,supplement_rating_id FROM ' . TBL_SUPPLEMENT_RATING_LIKE . ' WHERE status=0 GROUP BY supplement_rating_id) dlike', 'r.id=dlike.supplement_rating_id', 'left');
        $this->db->where('r.user_id', $user_id);
        $this->db->where('r.status', 'active');
        $this->db->order_by($order_by, $sort);
        $query = $this->db->get(TBL_SUPPLEMENT_RATING . ' r');
        return $query->result_array();
    }

    /**
     * Get all reviews by supplement 
     * @param int $supplement_id
     */
    public function get_supplement_reviews($supplement_id) {
        $this->db->select('r.*,f.name_gr as flavour_gr,f.name_en as flavour_en,l.name_en as liquid_en,l.name_gr as liquid_gr,pr.properties_gr,pr.properties_en,pr.property_ratings,rd.color,u.nickname,IF(tbllike.likes IS NULL,0,tbllike.likes) likes,IF(dlike.dislikes IS NULL,0,dlike.dislikes) dislikes');
        $this->db->join(TBL_FLAVOURS . ' f', 'r.flavour_id=f.id', 'left');
        $this->db->join(TBL_LIQUIDS . ' l', 'r.liquid_id=l.id', 'left');
        $this->db->join(TBL_USERS . ' u', 'r.user_id=u.id', 'left');
        $this->db->join(TBL_RANK_DETAILS . ' rd', 'u.rank_id=rd.id', 'left');
        $this->db->join('(SELECT supplement_rating_id,GROUP_CONCAT(p.name_gr) as properties_gr,GROUP_CONCAT(p.name_en) as properties_en,GROUP_CONCAT(rl.rating) as property_ratings FROM ' . TBL_PROPERTIES_RATING_LINK . ' rl  LEFT JOIN  ' . TBL_PROPERTIES . ' p ON rl.property_id=p.id GROUP BY rl.supplement_rating_id) pr', 'r.id=pr.supplement_rating_id', 'left');
        $this->db->join('(SELECT count(id) likes,supplement_rating_id FROM ' . TBL_SUPPLEMENT_RATING_LIKE . ' WHERE status=1 GROUP BY supplement_rating_id) tbllike', 'r.id=tbllike.supplement_rating_id', 'left');
        $this->db->join('(SELECT count(id) dislikes,supplement_rating_id FROM ' . TBL_SUPPLEMENT_RATING_LIKE . ' WHERE status=0 GROUP BY supplement_rating_id) dlike', 'r.id=dlike.supplement_rating_id', 'left');
        $this->db->where('r.supplement_id', $supplement_id);
        $this->db->where('r.status!=', 'deleted');
        $this->db->order_by('r.created', 'DESC');
        $query = $this->db->get(TBL_SUPPLEMENT_RATING . ' r');
        return $query->result_array();
    }

}

/* End of file Review_model.php */
/* Location: ./application/models/Review_model.php */