<?php

/**
 * Functionalities related to user ranks
 * @author PAV
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ranks_model extends MY_Model {

    public function get_all_rank_details_by_id($id = '') {
        $this->db->select('rd.*');
        $this->db->from(TBL_RANK_DETAILS . ' as rd');
        $this->db->where(array('rd.id' => $id));
        return $this->db->get();
    }

    /**
     * Get rank details by number of reviews and likes
     * @param int $reviews
     * @param int $likes
     * @author KU
     */
    public function get_rank_by_reviews_likes($reviews, $likes) {
        $this->db->select('id,name');
        $this->db->where('required_reviews <=', $reviews);
        $this->db->where('required_likes <=', $likes);
        $this->db->order_by('required_reviews,required_likes', 'DESC');
        $query = $this->db->get(TBL_RANK_DETAILS);
        return $query->row_array();
    }

    /**
     * Gets default rank
     * @author KU
     * @return arr
     */
    public function get_default_rank() {
        $this->db->select('id');
        $this->db->where('required_reviews', 0);
        $this->db->where('required_likes', 0);
        $query = $this->db->get(TBL_RANK_DETAILS);
        return $query->row_array();
    }

}

/* End of file Ranks_model.php */
/* Location: ./application/models/admin/Ranks_model.php */