<?php

/**
 * Properties Model - Manage Properties
 * @author KU
 */
class Properties_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all properties by its type from database
     * @param int $type 1 For Primary property and 2 For Secondary property
     * @return array
     */
    public function get_allproperties($type = 1) {
//        $this->db->select('*,(select GROUP_CONCAT(name_en) FROM ' . TBL_PROPERTIES . ' WHERE FIND_IN_SET(id,property_ids)!=0 AND status=\'active\') as properties,(select GROUP_CONCAT(name_en) FROM ' . TBL_LIQUIDS . ' WHERE FIND_IN_SET(id,liquid_ids)!=0 AND status=\'active\') as liquids');
        $this->db->where('type', $type);
        $this->db->where('status!=', 'deleted');
        $this->db->order_by('created', 'desc');
        $query = $this->db->get(TBL_PROPERTIES);
        return $query->result_array();
    }

    /**
     * Get results based on datatable in properties list page
     * @param string $type - count or result 
     * @return array
     */
    public function get_all_properties($type = 'result') {
        $columns = ['id', 'icon', 'name_en', 'price_property', 'properties', 'liquids', 'created'];

        $keyword = $this->input->get('search');
        $this->db->select('*,(select GROUP_CONCAT(name_en) FROM ' . TBL_PROPERTIES . ' WHERE FIND_IN_SET(id,property_ids)!=0 AND status=\'active\') as properties,(select GROUP_CONCAT(name_en) FROM ' . TBL_LIQUIDS . ' WHERE FIND_IN_SET(id,liquid_ids)!=0 AND status=\'active\') as liquids');

        if (!empty($keyword['value'])) {
            $where = '(name_en LIKE "%' . $keyword['value'] . '%" OR name_gr LIKE "%' . $keyword['value'] . '%")';
            $this->db->where($where);
        }
        $this->db->where('status', 'active');

        $this->db->order_by($columns[$this->input->get('order')[0]['column']], $this->input->get('order')[0]['dir']);

        if ($type == 'count') {
            $query = $this->db->get(TBL_PROPERTIES);
            return $query->num_rows();
        } else {
            $this->db->limit($this->input->get('length'), $this->input->get('start'));
            $query = $this->db->get(TBL_PROPERTIES);
            return $query->result_array();
        }
    }

    /**
     * Get property details
     * @param int $property_id Id of property
     * @return array
     */
    public function get_property($property_id) {
        $this->db->where('id', $property_id);
        $this->db->where('status!=', 'deleted');
        $query = $this->db->get(TBL_PROPERTIES);
        return $query->row_array();
    }

    /**
     * Get all properties of particular category
     * @param int $category_id
     * @param int $type Type of categroy
     * @return array
     */
    public function get_properties_by_category($category_id, $type) {
        $this->db->select('p.name_en,p.name_gr,p.desc_en,p.desc_gr,p.id');
        $this->db->join(TBL_CATEGORIES_PROPERTIES_LINK . ' l', 'p.id=l.property_id');
        $this->db->where('l.category_id', $category_id);
        $this->db->where('l.type', $type);
        $this->db->where('p.status', 'active');
        $query = $this->db->get(TBL_PROPERTIES . ' p');
        return $query->result_array();
    }

    /**
     * Get all properties with average rating of particular category and supplement
     * @param int $category_id
     * @param int $supplement_id
     * @param int $type 1- Powder and 2- Pill
     * @return type
     */
    public function get_properties_rating_cat_sup($category_id, $supplement_id, $type) {
        $this->db->select('p.name_en,p.name_gr,p.desc_en,p.desc_gr,p.id,IF(ar.average_rating IS NULL,0,ar.average_rating) average_rating');
        $this->db->join(TBL_CATEGORIES_PROPERTIES_LINK . ' l', 'p.id=l.property_id AND l.type=' . $type);
        $this->db->join(TBL_SUPPLEMENT_PROPERTIES_AVERAGE_RATINGS . ' ar', 'l.property_id=ar.property_id AND ar.supplement_id=' . $supplement_id, 'left');
        $this->db->where('l.category_id', $category_id);
        $this->db->where('p.status', 'active');
        $query = $this->db->get(TBL_PROPERTIES . ' p');
        return $query->result_array();
    }

    /**
     * Get secondary properties of supplements
     * @param int $supplement_id
     * @return array
     */
    public function get_secproperties_by_supplement($supplement_id) {
        $this->db->select('p.name_en,p.name_gr');
        $this->db->join(TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK . ' l', 'p.id=l.property_id');
        $this->db->where('l.supplement_id', $supplement_id);
        $this->db->where('p.status', 'active');
        $query = $this->db->get(TBL_PROPERTIES . ' p');
        return $query->result_array();
    }

    /**
     * Get price property detail
     */
    public function get_price_property() {
        $query = $this->db->get(TBL_PRICE_PROPERTY);
        return $query->row_array();
    }

}
