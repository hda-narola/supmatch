<?php

/**
 * Manage Users related stuff
 * @author PAV
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends MY_Model {

    public function index() {
        
    }

    public function get_all_users_details() {
        $this->db->select('u.*,r.name as role_name,rd.rank');
        $this->db->from(TBL_USERS . ' as u');
        $this->db->join(TBL_ROLES . ' as r', 'u.role_id=r.id');
        $this->db->join(TBL_RANK_DETAILS . ' as rd', 'u.rank_id=rd.id AND rd.status!=\'deleted\'', 'left');
        $this->db->order_by('u.created', 'DESC');
        $this->db->where('u.status!=', 'deleted'); //-- Added by KU to not display deleted records
        return $this->db->get();
    }

    public function get_all_users_details_by_id($id = '') {
        $this->db->select('u.*,r.name as role_name');
        $this->db->from(TBL_USERS . ' as u');
        $this->db->where(array('u.id' => $id));
        $this->db->join(TBL_ROLES . ' as r', 'u.role_id=r.id');
        return $this->db->get();
    }

    public function check_login_validation($uname, $pass) {
        $this->db->select('u.*');
        $this->db->from(TBL_USERS . ' as u');
        $where = '(u.nickname="' . $uname . '" or u.email="' . $uname . '")';
        $this->db->where($where);
        $this->db->where('u.password', $pass);
        $this->db->where('u.status', 'active');
        return $this->db->get();
    }

    /**
     * Check email exist or not for unique email
     * @author KU
     * @param string $email
     * @return array
     */
    public function check_unique_email($email) {
        $this->db->where('email', $email);
        $query = $this->db->get(TBL_USERS);
        return $query->row_array();
    }

    /**
     * Returns number of user's like counts
     * @param int $userid
     */
    public function get_user_total_like_counts($userid) {
        $this->db->select('count(sr.id) total');
        $this->db->join(TBL_SUPPLEMENT_RATING_LIKE . ' rl', 'sr.id=rl.supplement_rating_id');
        $this->db->where('rl.status', 1);
        $this->db->where('sr.status', 'active');
        $this->db->where('sr.user_id', $userid);
        $query = $this->db->get(TBL_SUPPLEMENT_RATING . ' sr');
        return $query->row_array();
    }

    /**
     * Returns number of user's total reviews (Verified reviews - a review that is made on a product that was ordered already threw Supmatch.)
     * @param int $userid
     */
    public function get_user_total_reviews($userid) {
        $this->db->select('count(r.id) as total');
        $this->db->join('(SELECT DISTINCT user_id,sup_id FROM ' . TBL_ORDERS . ') o', 'r.user_id=o.user_id AND r.supplement_id=o.sup_id');
        $this->db->where('r.status=\'active\' AND o.sup_id IS NOT NULL AND r.user_id=' . $userid);
        $query = $this->db->get(TBL_SUPPLEMENT_RATING . ' r');
        return $query->row_array();
        /*
          $this->db->select('count(id) total');
          $this->db->where('status', 'active');
          $this->db->where('user_id', $userid);
          $query = $this->db->get(TBL_SUPPLEMENT_RATING);
          return $query->row_array();
         */
    }

    /**
     * Returns user rank based on its current rank id
     * @param int $user_id
     * @author KU
     */
    public function get_user_rank($user_id) {
        $this->db->select('rd.rank,u.rank_id,u.email,u.nickname');
        $this->db->join(TBL_RANK_DETAILS . ' rd', 'u.rank_id=rd.id');
        $this->db->where('u.id', $user_id);
        $query = $this->db->get(TBL_USERS . ' u');
        return $query->row_array();
    }

    /**
     * Checks nicjname exist or not
     * @param string $username
     * @return array
     * @author KU
     */
    public function check_unique_nickname($username) {
        $this->db->where('status!=', 'deleted');
        $this->db->where('nickname', $username);
        $query = $this->db->get(TBL_USERS);
        return $query->row_array();
    }

}

/* End of file Users_model.php */
/* Location: ./application/models/admin/Users_model.php */