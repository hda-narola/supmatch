<?php

/**
 * Categories Model - Manage Categories
 * @author KU
 */
class Orders_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Insert category into table
     * @param array $dataArr
     * @return int 
     */
    public function insert_category($dataArr) {
        $this->db->insert(TBL_ORDERS, $dataArr);
        return $this->db->insert_id();
    }
    
    
    public function get_all_orders($tbl_name, $condition, $order_by = 'o.id'){
        $this->db->select('o.id as order_id, o.user_id, o.sup_id, o.quantity, o.order_date, o.created order_created, o.modified as order_modified, s.*');
        $this->db->join(TBL_SUPPLEMENTS . ' s', 'o.sup_id=s.id', 'left');
        $this->db->where($condition);
        if($order_by == 'name_gr'){
            $order = 'asc';
        }else{
            $order = 'desc';
        }
        $this->db->order_by($order_by, $order);
        $query = $this->db->get($tbl_name . ' o');
        return $query->result_array();
    }
    

}
