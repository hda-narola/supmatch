<?php

/**
 * Cart Model - Manage Cart
 * @author KU
 */
class cart_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get All user cart items
     * @param int $user_id
     * @param string $type - Either Count or Result
     * @return array or int based on type
     */
    public function get_cartitems($user_id, $type = 'result') {
        $this->db->select('s.name_en,s.name_gr,s.desc_en,s.desc_gr,s.images,s.price as supplement_price,s.amazon_link,c.*');
        $this->db->join(TBL_SUPPLEMENTS . ' s', 'c.supplement_id=s.id', 'left');
        $this->db->where('c.user_id', $user_id);
        $this->db->order_by('c.created', 'desc');
        $query = $this->db->get(TBL_SHOPPING_CART . ' c');
        if ($type == 'count') {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    /**
     * Insert cart item into table
     * @param array $dataArr
     * @return int 
     */
    public function insert_cartitem($dataArr) {
        $this->db->insert(TBL_SHOPPING_CART, $dataArr);
        return $this->db->insert_id();
    }

    /**
     * Update cart item into table
     * @param string $condition
     * @param array $dataArr
     */
    public function update_cartitem($condition, $dataArr) {
        $this->db->where($condition);
        $this->db->update(TBL_SHOPPING_CART, $dataArr);
    }

    /**
     * Delete cart item from table
     * @param string $condition
     * @return boolean
     */
    public function delete_cartitem($condition) {
        $this->db->where($condition);
        $this->db->delete(TBL_SHOPPING_CART);
    }

    /**
     * Insert multiples imtes in table
     * @param array $data
     * @return boolean
     */
    public function insert_cartitems($data) {
        if ($this->db->insert_batch(TBL_SHOPPING_CART, $data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Get catgeory results based on category id
     * @param string $condition
     * @return Object
     * @author pav
     */
    public function get_cart_item($condition) {
        $this->db->select('s.name_en,s.name_gr,s.desc_en,s.desc_gr,s.images,s.price as supplement_price,s.amazon_link,c.*');
        $this->db->join(TBL_SUPPLEMENTS . ' s', 'c.supplement_id=s.id', 'left');
        $this->db->where($condition);
        $query = $this->db->get(TBL_SHOPPING_CART . ' c');
        return $query->row_array();
    }

}
