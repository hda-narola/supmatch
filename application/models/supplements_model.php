<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supplements_model extends MY_Model {

    public function get_all_supplements_details() {
        //-- Get review counts (Added by KU)
        $select = ',(SELECT count(id) FROM ' . TBL_SUPPLEMENT_RATING . ' WHERE supplement_id=s.id AND status!=\'deleted\') as reviews';
        $this->db->select('s.*,c.name_en as cat_name_en' . $select);
        $this->db->from(TBL_SUPPLEMENTS . ' as s');
        $this->db->join(TBL_CATEGORIES . ' as c', 'c.id=s.category_ids');
        $this->db->where(array('c.status' => 'active', 's.status!=' => 'deleted')); //-- Added by KU
        $this->db->order_by('created', 'DESC');
        return $this->db->get();
    }

    /**
     * Get all supplement as per user's requirements
     * @param Array $condition
     * @return Object
     * @author pav
     */
    public function get_all_supplements_by_requirements($condition = '') {
        $this->db->select('s.*,c.name_en as cat_name_en,c.name_gr as cat_name_gr');
        $this->db->from(TBL_SUPPLEMENTS . ' as s');
        $this->db->join(TBL_CATEGORIES . ' as c', 's.category_ids=c.id');
        $this->db->join(TBL_CATEGORIES_GOAL_LINK . ' as cat_goal', 's.category_ids=cat_goal.category_id');
        $this->db->join(TBL_GOALS . ' as g', 'cat_goal.goal_id=g.id');
        $this->db->where($condition);
        $this->db->where('s.status', 'active');
        $this->db->where('c.status', 'active');
        $this->db->where('g.status', 'active');
        $this->db->where('c.status', 'active');
        // $this->db->where('s.availibility',1);
        $this->db->order_by('s.created', 'desc');
        $this->db->group_by('s.id');
        $this->db->limit(3);
        return $this->db->get();
    }

    /**
     * Get all flavour by suppelemt id arr
     * @param Array $supplements_Arr
     * @return Object
     * @author pav
     */
    public function get_all_flavour_details_by_supp_id_arr($supplements_Arr) {
        $this->db->select('sf.supplement_id,f.id,f.name_en as fla_name_en');
        $this->db->from(TBL_FLAVOURS . ' as f');
        $this->db->join(TBL_SUPPLEMENTS_FLAVOURS_LINK . ' as sf', 'f.id=sf.flavour_id');
        $this->db->where('f.status', 'active'); //-- Added by KU to get all active flavours
        $this->db->where_in('sf.supplement_id', $supplements_Arr);
        return $this->db->get();
    }

    /**
     * Get all supplement by id
     * @param Integer $id
     * @return Object
     * @author pav
     */
    public function get_all_supplement_details_by_id($id = '', $condition = null) {
        $select = ",(SELECT GROUP_CONCAT(flavour_id) FROM " . TBL_SUPPLEMENTS_FLAVOURS_LINK . " WHERE supplement_id=" . $id . ") as flavour_ids,(SELECT GROUP_CONCAT(property_id) FROM " . TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK . " WHERE supplement_id=" . $id . ") as secondaryproperty_ids";
        $this->db->select('s.*' . $select);
        $this->db->from(TBL_SUPPLEMENTS . ' as s');
        if ($condition != '') {
            $this->db->where($condition);
        } else {
            $this->db->where(array('s.id' => $id, 's.status' => 'active'));
        }
        return $this->db->get();
    }

    /**
     * Get all flavour by suppelemt id
     * @param Integer $supplement_id
     * @return Object
     * @author pav
     */
    public function get_all_flavour_details_by_supp_id($supplement_id) {
        $this->db->select('f.id');
        $this->db->from(TBL_FLAVOURS . ' as f');
        $this->db->join(TBL_SUPPLEMENTS_FLAVOURS_LINK . ' as sf', 'f.id=sf.flavour_id');
        $this->db->where('sf.supplement_id', $supplement_id);
        return $this->db->get();
    }

    /**
     * Returns matched supplements result based on user search
     * @param string $search_text
     * @return array
     * @author KU
     */
    function search_suggest($search_text) {
        $language = $this->session->userdata('lang');
        if ($language == '') {
            $language = 'gr';
        }
        $column_name = 'name_' . $language;
        $data = $this->db->query('SELECT images,id,' . $column_name . ' as name FROM ' . TBL_SUPPLEMENTS . ' WHERE ' . $column_name . ' LIKE "' . $this->db->escape_like_str($search_text) . '%" AND status=\'active\' ORDER BY created DESC');
        return $data->result_array();
    }

    /**
     * Get supplement's review by id
     * @param Integer $supplement_id
     * @return Object
     * @author pav
     */
    public function get_supplement_review_by_id($supplement_id = '', $session_user_id = '') {
        $this->db->select('supp_rate.id as review_id,u.id as user_id,u.nickname as user_name,supp_rate.created as review_date,supp_rate.comment as review_comment,f.name_gr as flavour_name_gr,l.name_gr as liquid_name_gr,GROUP_CONCAT(p.name_gr) as prop_name_gr,GROUP_CONCAT(prop_rate.rating) as prop_rating,rd.color,rd.icon as rank_icon');
        // count(DISTINCT supp_rate_like.id) as total_likes,count(DISTINCT supp_rate_dislike.id) as total_dislikes');
        //count( DISTINCT sess_user_like.id) as sess_user_like, count( DISTINCT sess_user_dislike.id) as sess_user_dislike');
        $this->db->from(TBL_SUPPLEMENT_RATING . ' as supp_rate');
        $this->db->join(TBL_PROPERTIES_RATING_LINK . ' as prop_rate', 'supp_rate.id=prop_rate.supplement_rating_id', 'left');
        $this->db->join(TBL_FLAVOURS . ' as f', 'supp_rate.flavour_id=f.id', 'left');
        $this->db->join(TBL_LIQUIDS . ' as l', 'supp_rate.liquid_id=l.id', 'left');
        $this->db->join(TBL_USERS . ' as u', 'supp_rate.user_id=u.id', 'left');
        $this->db->join(TBL_RANK_DETAILS . ' rd', 'u.rank_id=rd.id AND rd.status=\'active\'', 'left');
        $this->db->join(TBL_PROPERTIES . ' as p', 'prop_rate.property_id=p.id', 'left');
        // $this->db->join(TBL_SUPPLEMENT_RATING_LIKE.' as supp_rate_like','supp_rate.id=supp_rate_like.supplement_rating_id and supp_rate_like.status = "1"','left');
        // $this->db->join(TBL_SUPPLEMENT_RATING_LIKE.' as supp_rate_dislike','supp_rate.id=supp_rate_dislike.supplement_rating_id and supp_rate_dislike.status = "0"','left');
        // $this->db->join(TBL_SUPPLEMENT_RATING_LIKE.' as sess_user_like','sess_user_like.user_id=supp_rate_like.user_id and sess_user_like.user_id='.$session_user_id.' and sess_user_like.status="1"','left');
        // $this->db->join(TBL_SUPPLEMENT_RATING_LIKE.' as sess_user_dislike','sess_user_dislike.user_id=supp_rate_dislike.user_id and sess_user_dislike.user_id='.$session_user_id.' and sess_user_dislike.status="0"','left');
        $this->db->where('supp_rate.supplement_id', $supplement_id);
        $this->db->where('supp_rate.status', 'active');
        $this->db->group_by('supp_rate.id');
        $this->db->order_by('supp_rate.created', 'DESC');
        return $this->db->get();
    }

    /**
     * Get total likes/dislikes by supplement's review id
     * @param Integer $review_id
     * @return Object
     * @author pav
     */
    public function get_total_likes_dislikes_by_supp($review_id = '') {
        $this->db->select('srl.id,COUNT(IF(srl.status="1",1, NULL)) "tot_likes",COUNT(IF(srl.status="0",1, NULL)) "tot_dislikes"');
        $this->db->from(TBL_SUPPLEMENT_RATING_LIKE . ' as srl');
        $this->db->where(array('srl.supplement_rating_id' => $review_id));
        return $this->db->get();
    }

    /**
     * Get likes/dislikes by user on particular supplement's review id
     * @param Integer $sess_user_id
     * @param Integer $review_id
     * @return Object
     * @author pav
     */
    public function get_sess_user_like_dislikes($sess_user_id = '', $review_id = '') {
        $this->db->select('srl.id,COUNT(IF(srl.status="1",1, NULL)) "sess_user_likes",COUNT(IF(srl.status="0",1, NULL)) "sess_user_dislikes"');
        $this->db->from(TBL_SUPPLEMENT_RATING_LIKE . ' as srl');
        $this->db->where(array('srl.user_id' => $sess_user_id, 'srl.supplement_rating_id' => $review_id));
        return $this->db->get();
    }

    /**
     * Get review details by user id for like dislikes
     * @param Array $condition
     * @return Object
     * @author pav
     */
    public function get_review_details_by_user_id($condition = '') {
        $this->db->select('srl.*');
        $this->db->from(TBL_SUPPLEMENT_RATING_LIKE . ' as srl');
        $this->db->where($condition);
        return $this->db->get();
    }

    /**
     * Updates accuracy of the supplement
     * @param int $supplement_id
     * @author KU
     */
    public function update_accuracy($supplement_id) {
        $query = "UPDATE " . TBL_SUPPLEMENTS . " SET accuracy = (SELECT SUM(weight) FROM " . TBL_SUPPLEMENT_RATING . " WHERE supplement_id=" . $supplement_id . " AND status='active') where id=" . $supplement_id;
        $this->db->query($query);
    }

    /**
     * Updates supplements property ratings 
     * @param int $supplement_id
     * @author KU
     */
    public function insert_update_supplement_properties_rating($supplement_id) {
        $query = 'INSERT INTO ' . TBL_SUPPLEMENT_PROPERTIES_AVERAGE_RATINGS . ' (supplement_id,property_id,average_rating) '
                . 'SELECT sr.supplement_id,pr.property_id, (sum(pr.rating * sr.weight)/sum(sr.weight)) FROM ' . TBL_PROPERTIES_RATING_LINK . ' pr '
                . 'LEFT JOIN ' . TBL_SUPPLEMENT_RATING . ' sr ON pr.supplement_rating_id=sr.id WHERE sr.supplement_id=' . $supplement_id . ' AND sr.status!=\'deleted\' group by pr.property_id ON DUPLICATE KEY UPDATE average_rating = VALUES(average_rating)';
        $this->db->query($query);
    }

    /**
     * Return all supplements for matching functionality 
     * @author KU
     * @param string $select
     * @param string $condition
     * @param int $category_id
     * @param int $supplement_type Supplement type either Powder/Pill
     * @return array
     */
    public function get_supplements_for_matching($select, $condition, $category_id, $supplement_type) {
        $price_join = 'SELECT id,'
                . '(10 - (9 * ((price/weight)-@minprice) / (@maxprice-@minprice) )) as price_rating FROM ' . TBL_SUPPLEMENTS . ' ,'
                . '(SELECT @maxprice:= MAX(price/weight) FROM ' . TBL_SUPPLEMENTS . ' WHERE category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type . ') as maxprice,'
                . '(SELECT @minprice:= MIN(price/weight) FROM ' . TBL_SUPPLEMENTS . ' where category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type . ') as minprice '
                . 'WHERE category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type;
        $secondary_pros = 'SELECT supplement_id,GROUP_CONCAT(property_id) as secondary_properties FROM ' . TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK . ' GROUP BY supplement_id';
        $this->db->select($select);
        $this->db->join(TBL_CATEGORIES_PROPERTIES_LINK . ' cp', 's.category_ids=cp.category_id', 'left');
        $this->db->join(TBL_SUPPLEMENT_PROPERTIES_AVERAGE_RATINGS . ' ar', 'ar.property_id=cp.property_id AND ar.supplement_id=s.id', 'left');
        $this->db->join('(' . $price_join . ') p', 's.id=p.id', 'left');
        $this->db->join('(' . $secondary_pros . ') sp', 's.id=sp.supplement_id', 'left');
        $this->db->where($condition);
        $this->db->order_by('DOWNGRADE ASC,UPGRADE DESC,s.accuracy DESC');
        $this->db->group_by('s.id');
        $this->db->limit(3); //- Get best 3 supplements from database
        $query = $this->db->get(TBL_SUPPLEMENTS . ' s');
        return $query->result_array();
    }

    /**
     * Insert multiple supplement secondary properties into table
     * @author KU
     * @param array $dataArr
     * @return int 
     */
    public function insert_secondprop_links($dataArr) {
        $this->db->insert_batch(TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK, $dataArr);
    }

    /**
     * Delete supplement and property link from database
     * @author KU
     * @param int $supplement_id
     * @return boolean
     */
    public function delete_secondprop_links($supplement_id) {
        $this->db->where(array('supplement_id' => $supplement_id));
        $this->db->delete(TBL_SUPPLEMENTS_SECONDARYPROPERTIES_LINK);
    }

    /**
     * Return supplement's price rating
     * @param int $category_id
     * @param int $supplement_type
     * @param int $supplement_id
     */
    public function get_supplement_price_rating($category_id, $supplement_type, $supplement_id) {
        $price_query = 'SELECT id,'
                . '(10 - (9 * ((price/weight)-@minprice) / (@maxprice-@minprice) )) as price_rating FROM ' . TBL_SUPPLEMENTS . ' ,'
                . '(SELECT @maxprice:= MAX(price/weight) FROM ' . TBL_SUPPLEMENTS . ' WHERE category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type . ') as maxprice,'
                . '(SELECT @minprice:= MIN(price/weight) FROM ' . TBL_SUPPLEMENTS . ' where category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type . ') as minprice '
                . 'WHERE category_ids=' . $category_id . ' AND status=\'active\' AND type=' . $supplement_type . ' AND id=' . $supplement_id;
        $query = $this->db->query($price_query);
        return $query->row_array();
    }

    /**
     * Returns total number of review of particular supplements
     * @param int $supplement_id
     * @author KU
     */
    public function total_reviews($supplement_id = NULL) {
        $this->db->select('count(id) as total');
        $this->db->where('supplement_id', $supplement_id);
        $this->db->where('status!=', 'deleted');
        $query = $this->db->get(TBL_SUPPLEMENT_RATING);
        return $query->row_array();
    }

    /**
     * 
     * @param type $supplement_id
     */
    public function delete_properties_average_rating($supplement_id = NULL) {
        $this->db->where('supplement_id', $supplement_id);
        $this->db->delete(TBL_SUPPLEMENT_PROPERTIES_AVERAGE_RATINGS);
    }

}

/* End of file Supplements_model.php */
/* Location: ./application/models/admin/Supplements_model.php */